#include "stm32h7xx_sys.h"
#include <rt_sys.h>
#include <time.h>
#include <stdlib.h>
#include "hw_time.h"
#include "visual_debug.h"

#define SUPPORT_FILE_SYSTEM		1
#define SUPPORT_TIME			1
#define EXTRA_MALLOC			1

__STATIC_FORCEINLINE int Use_Message_Send(const unsigned char* str,unsigned int len){
	return Debug_Message_Send_With_Shell((const char*)str,len)?0:-1;
}

__STATIC_FORCEINLINE int Use_Message_Recive(unsigned char* str,unsigned int len){
	uint32_t real_len;
	return Debug_Message_Recive((char*)str,len,&real_len)?real_len:-1;
}

__STATIC_FORCEINLINE void Use_Error_Send(int ch){
	Debug_Error_Out(ch);
}

#if SUPPORT_FILE_SYSTEM
	#include "ff.h"
#endif

#if defined(__clang__)
	__asm(".global __use_no_semihosting\n\t");
	#if EXTRA_MALLOC
		__asm(".global __use_no_heap_region\n\t");
	#endif
#elif defined(__CC_ARM)
	#pragma import(__use_no_semihosting)
	#if EXTRA_MALLOC
		#pragma import(__use_no_heap_region)
	#endif
#endif

#if defined(__MICROLIB)
	#if defined(__clang__)
		__asm(".global __use_full_stdio\n\t");
	#elif defined(__CC_ARM)
		#pragma import(__use_full_stdio)
	#endif
#endif

#define STDIN	1
#define STDOUT	2
#define STDERR	3
#define IS_STD(fn)	((fn)>=1&&(fn)<=3)
/*
* These names are used during library initialization as the
* file names opened for stdin, stdout, and stderr.
* As we define _sys_open() to always return the same file handle,
* these can be left as their default values.
*/
const char __stdin_name[] = "::stdin";
const char __stdout_name[] = "::stdout";
const char __stderr_name[] = "::stderr";


FILEHANDLE _sys_open(const char *pcFile, int openmode)
{
#if SUPPORT_FILE_SYSTEM
	BYTE mode;
	FIL *fp;
	FRESULT fr;
#endif
	if(strcmp(__stdin_name,pcFile)==0)
		return STDIN;
	else if(strcmp(__stdout_name,pcFile)==0)
		return STDOUT;
	else if(strcmp(__stderr_name,pcFile)==0)
		return STDERR;
#if SUPPORT_FILE_SYSTEM
	fp=malloc(sizeof(FIL));
	if(fp==NULL){
		printf("malloc fail!\r\n");
		return -1;
	}
	/* http://elm-chan.org/fsw/ff/doc/open.html */
	if(openmode & OPEN_W){
		mode = FA_CREATE_ALWAYS | FA_WRITE;
		if (openmode & OPEN_PLUS)
			mode |= FA_READ;
	}
	else if(openmode & OPEN_A){
		mode = FA_OPEN_APPEND | FA_WRITE;
		if (openmode & OPEN_PLUS)
			mode |= FA_READ;
	}
	else{
		mode = FA_READ;
		if (openmode & OPEN_PLUS)
			mode |= FA_WRITE;
	}

	fr = f_open(fp, pcFile, mode);
	if (fr == FR_OK)
		return (FILEHANDLE)fp;

	free(fp);
#endif
	return -1;
}

int _sys_close(FILEHANDLE fh)
{
#if SUPPORT_FILE_SYSTEM
	FRESULT fr;
#endif
	if(IS_STD(fh))
		return 0;
#if SUPPORT_FILE_SYSTEM
	fr = f_close((FIL *)fh);
	if (fr == FR_OK){
		free((void *)fh);
		return 0;
	}
#endif
	return -1;
}

int _sys_write(FILEHANDLE fh, const unsigned char *buf,\
	unsigned len, int mode)
{	
#if SUPPORT_FILE_SYSTEM
	FRESULT fr;
	UINT bw;
#endif
	if(fh==STDIN)
		return -1;
	
	if(fh==STDOUT||fh==STDERR){
		return Use_Message_Send(buf,len);
	}
#if SUPPORT_FILE_SYSTEM	
	fr = f_write((FIL *)fh, buf, len, &bw);
	if (fr == FR_OK)
		return len - bw;
#endif	
	return -1;
}

int _sys_read(FILEHANDLE fh, unsigned char *buf,\
	unsigned len, int mode)
{
#if SUPPORT_FILE_SYSTEM
	FRESULT fr;
	UINT br;
#endif
	if(fh==STDOUT||fh==STDERR)
		return -1;
	if(fh==STDIN)
		return Use_Message_Recive(buf,len);
#if SUPPORT_FILE_SYSTEM		
	fr = f_read((FIL *)fh, buf, len, &br);
	if (fr == FR_OK)
		return len - br;
#endif
	return -1;
}

void _ttywrch(int ch)
{
	Use_Error_Send(ch);
}

int _sys_istty(FILEHANDLE fh)
{
	return IS_STD(fh); /* buffered output */
}

int _sys_seek(FILEHANDLE fh, long pos)
{
#if SUPPORT_FILE_SYSTEM
	FRESULT fr;
	if(!IS_STD(fh)){
		fr = f_lseek((FIL *)fh, pos);
		if (fr == FR_OK)
			return 0;
	}
#endif
	return -1;
}

long _sys_flen(FILEHANDLE fh)
{
#if SUPPORT_FILE_SYSTEM
	if (!IS_STD(fh))
		return f_size((FIL *)fh);
#endif
	return -1;
}
int _sys_ensure(FILEHANDLE fh){
	if(IS_STD(fh)){
		return 0;
	}
#if SUPPORT_FILE_SYSTEM
	if(f_sync((FIL*)fh)==FR_OK)
		return 0;
#endif
	return -1;
}
void _sys_exit(int code){
	printf("This Program exit! code:%d\r\n",code);
	osThreadExit();
}

int remove(const char *pcPath){
#if SUPPORT_FILE_SYSTEM
	if(strcmp(pcPath,__stdin_name)==0||strcmp(pcPath,__stdout_name)==0||strcmp(pcPath,__stderr_name)==0)
		return -1;
	if(f_unlink(pcPath)==FR_OK)
		return 0;
#endif
	return -1;
}

int rename(const char *old_filename, const char *new_filename){
#if SUPPORT_FILE_SYSTEM
	if(strcmp(old_filename,__stdin_name)==0||strcmp(old_filename,__stdout_name)==0||strcmp(old_filename,__stderr_name)==0)
		return -1;
	if(strcmp(new_filename,__stdin_name)==0||strcmp(new_filename,__stdout_name)==0||strcmp(new_filename,__stderr_name)==0)
		return -1;
	if(f_rename(old_filename,new_filename)==FR_OK)
		return 0;
#endif
	return -1;
}

int system(const char *command){
	printf("system:%s\r\n",command);
	return -1;
}

int _sys_tmpnam(char * name, int sig, unsigned maxlen){
	snprintf(name, maxlen, "$tem$%03d", sig);
	return 1;
}

clock_t clock(void){
#if SUPPORT_OS==FREE_RTOS
	return xTaskGetTickCount();
#elif SUPPORT_OS==RTX_RTOS
	return osKernelGetTickCount();
#else
	return 0;
#endif
}
#if SUPPORT_TIME
/*计算当前时间到格林威治时间总共过了多少秒，以当前北京地区东八区时间为准*/
static unsigned long mktime_second(unsigned int year0,unsigned int mon0,unsigned int day,unsigned int hour,unsigned int min,unsigned int sec)
{
	int leap_years = 0;
	unsigned long days = 0;
	unsigned long seconds = 0;
	unsigned long resultValue = 0;
	int i = 0;
	int year = year0 - 1 ;
	int TIME_ZONE	= 8;//用于表示当前时区，=8表示北京时区东八区，单位小时，因为比本初子午线时间快8个小时
	//              			     1,  2, 3, 4, 5, 6, 7, 8, 9, 10,11,12
	const int month_days[] = {31, 28, 31,30,31, 30,31,31, 30,31,30,31};
	int isleapyear = 0;
    
	if(mon0>12||mon0==0)
		return 0;
	if(day>31||day==0)
		return 0;
	if(hour>=24)
		return 0;
	if(min>=60)
		return 0;
	if(sec>=60)
		return 0;
	leap_years = year/4 - year/100;//计算普通闰年
	leap_years += year/400;//加上世纪闰年
	//闰年为366天，平年为365天
	days = year * 365 + leap_years;//如果当前年份是2000年，则到此便计算出了从公元0年初到1999年尾的天数
	
	//今年是否是闰年
	if((year0%4 == 0 && year0 % 100!=0) || year0%400==0) isleapyear = 1;//今年是闰年
	//按平年计算，到上个月为止总共度过的天数
	for(i=0;i<mon0 - 1;i++) days += month_days[i];
	if(mon0 >2) days +=isleapyear;//2月份闰年要按29天计算
	days= days + day - 1;
    
	//days应该减去1970年以前的天数,1970/1/1 0:0:0 0
	//year = 1969 	leap_years = 1969/4-1969/100 + 1969/400 = 492 - 19 + 4 = 477
	//isleapyear = 0
	//days = 1969 * 365 + 477 = 719162
 
	//考虑到时区的问题，实际秒钟数据应该在当前小时的基础之上加上时区时间TIME_ZONE
	//即在北京时间东八区，实际应该计算当前时间到1970/1/1 08:0:0 0的秒钟数
	//即 seconds = 8 * 60 * 60
     
	seconds = (hour) * 60 * 60 + (min) * 60 + sec;
	resultValue = (days - 719162) * 24 * 60 * 60;
	resultValue	+= seconds;
	resultValue -= ((unsigned long)TIME_ZONE)*60*60;
	
	return resultValue;
}
#endif
time_t time(time_t *t){
#if SUPPORT_TIME
	time_t ptime=mktime_second(RTC_Date.Year+2000,RTC_Date.Month,\
				RTC_Date.Date,RTC_Time.Hours,RTC_Time.Minutes,RTC_Time.Seconds);
	if(t!=NULL)
		*t=ptime;
	return ptime;
#else
	return 0;
#endif
}
#if defined(__MICROLIB)
int fputc(int ch, FILE *f){
	char c = ch;
	Use_Message_Send((const unsigned char*)&c,1);
	return ch;
}
int feof(FILE* f){
	return f_eof((FIL*)f);
}
__NO_RETURN void exit(int code){
	_sys_exit(code);
	while(true);
}
int system(const char* cmd){
	return 0;
}
#endif
//重定向内存分配的相关库函数
#if EXTRA_MALLOC

void *malloc (size_t size){
	return sys_malloc(size);
}

void free(void* p){
	sys_free(p);
}

void *realloc(void* p,size_t want){
	return sys_realloc(p,want);
}

void *calloc(size_t nmemb, size_t size){
	void* p;
	p=sys_malloc(size*nmemb);
	if(p!=NULL)
		memset(p,0,size*nmemb);
	return p;
}

void *aligned_alloc(size_t align_byte,size_t size){
	return sys_aligned_alloc(align_byte,size);
}

#endif



