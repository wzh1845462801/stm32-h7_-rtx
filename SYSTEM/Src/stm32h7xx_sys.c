#include "stm32h7xx_sys.h"

#if USE_EXRAM
#include "hw_sdram.h"
#endif
#include "tlsf.h"

//////////////////////////////////////////////////////////////////////////////////
#if SUPPORT_OS==RTX_RTOS
//内部存储分配256K，共计512K，起始地址0x24000000
#define HEAP_IN_AXI_RAM_SIZE  		(256*1024)
//外部存储分配16M，共计28M，起始地址0xC0000000
#define HEAP_IN_EXRAM_SIZE			(30*1024*1024)

#define HEAP_IN_SRAM1_SIZE			(128*1024)
#define HEAP_IN_SRAM2_SIZE			(128*1024)
#define HEAP_IN_SRAM3_SIZE			(32*1024)
#define HEAP_IN_SRAM4_SIZE			(64*1024)
static __ALIGNED(8) uint8_t INRAM_HEAP[HEAP_IN_AXI_RAM_SIZE]__IN_AXI_RAM;
static __ALIGNED(8) uint8_t IN_SRAM1_HEAP[HEAP_IN_SRAM1_SIZE]__IN_SRAM1;
static __ALIGNED(8) uint8_t IN_SRAM2_HEAP[HEAP_IN_SRAM2_SIZE]__IN_SRAM2;
static __ALIGNED(8) uint8_t IN_SRAM3_HEAP[HEAP_IN_SRAM3_SIZE]__IN_SRAM3;
static __ALIGNED(8) uint8_t IN_SRAM4_HEAP[HEAP_IN_SRAM4_SIZE]__IN_SRAM4;
static __ALIGNED(8) uint8_t EXRAM_HEAP[HEAP_IN_EXRAM_SIZE]__IN_EXRAM_HEAP;

typedef struct{
	void* addr;
	size_t mem_size;
}Mem_Region;

//注意低位地址在前，高位地址在后
static const Mem_Region xHeapRegions[]={
	{(uint8_t*)INRAM_HEAP,sizeof(INRAM_HEAP)},
	{(uint8_t*)IN_SRAM1_HEAP,sizeof(IN_SRAM1_HEAP)},
	{(uint8_t*)IN_SRAM2_HEAP,sizeof(IN_SRAM2_HEAP)},
	{(uint8_t*)IN_SRAM3_HEAP,sizeof(IN_SRAM3_HEAP)},
	{(uint8_t*)IN_SRAM4_HEAP,sizeof(IN_SRAM4_HEAP)},
	{(uint8_t*)EXRAM_HEAP,sizeof(EXRAM_HEAP)},
	{NULL,0}
};

static inline bool Check_In_Heap(void* addr){
	for(const Mem_Region* pRegion=xHeapRegions;pRegion->addr!=NULL;pRegion++){
		if((uint8_t*)addr>=(uint8_t*)pRegion->addr&&(uint8_t*)addr<((uint8_t*)pRegion->addr+pRegion->mem_size))
			return true;
	}
	return false;
}

static tlsf_t sys_heap_root=NULL;

static void sys_heap_init(void){
	for(const Mem_Region* pRegion=xHeapRegions;pRegion->addr!=NULL;pRegion++){
		if(sys_heap_root==NULL){
			sys_heap_root=tlsf_create_with_pool(pRegion->addr,pRegion->mem_size);
		}
		else{
			tlsf_add_pool(sys_heap_root,pRegion->addr,pRegion->mem_size);
		}
	}
}

static void* svc_sys_malloc(size_t want_size){
	return tlsf_malloc(sys_heap_root,want_size);
}

static void* svc_sys_realloc(void* src_addr,size_t want_size){
	return tlsf_realloc(sys_heap_root,src_addr,want_size);
}

static void* svc_sys_aligend_alloc(size_t align_byte,size_t want_size){
	if((align_byte-1)&align_byte)
		return NULL;
	return tlsf_memalign(sys_heap_root,align_byte,want_size);
}

static void svc_sys_free(void* addr){
	if(Check_In_Heap(addr)){
		tlsf_free(sys_heap_root,addr);
	}
}

static void sys_heap_state_tlfs_walk_func(void *ptr, size_t size, int used, void *user){
	Sys_Heap_State* pstate=user;
	pstate->total_size+=size;
	if(used){
		pstate->used_size+=size;
		pstate->used_node_num++;
		if(pstate->max_used_node_size==0||pstate->max_used_node_size<size)
			pstate->max_used_node_size=size;
		if(pstate->min_used_node_size==0||pstate->min_used_node_size>size)
			pstate->min_used_node_size=size;
	}
	else{
		pstate->free_size+=size;
		pstate->free_node_num++;
		if(pstate->max_free_node_size==0||pstate->max_free_node_size<size){
			pstate->max_free_node_size=size;
		}
		if(pstate->min_free_node_size==0||pstate->min_free_node_size>size){
			pstate->min_free_node_size=size;
		}
	}
}

static void svc_sys_get_heap_state(Sys_Heap_State* pstate){
	memset(pstate,0,sizeof(Sys_Heap_State));
	for(const Mem_Region* pRegion=xHeapRegions;pRegion->addr!=NULL;pRegion++){
		if(pRegion==xHeapRegions){
			tlsf_walk_pool(tlsf_get_pool(sys_heap_root),sys_heap_state_tlfs_walk_func,pstate);
		}
		else{
			tlsf_walk_pool(pRegion->addr,sys_heap_state_tlfs_walk_func,pstate);
		}
	}
}

static void svc_rtos_get_heap_state(Sys_Heap_State* pstate){
	memset(pstate,0,sizeof(Sys_Heap_State));
	tlsf_walk_pool(tlsf_get_pool(osRtxInfo.mem.common),sys_heap_state_tlfs_walk_func,pstate);
}

extern void * const osRtxUserSVC[];
#define USE_SVC_COUNT		8

extern void* svc_dl_vector_get_lib_func_entry(size_t index);

void * const osRtxUserSVC[USE_SVC_COUNT+1] = { 
	(void *)USE_SVC_COUNT,
	[1]=(void *)svc_sys_malloc,
	[2]=(void *)svc_sys_realloc,
	[3]=(void *)svc_sys_aligend_alloc,
	[4]=(void *)svc_sys_free,
	[5]=(void *)svc_sys_get_heap_state,
	[6]=(void *)svc_rtos_get_heap_state,
	[7]=(void *)NULL,
	[8]=(void *)svc_dl_vector_get_lib_func_entry,
};



#endif

#if SUPPORT_OS==FREE_RTOS
#if( configUSE_MALLOC_FAILED_HOOK == 1 )
void vApplicationMallocFailedHook( void ){
	printf("Memeory error!!\r\n");
}
#endif
#if defined(configSUPPORT_STATIC_ALLOCATION)&& (configSUPPORT_STATIC_ALLOCATION==1)

static __ALIGNED(8) StackType_t IdleTaskStack[configMINIMAL_STACK_SIZE];
static __ALIGNED(8) StackType_t TimerTaskStack[configTIMER_TASK_STACK_DEPTH];

static StaticTask_t TimerTaskTCB;
static StaticTask_t IdleTaskTCB;

void vApplicationGetIdleTaskMemory(StaticTask_t **ppxIdleTaskTCBBuffer, 
								   StackType_t **ppxIdleTaskStackBuffer, 
								   uint32_t *pulIdleTaskStackSize)
{
	*ppxIdleTaskTCBBuffer=&IdleTaskTCB;
	*ppxIdleTaskStackBuffer=IdleTaskStack;
	*pulIdleTaskStackSize=configMINIMAL_STACK_SIZE;
}

void vApplicationGetTimerTaskMemory(StaticTask_t **ppxTimerTaskTCBBuffer, 
									StackType_t **ppxTimerTaskStackBuffer, 
									uint32_t *pulTimerTaskStackSize)
{
	*ppxTimerTaskTCBBuffer=&TimerTaskTCB;
	*ppxTimerTaskStackBuffer=TimerTaskStack;
	*pulTimerTaskStackSize=configTIMER_TASK_STACK_DEPTH;
}

#endif
#endif

static UART_HandleTypeDef Debug_Handler; //UART句柄

static void delay_init(void);
static void uart_init(uint32_t bound);
static void STM32_Clock_Init(void);


static void MPU_Set_Protection(void)
{
	MPU_Region_InitTypeDef MPU_InitStruct;

	/* 禁止 MPU */
	HAL_MPU_Disable();

	/* 配置AXI SRAM的MPU属性为Write back, Read allocate，Write allocate */
	MPU_InitStruct.Enable           = MPU_REGION_ENABLE;
	MPU_InitStruct.BaseAddress      = 0x24000000;
	MPU_InitStruct.Size             = MPU_REGION_SIZE_512KB;
	MPU_InitStruct.AccessPermission = MPU_REGION_FULL_ACCESS;
	MPU_InitStruct.IsBufferable     = MPU_ACCESS_BUFFERABLE;
	MPU_InitStruct.IsCacheable      = MPU_ACCESS_CACHEABLE;
	MPU_InitStruct.IsShareable      = MPU_ACCESS_NOT_SHAREABLE;
	MPU_InitStruct.Number           = MPU_REGION_NUMBER0;
	MPU_InitStruct.TypeExtField     = MPU_TEX_LEVEL1;
	MPU_InitStruct.SubRegionDisable = 0x00;
	MPU_InitStruct.DisableExec      = MPU_INSTRUCTION_ACCESS_ENABLE;

	HAL_MPU_ConfigRegion(&MPU_InitStruct);
	
	/* 配置SRAM1的属性为Write back, Read allocate，Write allocate */
    MPU_InitStruct.Enable           = MPU_REGION_ENABLE;
    MPU_InitStruct.BaseAddress      = 0x30000000;
    MPU_InitStruct.Size             = MPU_REGION_SIZE_128KB;	
    MPU_InitStruct.AccessPermission = MPU_REGION_FULL_ACCESS;
    MPU_InitStruct.IsBufferable     = MPU_ACCESS_BUFFERABLE;
    MPU_InitStruct.IsCacheable      = MPU_ACCESS_CACHEABLE;
    MPU_InitStruct.IsShareable      = MPU_ACCESS_NOT_SHAREABLE;
    MPU_InitStruct.Number           = MPU_REGION_NUMBER1;
    MPU_InitStruct.TypeExtField     = MPU_TEX_LEVEL1;
    MPU_InitStruct.SubRegionDisable = 0x00;
    MPU_InitStruct.DisableExec      = MPU_INSTRUCTION_ACCESS_ENABLE;

    HAL_MPU_ConfigRegion(&MPU_InitStruct);
	
	/* 配置SRAM2的属性为Write back, Read allocate，Write allocate */
    MPU_InitStruct.Enable           = MPU_REGION_ENABLE;
    MPU_InitStruct.BaseAddress      = 0x30020000;
    MPU_InitStruct.Size             = MPU_REGION_SIZE_128KB;	
    MPU_InitStruct.AccessPermission = MPU_REGION_FULL_ACCESS;
    MPU_InitStruct.IsBufferable     = MPU_ACCESS_BUFFERABLE;
    MPU_InitStruct.IsCacheable      = MPU_ACCESS_CACHEABLE;
    MPU_InitStruct.IsShareable      = MPU_ACCESS_NOT_SHAREABLE;
    MPU_InitStruct.Number           = MPU_REGION_NUMBER2;
    MPU_InitStruct.TypeExtField     = MPU_TEX_LEVEL1;
    MPU_InitStruct.SubRegionDisable = 0x00;
    MPU_InitStruct.DisableExec      = MPU_INSTRUCTION_ACCESS_ENABLE;

    HAL_MPU_ConfigRegion(&MPU_InitStruct);
    
	/* 配置SRAM3的属性为Write back, Read allocate，Write allocate */
    MPU_InitStruct.Enable           = MPU_REGION_ENABLE;
    MPU_InitStruct.BaseAddress      = 0x30040000;
    MPU_InitStruct.Size             = MPU_REGION_SIZE_32KB;	
    MPU_InitStruct.AccessPermission = MPU_REGION_FULL_ACCESS;
    MPU_InitStruct.IsBufferable     = MPU_ACCESS_BUFFERABLE;
    MPU_InitStruct.IsCacheable      = MPU_ACCESS_CACHEABLE;
    MPU_InitStruct.IsShareable      = MPU_ACCESS_NOT_SHAREABLE;
    MPU_InitStruct.Number           = MPU_REGION_NUMBER3;
    MPU_InitStruct.TypeExtField     = MPU_TEX_LEVEL1;
    MPU_InitStruct.SubRegionDisable = 0x00;
    MPU_InitStruct.DisableExec      = MPU_INSTRUCTION_ACCESS_ENABLE;

    HAL_MPU_ConfigRegion(&MPU_InitStruct);
	
	/* 配置SRAM4的属性为Write back, Read allocate，Write allocate */
    MPU_InitStruct.Enable           = MPU_REGION_ENABLE;
    MPU_InitStruct.BaseAddress      = 0x38000000;
    MPU_InitStruct.Size             = MPU_REGION_SIZE_64KB;	
    MPU_InitStruct.AccessPermission = MPU_REGION_FULL_ACCESS;
    MPU_InitStruct.IsBufferable     = MPU_ACCESS_BUFFERABLE;
    MPU_InitStruct.IsCacheable      = MPU_ACCESS_CACHEABLE;
    MPU_InitStruct.IsShareable      = MPU_ACCESS_NOT_SHAREABLE;
    MPU_InitStruct.Number           = MPU_REGION_NUMBER4;
    MPU_InitStruct.TypeExtField     = MPU_TEX_LEVEL1;
    MPU_InitStruct.SubRegionDisable = 0x00;
    MPU_InitStruct.DisableExec      = MPU_INSTRUCTION_ACCESS_ENABLE;

    HAL_MPU_ConfigRegion(&MPU_InitStruct);
	
	/* 配置BKRAM的属性为Non cacheable, no read allocate，no write allocate */
    MPU_InitStruct.Enable           = MPU_REGION_ENABLE;
    MPU_InitStruct.BaseAddress      = 0x38800000;
    MPU_InitStruct.Size             = MPU_REGION_SIZE_4KB;	
    MPU_InitStruct.AccessPermission = MPU_REGION_FULL_ACCESS;
    MPU_InitStruct.IsBufferable     = MPU_ACCESS_NOT_BUFFERABLE;
    MPU_InitStruct.IsCacheable      = MPU_ACCESS_NOT_CACHEABLE;
    MPU_InitStruct.IsShareable      = MPU_ACCESS_NOT_SHAREABLE;
    MPU_InitStruct.Number           = MPU_REGION_NUMBER5;
    MPU_InitStruct.TypeExtField     = MPU_TEX_LEVEL1;
    MPU_InitStruct.SubRegionDisable = 0x00;
    MPU_InitStruct.DisableExec      = MPU_INSTRUCTION_ACCESS_ENABLE;

    HAL_MPU_ConfigRegion(&MPU_InitStruct);
#if USE_EXRAM==1
	
	/* 配置SDRAM的MPU属性为Write back, Read allocate，Write allocate */
	MPU_InitStruct.Enable           = MPU_REGION_ENABLE;
	MPU_InitStruct.BaseAddress      = 0xC0000000;
	MPU_InitStruct.Size             = MPU_REGION_SIZE_32MB;	
	MPU_InitStruct.AccessPermission = MPU_REGION_FULL_ACCESS;
	MPU_InitStruct.IsBufferable     = MPU_ACCESS_BUFFERABLE;
	MPU_InitStruct.IsCacheable      = MPU_ACCESS_CACHEABLE;
	MPU_InitStruct.IsShareable      = MPU_ACCESS_NOT_SHAREABLE;
	MPU_InitStruct.Number           = MPU_REGION_NUMBER6;
	MPU_InitStruct.TypeExtField     = MPU_TEX_LEVEL1;
	MPU_InitStruct.SubRegionDisable = 0x00;
	MPU_InitStruct.DisableExec      = MPU_INSTRUCTION_ACCESS_ENABLE;
	
	HAL_MPU_ConfigRegion(&MPU_InitStruct);
	
	/* 配置用于LCD的SDRAM的MPU属性为Write through, read allocate，no write allocate */
	//由于LTDC会周期性的对此区域内存进行读操作，同时CPU与DMA2D也会周期性地进行写ram操作，
	//这时若使用write back策略就会出现显示异常, CPU的部分写操作会滞留在cache中，而由于这种
	//写操作比较频繁（基本上GUI的绘图函数都会使用到），不能保证及时的更新cache的数据到ram中。
	//因此将此区域直接设置为write through模式。CPU对此区域ram基本上没有读操作，有读操作时调用函数更新cache即可。
	MPU_InitStruct.Enable           = MPU_REGION_ENABLE;
	MPU_InitStruct.BaseAddress      = 0xC1E00000;
	MPU_InitStruct.Size             = MPU_REGION_SIZE_2MB;	
	MPU_InitStruct.AccessPermission = MPU_REGION_FULL_ACCESS;
	MPU_InitStruct.IsBufferable     = MPU_ACCESS_NOT_BUFFERABLE;
	MPU_InitStruct.IsCacheable      = MPU_ACCESS_CACHEABLE;
	MPU_InitStruct.IsShareable      = MPU_ACCESS_NOT_SHAREABLE;
	MPU_InitStruct.Number           = MPU_REGION_NUMBER7;
	MPU_InitStruct.TypeExtField     = MPU_TEX_LEVEL0;
	MPU_InitStruct.SubRegionDisable = 0x00;
	MPU_InitStruct.DisableExec      = MPU_INSTRUCTION_ACCESS_ENABLE;
	
	HAL_MPU_ConfigRegion(&MPU_InitStruct);
#endif
	/*使能 MPU */
	HAL_MPU_Enable(MPU_PRIVILEGED_DEFAULT);
}

//时钟设置函数
//Fvco=Fs*(plln/pllm);
//Fsys=Fvco/pllp=Fs*(plln/(pllm*pllp));
//Fq=Fvco/pllq=Fs*(plln/(pllm*pllq));

//Fvco:VCO频率
//Fsys:系统时钟频率,也是PLL1的p分频输出时钟频率
//Fq:PLL1的q分频输出时钟频率
//Fs:PLL输入时钟频率,可以是HSI,CSI,HSE等. 

//plln:PLL1倍频系数(PLL倍频),取值范围:4~512.
//pllm:PLL1预分频系数(进PLL之前的分频),取值范围:2~63.
//pllp:PLL1的p分频系数(PLL之后的分频),分频后作为系统时钟,取值范围:2~128.(且必须是2的倍数)
//pllq:PLL1的q分频系数(PLL之后的分频),取值范围:1~128.

//CPU频率(rcc_c_ck)=sys_d1cpre_ck=480Mhz 
//rcc_aclk=rcc_hclk3=240Mhz
//AHB1/2/3/4(rcc_hclk1/2/3/4)=240Mhz  
//APB1/2/3/4(rcc_pclk1/2/3/4)=120Mhz  
//FMC时钟频率=pll2_r_ck=((25/25)*512/2)=256Mhz

//外部晶振为25M的时候,推荐值:plln=160,pllm=5,pllp=2,pllq=2.
//得到:Fvco=25*(160/5)=800Mhz
//     Fsys=800/2=400Mhz
//     Fq=800/2=400Mhz
//返回值:0,成功;1,失败。
static void STM32_Clock_Init(void)
{
    HAL_StatusTypeDef ret=HAL_OK;
	RCC_ClkInitTypeDef RCC_ClkInitStruct={0};
	RCC_OscInitTypeDef RCC_OscInitStruct={0};
	RCC_PeriphCLKInitTypeDef RCC_PeriphStruct={0};
	/** Supply configuration update enable
	*/
	HAL_PWREx_ConfigSupply(PWR_LDO_SUPPLY);
	/** Configure the main internal regulator output voltage
	*/
	__HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE0);

	while(!__HAL_PWR_GET_FLAG(PWR_FLAG_VOSRDY)) {}
	/** Macro to configure the PLL clock source
	*/
	__HAL_RCC_PLL_PLLSOURCE_CONFIG(RCC_PLLSOURCE_HSE);
	/** Initializes the RCC Oscillators according to the specified parameters
	* in the RCC_OscInitTypeDef structure.
	*/
		
	RCC_OscInitStruct.OscillatorType=RCC_OSCILLATORTYPE_HSI48|RCC_OSCILLATORTYPE_HSE;
	RCC_OscInitStruct.HSEState=RCC_HSE_ON;
	RCC_OscInitStruct.HSIState=RCC_HSI_OFF;
	RCC_OscInitStruct.CSIState=RCC_CSI_OFF;
	RCC_OscInitStruct.LSEState=RCC_LSE_ON;
	RCC_OscInitStruct.HSI48State=RCC_HSI48_ON;	
	RCC_OscInitStruct.PLL.PLLFRACN = 0;
	
	RCC_OscInitStruct.PLL.PLLN=192;
	RCC_OscInitStruct.PLL.PLLM=5;
	RCC_OscInitStruct.PLL.PLLP=2;
	RCC_OscInitStruct.PLL.PLLQ=20;
	RCC_OscInitStruct.PLL.PLLR=2;
	RCC_OscInitStruct.PLL.PLLRGE = RCC_PLL1VCIRANGE_2;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
	RCC_OscInitStruct.PLL.PLLState	= RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLVCOSEL = RCC_PLL1VCOWIDE;
	
	ret=HAL_RCC_OscConfig(&RCC_OscInitStruct);
	if(ret!=HAL_OK) while(1);
  
	RCC_ClkInitStruct.ClockType=(RCC_CLOCKTYPE_SYSCLK|\
                                RCC_CLOCKTYPE_HCLK |\
                                RCC_CLOCKTYPE_D1PCLK1 |\
                                RCC_CLOCKTYPE_PCLK1 |\
                                RCC_CLOCKTYPE_PCLK2 |\
                                RCC_CLOCKTYPE_D3PCLK1);

	RCC_ClkInitStruct.SYSCLKSource=RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.SYSCLKDivider=RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.AHBCLKDivider=RCC_HCLK_DIV2;
	RCC_ClkInitStruct.APB1CLKDivider=RCC_APB1_DIV2; 
	RCC_ClkInitStruct.APB2CLKDivider=RCC_APB2_DIV2; 
	RCC_ClkInitStruct.APB3CLKDivider=RCC_APB3_DIV2;  
	RCC_ClkInitStruct.APB4CLKDivider=RCC_APB4_DIV2; 
	ret=HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4);
	if(ret!=HAL_OK) while(1);
	
	//PLL3给SAI1和LTDC,LTDC用40M
	RCC_PeriphStruct.PLL3.PLL3M=25;
	RCC_PeriphStruct.PLL3.PLL3N=406;
	RCC_PeriphStruct.PLL3.PLL3P=6;
	RCC_PeriphStruct.PLL3.PLL3Q=2;
	RCC_PeriphStruct.PLL3.PLL3R=10;
	
	RCC_PeriphStruct.PLL3.PLL3FRACN=0;
	RCC_PeriphStruct.PLL3.PLL3RGE = RCC_PLL3VCIRANGE_0;
	RCC_PeriphStruct.PLL3.PLL3VCOSEL = RCC_PLL3VCOMEDIUM;
	
	RCC_PeriphStruct.PeriphClockSelection=	RCC_PERIPHCLK_SAI1|RCC_PERIPHCLK_USART16|\
											RCC_PERIPHCLK_RNG|RCC_PERIPHCLK_FMC|RCC_PERIPHCLK_QSPI|\
											RCC_PERIPHCLK_SDMMC|RCC_PERIPHCLK_LTDC|RCC_PERIPHCLK_USB;
	
	RCC_PeriphStruct.Sai1ClockSelection=RCC_SAI1CLKSOURCE_PLL3;//67.66M
	RCC_PeriphStruct.Usart16ClockSelection = RCC_USART16CLKSOURCE_D2PCLK2;//120M
	RCC_PeriphStruct.RngClockSelection = RCC_RNGCLKSOURCE_PLL;//48M
	RCC_PeriphStruct.FmcClockSelection = RCC_FMCCLKSOURCE_D1HCLK;//240M
	RCC_PeriphStruct.QspiClockSelection = RCC_QSPICLKSOURCE_D1HCLK;//240M
	RCC_PeriphStruct.SdmmcClockSelection = RCC_SDMMCCLKSOURCE_PLL;//48M
	RCC_PeriphStruct.UsbClockSelection = RCC_USBCLKSOURCE_PLL;//48M
	ret=HAL_RCCEx_PeriphCLKConfig(&RCC_PeriphStruct);
	if(ret!=HAL_OK) while(1);
	

	__HAL_RCC_CSI_ENABLE();
	__HAL_RCC_SYSCFG_CLK_ENABLE();
	HAL_EnableCompensationCell();
	
	__HAL_RCC_D2SRAM1_CLK_ENABLE();
	__HAL_RCC_D2SRAM2_CLK_ENABLE();
	__HAL_RCC_D2SRAM3_CLK_ENABLE();
    __HAL_RCC_BKPRAM_CLKAM_ENABLE();       
	__HAL_RCC_D3SRAM1_CLKAM_ENABLE();	
	HAL_PWREx_EnableUSBVoltageDetector();
}


static void uart_gpio_init(void){
	GPIO_InitTypeDef GPIO_Initure;
	
	__HAL_RCC_GPIOA_CLK_ENABLE();			//使能GPIOA时钟

	GPIO_Initure.Pin=GPIO_PIN_9|GPIO_PIN_10;//PA9,PA10
	GPIO_Initure.Mode=GPIO_MODE_AF_PP;		//复用推挽输出
	GPIO_Initure.Pull=GPIO_PULLUP;			//上拉
	GPIO_Initure.Speed=GPIO_SPEED_MEDIUM;	//中速
	GPIO_Initure.Alternate=GPIO_AF7_USART1;	//复用为USART1
	HAL_GPIO_Init(GPIOA,&GPIO_Initure);	   	//初始化PA9	
}
//初始化IO 串口1 
//bound:波特率
static void uart_init(uint32_t bound)
{	
	__HAL_RCC_USART1_CLK_ENABLE();			//使能USART1时钟
	uart_gpio_init();
	//UART 初始化设置
	Debug_Handler.Instance=USART1;					    //USART1
	Debug_Handler.Init.BaudRate=bound;				    //波特率
	Debug_Handler.Init.WordLength=UART_WORDLENGTH_8B;   //字长为8位数据格式
	Debug_Handler.Init.StopBits=UART_STOPBITS_1;	    //一个停止位
	Debug_Handler.Init.Parity=UART_PARITY_NONE;		    //无奇偶校验位
	Debug_Handler.Init.HwFlowCtl=UART_HWCONTROL_NONE;   //无硬件流控
	Debug_Handler.Init.Mode=UART_MODE_TX_RX;		    	//发送模式
	Debug_Handler.Init.OverSampling=UART_OVERSAMPLING_16;//16倍过采样
	HAL_UART_Init(&Debug_Handler);					    //HAL_UART_Init()会使能UART1
	
	__HAL_UART_ENABLE_IT(&Debug_Handler,UART_IT_RXNE);
	HAL_NVIC_EnableIRQ(USART1_IRQn);				//使能USART1中断通道
	HAL_NVIC_SetPriority(USART1_IRQn,DEBUG_UART_ISR_PRIO,0);			
}

void Debug_Send_Data(const uint8_t* pdata,uint32_t len){
	while(len--){
		while(__HAL_UART_GET_FLAG(&Debug_Handler,UART_FLAG_TXE)==RESET);
		Debug_Handler.Instance->TDR=*pdata++;
	}
	while(__HAL_UART_GET_FLAG(&Debug_Handler,UART_FLAG_TC)==RESET);
}

void Debug_UnSafe_Printf(const char* fmt,...){
	static char print_buf[200];
	int len;
	va_list arg_list;
	va_start(arg_list,fmt);
	len=vsnprintf(print_buf,sizeof(print_buf),fmt,arg_list);
	va_end(arg_list);
	Debug_Send_Data((uint8_t*)print_buf,len);
}


__WEAK void Debug_Process_Data(uint8_t data){
	while((Debug_Handler.Instance->ISR&0X40)==0);//循环发送,直到发送完毕   
	Debug_Handler.Instance->TDR=(uint8_t)data;
}

void USART1_IRQHandler(void){
	if(__HAL_UART_GET_FLAG(&Debug_Handler,UART_FLAG_RXNE)!=RESET||
	   __HAL_UART_GET_FLAG(&Debug_Handler,UART_FLAG_ORE)!=RESET){
		Debug_Process_Data(Debug_Handler.Instance->RDR);
	}
}

static void DWT_Init(void){
	CoreDebug->DEMCR|=CoreDebug_DEMCR_TRCENA_Msk;
	DWT->CYCCNT=0;
	DWT->CTRL|=DWT_CTRL_CYCCNTENA_Msk;
}			   
//初始化延迟函数
//SYSCLK:系统时钟频率
static void delay_init(void)
{
	DWT_Init();
}								    

//延时nus
//nus:要延时的us数.	
//nus:0~204522252(最大值即2^32/fac_us@fac_us=21)
//这个函数是线程安全的，只要不在调用此函数期间被抢占8s以上即可
void delay_us(volatile uint32_t nus){	
	volatile uint32_t told=DWT->CYCCNT,tnow,tcnt=0;	      //刚进入时的计数器值	
	volatile uint32_t ticks=nus*(SystemCoreClock/1000000);//需要的节拍数 
	do{
		tnow=DWT->CYCCNT;	
		tcnt+=tnow-told;
		told=tnow;  
	}while(tcnt<ticks);								    
}  

//延时nms,不会引起任务调度
//nms:要延时的ms数
void delay_xms(volatile uint32_t nms)
{
	uint32_t i;
	for(i=0;i<nms;i++) 
		delay_us(1000);
}

#if SUPPORT_OS!=NO_RTOS
__STATIC_FORCEINLINE uint32_t OS_Get_Freq(void){
	#if SUPPORT_OS==FREE_RTOS
	return portTICK_PERIOD_MS;
	#elif SUPPORT_OS==RTX_RTOS
	static uint32_t freq_ms=0;
	if(freq_ms==0){
		freq_ms=osKernelGetTickFreq();
	}
	return freq_ms;
	#else
	return 1000;
	#endif
}
#endif

#if SUPPORT_OS==NO_RTOS

//systick中断服务函数,使用OS时用到
void SysTick_Handler(void)
{	
    HAL_IncTick();
}

//延时nms,会引起任务调度
//nms:要延时的ms数
//nms:0~65535
void delay_ms(volatile uint32_t nms)
{	
	delay_xms(nms);
}

#elif SUPPORT_OS==FREE_RTOS
//systick中断服务函数,使用OS时用到
void SysTick_Handler(void)
{	
	extern void xPortSysTickHandler(void);
	if(xTaskGetSchedulerState()!=taskSCHEDULER_NOT_STARTED)//系统已经运行
    {
        xPortSysTickHandler();	
    }
    HAL_IncTick();
}

//延时nms,会引起任务调度
//nms:要延时的ms数
//nms:0~65535
void delay_ms(volatile uint32_t nms)
{	
	if(xTaskGetSchedulerState()!=taskSCHEDULER_NOT_STARTED)//系统已经运行	
		vTaskDelay(nms*OS_Get_Freq()/1000);		
	else
		delay_xms(nms);
}

#elif SUPPORT_OS==RTX_RTOS
/* 采用TIM7方案或者RTX5内核时钟方案 */

static TIM_HandleTypeDef   TimHandle = {0};
static volatile uint32_t osCPU_Usage,osCPU_TotalIdleTime;
#define CALCULATION_PERIOD	1000
/**
  * @brief  Application Idle Hook
  * @param  None 
  * @retval None
  */
static void vApplicationTickHook (void)
{
  static int tick = 0;
  
  if(tick ++ > CALCULATION_PERIOD)
  {
    tick = 0;
    
    if(osCPU_TotalIdleTime > 1000)
    {
      osCPU_TotalIdleTime = 1000;
    }
    osCPU_Usage = (100 - (osCPU_TotalIdleTime * 100) / CALCULATION_PERIOD);
    osCPU_TotalIdleTime = 0;
  }
}

void vApplicationIdleHook(void){
	osCPU_TotalIdleTime++;
}

uint16_t osGetCPUUsage (void){
  return (uint16_t)osCPU_Usage;
}
/*
*********************************************************************************************************
*	函 数 名: HAL_InitTick
*	功能说明: 为HAL库配置1ms的时间基准，此函数会被HAL_Init和HAL_RCC_ClockConfig调用
*	形    参：TickPriority  定时器优先级
*	返 回 值: 无
*********************************************************************************************************
*/
HAL_StatusTypeDef HAL_InitTick (uint32_t TickPriority)
{

	RCC_ClkInitTypeDef    clkconfig;
	uint32_t              uwTimclock, uwAPB1Prescaler = 0U;
	uint32_t              uwPrescalerValue = 0U;
	uint32_t              pFLatency;

	
	/* 复位定时器7 */
	TimHandle.Instance = TIM7;
	HAL_TIM_Base_DeInit(&TimHandle);
	/* 设置TIM7的中断优先级并使能 */
	HAL_NVIC_SetPriority(TIM7_IRQn, TickPriority ,0U);
	HAL_NVIC_EnableIRQ(TIM7_IRQn);

	/* 使能TIM7时钟 */
	__HAL_RCC_TIM7_CLK_ENABLE();

	/*-----------------------------------------------------------------------
        System Clock source       = PLL (HSE)
        SYSCLK(Hz)                = 400000000 (CPU Clock)
        HCLK(Hz)                  = 200000000 (AXI and AHBs Clock)
        AHB Prescaler             = 2
        D1 APB3 Prescaler         = 2 (APB3 Clock  100MHz)
        D2 APB1 Prescaler         = 2 (APB1 Clock  100MHz)
        D2 APB2 Prescaler         = 2 (APB2 Clock  100MHz)
        D3 APB4 Prescaler         = 2 (APB4 Clock  100MHz)

        因为APB1 prescaler != 1, 所以 APB1上的TIMxCLK = APB1 x 2 = 200MHz;
        因为APB2 prescaler != 1, 所以 APB2上的TIMxCLK = APB2 x 2 = 200MHz;
        APB4上面的TIMxCLK没有分频，所以就是100MHz;

        APB1 定时器有 TIM2, TIM3 ,TIM4, TIM5, TIM6, TIM7, TIM12, TIM13, TIM14，LPTIM1
        APB2 定时器有 TIM1, TIM8 , TIM15, TIM16，TIM17

        APB4 定时器有 LPTIM2，LPTIM3，LPTIM4，LPTIM5
	----------------------------------------------------------------------- */
	/* 获取时钟配置 */
	HAL_RCC_GetClockConfig(&clkconfig, &pFLatency);

	/* 获取APB1时钟 */
	uwAPB1Prescaler = clkconfig.APB1CLKDivider;

	/* 获得TIM7时钟 */
	if (uwAPB1Prescaler == RCC_HCLK_DIV1) 
	{
		uwTimclock = HAL_RCC_GetPCLK1Freq();
	}
	else
	{
		uwTimclock = 2*HAL_RCC_GetPCLK1Freq();
	}

	/* TIM7分频到1MHz */
	uwPrescalerValue = (uint32_t) ((uwTimclock / 1000000U) - 1U);

	/* TIM7CLK = uwTimclock / (Period + 1) / (Prescaler + 1) = 1KHz */
	TimHandle.Init.Period = (1000000U / 1000U) - 1U;
	TimHandle.Init.Prescaler = uwPrescalerValue;
	TimHandle.Init.ClockDivision = 0;
	TimHandle.Init.CounterMode = TIM_COUNTERMODE_UP;
	if(HAL_TIM_Base_Init(&TimHandle) == HAL_OK)
	{
		/* 启动TIM7 */
		return HAL_TIM_Base_Start_IT(&TimHandle);
	}

	/* 返回错误 */
	return HAL_ERROR;
}

/*
*********************************************************************************************************
*	函 数 名: HAL_SuspendTick
*	功能说明: 关闭TIM7
*	形    参: 无
*	返 回 值: 无
*********************************************************************************************************
*/
void HAL_SuspendTick(void)
{
	__HAL_TIM_DISABLE_IT(&TimHandle, TIM_IT_UPDATE);
}

/*
*********************************************************************************************************
*	函 数 名: HAL_ResumeTick
*	功能说明: 使能TIM7
*	形    参: 无
*	返 回 值: 无
*********************************************************************************************************
*/
void HAL_ResumeTick(void)
{
	__HAL_TIM_ENABLE_IT(&TimHandle, TIM_IT_UPDATE);
}
/*
*********************************************************************************************************
*	函 数 名: TIM7_IRQHandler
*	功能说明: TIM7定时器中断服务程序
*	形    参: 无
*	返 回 值: 无
*********************************************************************************************************
*/


void TIM7_IRQHandler(void)
{
	if((TIM7->SR & TIM_FLAG_UPDATE) != RESET)
	{
		TIM7->SR = ~ TIM_FLAG_UPDATE;
		vApplicationTickHook();
		HAL_IncTick();
	}
}
//延时nms,会引起任务调度
//nms:要延时的ms数
//nms:0~65535
void delay_ms(volatile uint32_t nms)
{	
	if(osKernelGetState()==osKernelRunning)//系统已经运行	
		osDelay(nms*OS_Get_Freq()/1000);		
	else
		delay_xms(nms);
}

#endif

static void Basic_HardWare_Init(void){
	HAL_Init();
	STM32_Clock_Init();
	MPU_Set_Protection();
	SCB_EnableICache();
	SCB_EnableDCache();
	delay_init();
	uart_init(115200);
#if	USE_EXRAM
	HW_SDRAM_Init();
#endif
#if SUPPORT_OS==RTX_RTOS
	sys_heap_init();
#endif
#if defined(RTE_Compiler_EventRecorder)
	EventRecorderInitialize(EventRecordAll, 1U);
	EventRecorderStart();
#endif
}


extern void _platform_post_stackheap_init (void);
void _platform_post_stackheap_init (void){
#if SUPPORT_OS==RTX_RTOS
	osKernelInitialize();
#endif
	Basic_HardWare_Init();
}

void HAL_Delay(__IO uint32_t Delay){//HAL库的原本实现，依赖于中断，易出问题
	delay_ms(Delay/uwTickFreq);
}

#ifdef  USE_FULL_ASSERT
//当编译提示出错的时候此函数用来报告错误的文件和所在行
//file：指向源文件
//line：指向在文件中的行数
void assert_failed(uint8_t* file, uint32_t line)
{ 
	while (1)
	{
	}
}
#endif


/**
  * @brief  This function handles NMI exception.
  * @param  None
  * @retval None
  */
void NMI_Handler(void)
{
	Debug_UnSafe_Printf("NMI Error!\r\n");
	HAL_NVIC_SystemReset();
}

/**
  * @brief  This function handles Memory Manage exception.
  * @param  None
  * @retval None
  */
void MemManage_Handler(void)
{
  /* Go to infinite loop when Memory Manage exception occurs */
  while (1)
  {
	  Debug_UnSafe_Printf("MemManage error!\r\n");
	  delay_xms(1000);
  }
}

/**
  * @brief  This function handles Bus Fault exception.
  * @param  None
  * @retval None
  */
void BusFault_Handler(void)
{
  /* Go to infinite loop when Bus Fault exception occurs */
  while (1)
  {
	  Debug_UnSafe_Printf("BusFault error!\r\n");
	  delay_xms(1000);
  }
}

/**
  * @brief  This function handles Usage Fault exception.
  * @param  None
  * @retval None
  */
void UsageFault_Handler(void)
{
  /* Go to infinite loop when Usage Fault exception occurs */
  while (1)
  {
	  Debug_UnSafe_Printf("UsageFault error!\r\n");
	  delay_xms(1000);
  }
}
