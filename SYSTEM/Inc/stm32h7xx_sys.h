#ifndef STM32H7XX_SYS_H_
#define STM32H7XX_SYS_H_

#include "RTE_Components.h"
#include "stm32h7xx.h"
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdalign.h>
#include "atomic_cm.h"
#ifdef __cplusplus
#include <iostream>
#include <exception>
#include <vector>
#include <map>
#include <stack>
#include <string>
#include <list>
#include <array>
#include <algorithm>
#endif

#define SYS_VERSION				"V1.4.1"

#define NO_RTOS					0
#define FREE_RTOS				1
#define RTX_RTOS				2

#define SUPPORT_OS				RTX_RTOS		//定义系统文件夹是否支持OS
#define USE_EXRAM		1

#if SUPPORT_OS==FREE_RTOS
#include "FreeRTOS.h"
#include "task.h"
#include "event_groups.h"
#include "queue.h"
#include "semphr.h"
#include "timers.h"
#elif SUPPORT_OS==RTX_RTOS
#include <cmsis_os2.h>
#include <rtx_os.h>
#define __THREAD		__attribute__((section(".bss.os.thread.cb")))
#define __TIMER			__attribute__((section(".bss.os.timer.cb")))
#define __EVENT			__attribute__((section(".bss.os.evflags.cb")))
#define __MUTEX			__attribute__((section(".bss.os.mutex.cb")))
#define __SEMPHORE		__attribute__((section(".bss.os.semaphore.cb")))
#define __MEM_POOL		__attribute__((section(".bss.os.mempool.cb")))
#define __QUEUE			__attribute__((section(".bss.os.msgqueue.cb")))
#endif

#include "isr_prio_manage.h"
#if SUPPORT_OS!=NO_RTOS
#include "task_prio_manage.h"
#endif

#if defined(RTE_Compiler_EventRecorder)
#include <EventRecorder.h>
#endif

//声明不初始化的外部存储
//使用时注意，此声明所定义的变量的初值是随机的
//依赖于上次运行时写入的值
//使用外部SDRAM时应注意资源争夺的问题
#if defined(__clang__)
#if USE_EXRAM==1
//此处内存关闭cache，用于emwin及其他地方
#define __IN_EXRAM		__attribute__((section(".bss.EXRAM")))
//此处内存开启cahce，用于系统堆区
#define __IN_EXRAM_HEAP __attribute__((section(".bss.EXRAM_HEAP")))
#else
#define __IN_EXRAM_HEAP
#define __IN_EXRAM
#endif
#define __IN_AXI_RAM	__attribute__((section(".bss.AXI_RAM")))
#define __IN_SRAM1		__attribute__((section(".bss.SRAM1")))
#define __IN_SRAM2		__attribute__((section(".bss.SRAM2")))
#define __IN_SRAM3		__attribute__((section(".bss.SRAM3")))
#define __IN_SRAM4		__attribute__((section(".bss.SRAM4")))

#elif  defined(__CC_ARM)
#if USE_EXRAM==1
#define __IN_EXRAM		__attribute__((section(".bss.EXRAM"),zero_init))
#define __IN_EXRAM_HEAP __attribute__((section(".bss.EXRAM_HEAP"),zero_init))
#else
#define __IN_EXRAM
#define __IN_EXRAM_HEAP
#endif
#define __IN_AXI_RAM	__attribute__((section(".bss.AXI_RAM"),zero_init))
#define __IN_SRAM1		__attribute__((section(".bss.SRAM1"),zero_init))
#define __IN_SRAM2		__attribute__((section(".bss.SRAM2"),zero_init))
#define __IN_SRAM3		__attribute__((section(".bss.SRAM3"),zero_init))
#define __IN_SRAM4		__attribute__((section(".bss.SRAM4"),zero_init))
#else	
#define __IN_EXRAM
#define __IN_EXRAM_HEAP
#define __IN_AXI_RAM	
#define __IN_SRAM1		
#define __IN_SRAM2		
#define __IN_SRAM3		
#define __IN_SRAM4		
#endif

#ifdef __cplusplus
 extern "C" {
#endif

#if SUPPORT_OS!=NO_RTOS
uint16_t osGetCPUUsage (void);
#endif
void delay_ms(volatile uint32_t nms);
void delay_xms(volatile uint32_t nms);
void delay_us(volatile uint32_t nus);
extern void *aligned_alloc(size_t align_byte,size_t size);
extern void Debug_Process_Data(uint8_t data);
void Debug_Send_Data(const uint8_t* pdata,uint32_t len);
#if defined(__clang__)
__attribute__((format(printf,1,2)))
#endif
void Debug_UnSafe_Printf(const char* fmt,...);
	 
typedef struct{
	size_t free_node_num;	//空闲节点个数
	size_t used_node_num;	//分配节点个数
	size_t max_free_node_size;	//最大节点内存
	size_t min_free_node_size;	//最小节点内存
	size_t max_used_node_size;
	size_t min_used_node_size;
	size_t total_size;
	size_t free_size;
	size_t used_size;
}Sys_Heap_State;

//使用SVC指令实现互斥的内存分配
//malloc、realloc默认4字节对齐
__attribute__((always_inline)) static __inline void* sys_malloc(size_t want_size){
	register size_t __r0 __asm("r0") = (size_t)want_size;
	__asm volatile ("svc 1":"=r"(__r0):"r"(__r0):"cc","memory");
	return (void*)__r0;
}

__attribute__((always_inline)) static __inline void* sys_realloc(void* src_addr,size_t want_size){
	register size_t __r0 __asm("r0") = (size_t)src_addr;
	register size_t __r1 __asm("r1") = (size_t)want_size;
	__asm volatile ("svc 2":"=r"(__r0):"r"(__r0),"r"(__r1):"cc","memory");
	return (void*)__r0;
}

__attribute__((always_inline)) static __inline void* sys_aligned_alloc(size_t align_byte,size_t want_size){
	register size_t __r0 __asm("r0") = (size_t)align_byte;
	register size_t __r1 __asm("r1") = (size_t)want_size;
	__asm volatile ("svc 3":"=r"(__r0):"r"(__r0),"r"(__r1):"cc","memory");
	return (void*)__r0; 
}

__attribute__((always_inline)) static __inline void sys_free(void* addr){
	register size_t __r0 __asm("r0") = (size_t)addr;
	__asm volatile ("svc 4"::"r"(__r0):"cc","memory");
}

__attribute__((always_inline)) static __inline void sys_get_heap_state(Sys_Heap_State* pstate){
	register size_t __r0 __asm("r0") = (size_t)pstate;
	__asm volatile ("svc 5"::"r"(__r0):"cc","memory");
}

__attribute__((always_inline)) static __inline void rtos_get_heap_state(Sys_Heap_State* pstate){
	register size_t __r0 __asm("r0") = (size_t)pstate;
	__asm volatile ("svc 6"::"r"(__r0):"cc","memory");
}

#ifdef __cplusplus
}
#endif

#endif

