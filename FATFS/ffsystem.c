/*------------------------------------------------------------------------*/
/* Sample Code of OS Dependent Functions for FatFs                        */
/* (C)ChaN, 2018                                                          */
/*------------------------------------------------------------------------*/


#include "ff.h"
#include "stm32h7xx_sys.h"

#if FF_FS_NORTC==0
#include "hw_time.h"
DWORD get_fattime(void) {
	DWORD ptime=0;
	ptime=((RTC_Date.Year+20)<<25)+(RTC_Date.Month<<21)+(RTC_Date.Date<<16)+\
			(RTC_Time.Hours<<11)+(RTC_Time.Minutes<<5)+(RTC_Time.Seconds<<0);
	return ptime;

}
#endif
#if FF_USE_LFN == 3	/* Dynamic memory allocation */

/*------------------------------------------------------------------------*/
/* Allocate a memory block                                                */
/*------------------------------------------------------------------------*/

void* ff_memalloc (	/* Returns pointer to the allocated memory block (null if not enough core) */
	UINT msize		/* Number of bytes to allocate */
)
{
	return malloc(msize);	/* Allocate a new memory block with POSIX API */
}


/*------------------------------------------------------------------------*/
/* Free a memory block                                                    */
/*------------------------------------------------------------------------*/

void ff_memfree (
	void* mblock	/* Pointer to the memory block to free (nothing to do if null) */
)
{
	free(mblock);	/* Free the memory block with POSIX API */
}

#endif



#if FF_FS_REENTRANT	/* Mutal exclusion */

/*------------------------------------------------------------------------*/
/* Create a Synchronization Object                                        */
/*------------------------------------------------------------------------*/
/* This function is called in f_mount() function to create a new
/  synchronization object for the volume, such as semaphore and mutex.
/  When a 0 is returned, the f_mount() function fails with FR_INT_ERR.
*/

int ff_cre_syncobj (	/* 1:Function succeeded, 0:Could not create the sync object */
	BYTE vol,			/* Corresponding volume (logical drive number) */
	FF_SYNC_t* sobj		/* Pointer to return the created sync object */
)
{
#if SUPPORT_OS==RTX_RTOS
	static const osMutexAttr_t FatfsMutex_Attr={
		.attr_bits=osMutexPrioInherit|osMutexRobust,
		.name="Fatfs Mutex"
	};
	*sobj = osMutexNew(&FatfsMutex_Attr);
#elif SUPPORT_OS==FREE_RTOS
	*sobj = xSemaphoreCreateMutex();
#endif
	return (int)(*sobj != NULL);
}


/*------------------------------------------------------------------------*/
/* Delete a Synchronization Object                                        */
/*------------------------------------------------------------------------*/
/* This function is called in f_mount() function to delete a synchronization
/  object that created with ff_cre_syncobj() function. When a 0 is returned,
/  the f_mount() function fails with FR_INT_ERR.
*/

int ff_del_syncobj (	/* 1:Function succeeded, 0:Could not delete due to an error */
	FF_SYNC_t sobj		/* Sync object tied to the logical drive to be deleted */
)
{
#if SUPPORT_OS==RTX_RTOS
	return (int)(osMutexDelete(sobj)==osOK);
#elif SUPPORT_OS==FREE_RTOS
	vSemaphoreDelete(sobj);
	return 1;
#endif
}


/*------------------------------------------------------------------------*/
/* Request Grant to Access the Volume                                     */
/*------------------------------------------------------------------------*/
/* This function is called on entering file functions to lock the volume.
/  When a 0 is returned, the file function fails with FR_TIMEOUT.
*/

int ff_req_grant (	/* 1:Got a grant to access the volume, 0:Could not get a grant */
	FF_SYNC_t sobj	/* Sync object to wait */
)
{
#if SUPPORT_OS==RTX_RTOS
	return (int)(osMutexAcquire(sobj, FF_FS_TIMEOUT) == osOK);
#elif SUPPORT_OS==FREE_RTOS
	return (int)(xSemaphoreTake(sobj, FF_FS_TIMEOUT) == pdTRUE);
#endif
}


/*------------------------------------------------------------------------*/
/* Release Grant to Access the Volume                                     */
/*------------------------------------------------------------------------*/
/* This function is called on leaving file functions to unlock the volume.
*/

void ff_rel_grant (
	FF_SYNC_t sobj	/* Sync object to be signaled */
)
{
#if SUPPORT_OS==RTX_RTOS
	osMutexRelease(sobj);
#elif SUPPORT_OS==FREE_RTOS
	xSemaphoreGive(sobj);
#endif
}

#endif

