#include "fatfs.h"
#include <ctype.h>

static FATFS fs ;//逻辑磁盘工作区.	 
///////////////////////////////////////////////////////////////////////////////////////
//为exfuns申请内存
//返回值:0,成功
//1,失败

static bool is_init=false;
bool FATFS_Init(void)
{
	if(is_init)
		return true;
	if(f_mount(&fs,"0:",0)!=FR_OK){
		printf("SD卡无文件系统，即将初始化......\r\n");
		if(f_mkfs("0:",NULL,NULL,1024*2)==FR_OK){
			printf("SD文件系统格式化成功！\r\n");
		}
		else{
			printf("格式化失败，请检查SD卡!\r\n");
			return false;
		}
	}
	is_init=true;
	return true;	
}

void FATFS_UnInit(void){
	if(is_init==false)
		return;
	f_unmount("0:");
	is_init=false;
}

static int stringmatchlen(const char *pattern, int patternLen,\
	const char *string, int stringLen, int nocase)
{
    while(patternLen) {
        switch(pattern[0]) {
        case '*':
            while (pattern[1] == '*') {
                pattern++;
                patternLen--;
            }
            if (patternLen == 1)
                return 1; /** match */
            while(stringLen) {
                if (stringmatchlen(pattern+1, patternLen-1,
                            string, stringLen, nocase))
                    return 1; /** match */
                string++;
                stringLen--;
            }
            return 0; /** no match */
            break;
        case '?':
            if (stringLen == 0)
                return 0; /** no match */
            string++;
            stringLen--;
            break;
        case '[':
        {
            int inot, match;

            pattern++;
            patternLen--;
            inot = pattern[0] == '^';
            if (inot) {
                pattern++;
                patternLen--;
            }
            match = 0;
            while(1) {
                if (pattern[0] == '\\') {
                    pattern++;
                    patternLen--;
                    if (pattern[0] == string[0])
                        match = 1;
                } else if (pattern[0] == ']') {
                    break;
                } else if (patternLen == 0) {
                    pattern--;
                    patternLen++;
                    break;
                } else if (pattern[1] == '-' && patternLen >= 3) {
                    int start = pattern[0];
                    int end = pattern[2];
                    int c = string[0];
                    if (start > end) {
                        int t = start;
                        start = end;
                        end = t;
                    }
                    if (nocase) {
                        start = tolower(start);
                        end = tolower(end);
                        c = tolower(c);
                    }
                    pattern += 2;
                    patternLen -= 2;
                    if (c >= start && c <= end)
                        match = 1;
                } else {
                    if (!nocase) {
                        if (pattern[0] == string[0])
                            match = 1;
                    } else {
                        if (tolower((int)pattern[0]) == tolower((int)string[0]))
                            match = 1;
                    }
                }
                pattern++;
                patternLen--;
            }
            if (inot)
                match = !match;
            if (!match)
                return 0; /** no match */
            string++;
            stringLen--;
            break;
        }
        case '\\':
            if (patternLen >= 2) {
                pattern++;
                patternLen--;
            }
            /** fall through */
        default:
            if (!nocase) {
                if (pattern[0] != string[0])
                    return 0; /** no match */
            } else {
                if (tolower((int)pattern[0]) != tolower((int)string[0]))
                    return 0; /** no match */
            }
            string++;
            stringLen--;
            break;
        }
        pattern++;
        patternLen--;
        if (stringLen == 0) {
            while(*pattern == '*') {
                pattern++;
                patternLen--;
            }
            break;
        }
    }
    if (patternLen == 0 && stringLen == 0)
        return 1;
    return 0;
}

static int stringmatch(const char *pattern, const char *string, int nocase) {
    return stringmatchlen(pattern,strlen(pattern),string,strlen(string),nocase);
}

static int use_match(const File_Filter* pfilter, const char *string, int nocase){
	int str_len=strlen(string);
	for(size_t i=0;i<pfilter->pattern_num;i++){
		if(stringmatchlen(pfilter->pattern[i],strlen(pfilter->pattern[i]),string,str_len,nocase))
			return 1;
	}
	return 0;
}

static bool Dir_Scan_Sub(const char* dir_path,const char* sub_path,const File_Filter* pfilter,File_Dir* const proot,uint8_t mode){
	FRESULT 	res=FR_OK;
	FILINFO* 	fileinfo=(FILINFO*)malloc(sizeof(FILINFO));	//文件信息
	DIR* 		dir=(DIR*)malloc(sizeof(DIR));  			//目录
	File_Node*  buf_node;
	char*		full_path=NULL;
	uint32_t    sub_path_len=strlen(sub_path);
	uint32_t    dir_path_len=strlen(dir_path);
	if(fileinfo==NULL||dir==NULL){
		goto fail0;
	}
	
	if(sub_path_len!=0){
		size_t buf_len=sub_path_len+dir_path_len+2;
		full_path=(char*)malloc(buf_len);
		if(full_path==NULL){
			goto fail0;
		}
		strncpy(full_path,dir_path,buf_len);
		strncat(strncat(full_path,"/",buf_len),sub_path,buf_len);
	}
	else{
		size_t buf_len=dir_path_len+1;
		full_path=(char*)malloc(buf_len);
		if(full_path==NULL){
			goto fail0;
		}
		strncpy(full_path,dir_path,buf_len);
	}
	res=f_opendir(dir,full_path);
	if(res!=FR_OK){
		printf("目录%s打开失败！\r\n",full_path);
		goto fail1;
	}
	res=f_rewinddir(dir);
	if(res!=FR_OK){
		printf("目录%s打开失败！\r\n",full_path);
		goto fail2;
	}
	while(true){
		res = f_readdir(dir, fileinfo);     //读取目录下的一个文件
		if (res != FR_OK || !fileinfo->fname[0]){
			break;  						//错误了/到末尾了,退出
		}
		if(fileinfo->fattrib&AM_DIR){
			char* sub_buf_path;
			if(mode&DS_NORECUSIVE)
				continue;
			if(sub_path_len!=0){
				size_t buf_len=sub_path_len+strlen(fileinfo->fname)+2;
				sub_buf_path=malloc(buf_len);
				if(sub_buf_path==NULL){
					goto fail2;
				}
				strncpy(sub_buf_path,sub_path,buf_len);
				strncat(strncat(sub_buf_path,"/",buf_len),fileinfo->fname,buf_len);
			}
			else{
				size_t buf_len=strlen(fileinfo->fname)+1;
				sub_buf_path=malloc(buf_len);
				if(sub_buf_path==NULL){
					goto fail2;
				}
				strncpy(sub_buf_path,fileinfo->fname,buf_len);
			}
			if(Dir_Scan_Sub(dir_path,sub_buf_path,pfilter,proot,mode)==false){
				free(sub_buf_path);
				goto fail2;
			}
			free(sub_buf_path);
			continue;
		}
		if(use_match(pfilter,fileinfo->fname,mode&DS_NOCASE)){
			uint32_t name_len=strlen(fileinfo->fname);
			buf_node=malloc(sizeof(File_Node));
			if(buf_node==NULL){
				goto fail2;
			}
			if(sub_path_len!=0){
				size_t buf_len=name_len+sub_path_len+2;
				buf_node->data=(char*)malloc(buf_len);
				if(buf_node->data==NULL){
					goto fail3;
				}
				strncpy(buf_node->data,sub_path,buf_len);
				strncat(strncat(buf_node->data,"/",buf_len),fileinfo->fname,buf_len);
			}
			else{
				size_t buf_len=name_len+1;
				buf_node->data=(char*)malloc(buf_len);
				if(buf_node->data==NULL){
					goto fail3;
				}
				strncpy(buf_node->data,fileinfo->fname,buf_len);
			}
			My_List_Insert_End(proot,buf_node);
		}
	}
	
	f_closedir(dir);
	free(full_path);
	free(dir);
	free(fileinfo);
	return true;
fail3:
	free(buf_node);
fail2:
	Dir_Free(proot);
	f_closedir(dir);
fail1:
	free(full_path);
fail0:
	free(dir);
	free(fileinfo);
	return false;
}

//扫描目录下指定命名规则的文件
//dir_path:目录路径，形如0:/test
//pattern：文件名匹配模板，正则表达式表示，形如："*.txt"，表示扫描以.txt为后缀名的文件
//proot：文件名存储的链表
//scan_mode：扫描模式 DS_DEFAULT 默认的扫描方式
//					  DS_NOCASE  不关心文件名的大小写
//					  DS_NORECUSIVE 不进行递归搜索，遇到目录中的目录会直接跳过
//返回值：true 扫描成功
//		  false 扫描失败
//注：在递归搜索中，文件会表示为a.txt的形式，子目录下的匹配文件会表示为dir/b.txt的形式
//	  扫描完成后，可以通过遍历链表的方式获取所有文件名
bool Dir_Scan(const char* dir_path,const File_Filter* pfilter,File_Dir* const proot,uint8_t scan_mode){
	My_List_Init(proot);
	return Dir_Scan_Sub(dir_path,"",pfilter,proot,scan_mode);
}

//释放由Dir_Scan分配的内存
//proot：文件名存储的链表
void Dir_Free(File_Dir* const proot){
	File_Node* buf_node=My_List_Get_Start(proot);
	while(My_List_Remove_Item(buf_node)){
		free(buf_node->data);
		free(buf_node);
		buf_node=My_List_Get_Start(proot);
	}
}

void Dir_File_Filter_Destroy(File_Filter* dest_filter){
	for(size_t i=0;i<dest_filter->pattern_num;i++){
		free((char*)dest_filter->pattern[i]);
	}
	free(dest_filter->pattern);
	dest_filter->pattern=NULL;
	dest_filter->pattern_num=0;
}


bool Dir_File_Filter_Copy(File_Filter* dest_filter,const File_Filter* src_filter){
	if(dest_filter->pattern_num!=0){
		Dir_File_Filter_Destroy(dest_filter);
	}
	dest_filter->pattern=malloc(sizeof(char*)*src_filter->pattern_num);
	if(dest_filter->pattern==NULL){
		goto fail0;
	}
	for(size_t i=0;i<src_filter->pattern_num;i++){
		dest_filter->pattern[i]=malloc(strlen(src_filter->pattern[i])+1);
		if(dest_filter->pattern[i]==NULL){
			goto fail1;
		}
		strcpy((char*)dest_filter->pattern[i],src_filter->pattern[i]);
		dest_filter->pattern_num++;
	}
	return true;
fail1:
	Dir_File_Filter_Destroy(dest_filter);
fail0:	
	return false;
}

bool Dir_File_Filter_Add(File_Filter* dest_filter,const char* pattern){
	const char** buf;
	buf=realloc(dest_filter->pattern,(dest_filter->pattern_num+1)*sizeof(char*));
	if(buf==NULL){
		goto fail0;
	}
	dest_filter->pattern=buf;
	
	dest_filter->pattern[dest_filter->pattern_num]=malloc(strlen(pattern)+1);
	if(dest_filter->pattern[dest_filter->pattern_num]==NULL){
		goto fail0;
	}
	strcpy((char*)dest_filter->pattern[dest_filter->pattern_num],pattern);
	dest_filter->pattern_num++;
	return true;
fail0:	
	return false;
}

//将Dir_Scan使用的链表转换为数组
bool Dir_To_Array(File_Dir* proot,File_Array* parray){
	size_t i=0;
	parray->file_num=Dir_Get_FileNum(proot);
	if(parray->file_num==0){
		parray->name_buf=NULL;
		return true;
	}
	parray->name_buf=malloc(sizeof(const char*)*parray->file_num);
	if(parray->name_buf==NULL){
		parray->file_num=0;
		return false;
	}
	TRAVERSE_MY_LIST(pItem,proot){
		parray->name_buf[i]=pItem->data;
		pItem->data=NULL;
		i++;
	}
	Dir_Free(proot);
	return true;
}

//释放Dir_To_Array类型的数组
void Dir_Array_Free(File_Array* parray){
	for(size_t i=0;i<parray->file_num;i++){
		free((void*)parray->name_buf[i]);
	}
	free(parray->name_buf);
}

//得到磁盘剩余容量
//drv:磁盘编号("0:"/"1:")
//total:总容量	 （单位KB）
//free:剩余容量	 （单位KB）
//返回值:0,正常.其他,错误代码
bool FATFS_GetFree(const char *drv,uint64_t *total,uint64_t *free)
{
	FATFS *fs1;
	FRESULT res;
    uint32_t fre_clust=0, fre_sect=0, tot_sect=0;
    //得到磁盘信息及空闲簇数量
    res =f_getfree(drv, (DWORD*)&fre_clust, &fs1);
    if(res==FR_OK)
	{											   
	    tot_sect=(fs1->n_fatent-2)*fs1->csize;	//得到总扇区数
	    fre_sect=fre_clust*fs1->csize;			//得到空闲扇区数	   
#if FF_MAX_SS!=512				  				//扇区大小不是512字节,则转换为512字节
		tot_sect*=fs1->ssize/512;
		fre_sect*=fs1->ssize/512;
#endif	  
		*total=tot_sect>>1;	//单位为KB
		*free=fre_sect>>1;	//单位为KB 
		return true;
 	}
	else
		return false;
}		   
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//文件复制
//注意文件大小不要超过4GB.
//将psrc文件,copy到pdst.
//fcpymsg,函数指针,用于实现拷贝时的信息显示
//        pname:文件/文件夹名
//        pct:百分比
//        mode:
//			[0]:更新文件名
//			[1]:更新百分比pct
//			[2]:更新文件夹
//			[3~7]:保留
//psrc,pdst:源文件和目标文件
//totsize:总大小(当totsize为0的时候,表示仅仅为单个文件拷贝)
//cpdsize:已复制了的大小.
//fwmode:文件写入模式
//0:不覆盖原有的文件
//1:覆盖原有的文件
//返回值:0,正常
//    其他,错误,0XFF,强制退出
uint32_t exf_copy(uint8_t(*fcpymsg)(char*pname,uint8_t pct,uint8_t mode),char *psrc\
					,char *pdst,uint32_t totsize,uint32_t cpdsize,uint8_t fwmode)
{
	uint8_t res;
    uint16_t br=0;
	uint16_t bw=0;
	FIL *fsrc=0;
	FIL *fdst=0;
	char *fbuf=0;
	uint8_t curpct=0;
	unsigned long long lcpdsize=cpdsize; 
	fsrc=(FIL*)malloc(sizeof(FIL));
	fdst=(FIL*)malloc(sizeof(FIL));
	fbuf=(char*)malloc(8192);
  	if(fsrc==NULL||fdst==NULL||fbuf==NULL)res=100;//前面的值留给fatfs
	else
	{   
		if(fwmode==0)fwmode=FA_CREATE_NEW;//不覆盖
		else fwmode=FA_CREATE_ALWAYS;	  //覆盖存在的文件
		 
	 	res=f_open(fsrc,(const TCHAR*)psrc,FA_READ|FA_OPEN_EXISTING);	//打开只读文件
	 	if(res==0)res=f_open(fdst,(const TCHAR*)pdst,FA_WRITE|fwmode); 	//第一个打开成功,才开始打开第二个
		if(res==0)//两个都打开成功了
		{
			if(totsize==0)//仅仅是单个文件复制
			{
				totsize=f_size(fsrc);
				lcpdsize=0;
				curpct=0;
		 	}else curpct=(lcpdsize*100)/totsize;	//得到新百分比
			fcpymsg(psrc,curpct,0X02);			//更新百分比
			while(res==0)//开始复制
			{
				res=f_read(fsrc,fbuf,8192,(UINT*)&br);	//源头读出512字节
				if(res||br==0)break;
				res=f_write(fdst,fbuf,(UINT)br,(UINT*)&bw);	//写入目的文件
				lcpdsize+=bw;
				if(curpct!=(lcpdsize*100)/totsize)//是否需要更新百分比
				{
					curpct=(lcpdsize*100)/totsize;
					if(fcpymsg(psrc,curpct,0X02))//更新百分比
					{
						res=0XFF;//强制退出
						break;
					}
				}			     
				if(res||bw<br)break;       
			}
		    f_close(fsrc);
		    f_close(fdst);
		}
	}
	free(fsrc);
	free(fdst);
	free(fbuf);
	return res;
}

//得到路径下的文件夹
//返回值:0,路径就是个卷标号.
//    其他,文件夹名字首地址
char* exf_get_src_dname(char* dpfn)
{
	uint16_t temp=0;
 	while(*dpfn!=0)
	{
		dpfn++;
		temp++;	
	}
	if(temp<4)return NULL; 
	while((*dpfn!=0x5c)&&(*dpfn!=0x2f))dpfn--;	//追述到倒数第一个"\"或者"/"处 
	return ++dpfn;
}
//得到文件夹大小
//注意文件夹大小不要超过4GB.
//返回值:0,文件夹大小为0,或者读取过程中发生了错误.
//    其他,文件夹大小.
uint32_t exf_fdsize(char *fdname)
{
#define MAX_PATHNAME_DEPTH	512+1	//最大目标文件路径+文件名深度
	uint8_t res=0;	  
    DIR *fddir=0;		//目录
	FILINFO *finfo=0;	//文件信息
	char * pathname=0;	//目标文件夹路径+文件名
 	uint16_t pathlen=0;		//目标路径长度
	uint32_t fdsize=0;

	fddir=(DIR*)malloc(sizeof(DIR));//申请内存
 	finfo=(FILINFO*)malloc(sizeof(FILINFO));
   	if(fddir==NULL||finfo==NULL)res=100;
	if(res==0)
	{ 
 		pathname=malloc(MAX_PATHNAME_DEPTH);	    
 		if(pathname==NULL)res=101;	   
 		if(res==0)
		{
			pathname[0]=0;	    
			strcat((char*)pathname,(const char*)fdname); //复制路径	
		    res=f_opendir(fddir,(const TCHAR*)fdname); 		//打开源目录
		    if(res==0)//打开目录成功 
			{														   
				while(res==0)//开始复制文件夹里面的东东
				{
			        res=f_readdir(fddir,finfo);						//读取目录下的一个文件
			        if(res!=FR_OK||finfo->fname[0]==0)break;		//错误了/到末尾了,退出
			        if(finfo->fname[0]=='.')continue;     			//忽略上级目录
					if(finfo->fattrib&0X10)//是子目录(文件属性,0X20,归档文件;0X10,子目录;)
					{
 						pathlen=strlen((const char*)pathname);		//得到当前路径的长度
						strcat((char*)pathname,(const char*)"/");	//加斜杠
						strcat((char*)pathname,(const char*)finfo->fname);	//源路径加上子目录名字
 						//printf("\r\nsub folder:%s\r\n",pathname);	//打印子目录名
						fdsize+=exf_fdsize(pathname);				//得到子目录大小,递归调用
						pathname[pathlen]=0;						//加入结束符
					}else fdsize+=finfo->fsize;						//非目录,直接加上文件的大小
						
				} 
		    }	  
  			free(pathname);	     
		}
 	}
	free(fddir);    
	free(finfo);
	if(res)return 0;
	else return fdsize;
}	  
//文件夹复制
//注意文件夹大小不要超过4GB.
//将psrc文件夹,copy到pdst文件夹.
//pdst:必须形如"X:"/"X:XX"/"X:XX/XX"之类的.而且要实现确认上一级文件夹存在
//fcpymsg,函数指针,用于实现拷贝时的信息显示
//        pname:文件/文件夹名
//        pct:百分比
//        mode:
//			[0]:更新文件名
//			[1]:更新百分比pct
//			[2]:更新文件夹
//			[3~7]:保留
//psrc,pdst:源文件夹和目标文件夹
//totsize:总大小(当totsize为0的时候,表示仅仅为单个文件拷贝)
//cpdsize:已复制了的大小.
//fwmode:文件写入模式
//0:不覆盖原有的文件
//1:覆盖原有的文件
//返回值:0,成功
//    其他,错误代码;0XFF,强制退出
uint8_t exf_fdcopy(uint8_t(*fcpymsg)(char*pname,uint8_t pct,uint8_t mode),char *psrc,char *pdst,\
						uint32_t *totsize,uint32_t *cpdsize,uint8_t fwmode)
{
#define MAX_PATHNAME_DEPTH	512+1	//最大目标文件路径+文件名深度
	uint8_t res=0;	  
    DIR *srcdir=0;		//源目录
	DIR *dstdir=0;		//源目录
	FILINFO *finfo=0;	//文件信息
	char *fn=0;   		//长文件名

	char * dstpathname=0;	//目标文件夹路径+文件名
	char * srcpathname=0;	//源文件夹路径+文件名
	
 	uint16_t dstpathlen=0;	//目标路径长度
 	uint16_t srcpathlen=0;	//源路径长度

  
	srcdir=(DIR*)malloc(sizeof(DIR));//申请内存
 	dstdir=(DIR*)malloc(sizeof(DIR));
	finfo=(FILINFO*)malloc(sizeof(FILINFO));

   	if(srcdir==NULL||dstdir==NULL||finfo==NULL)res=100;
	if(res==0)
	{ 
 		dstpathname=malloc(MAX_PATHNAME_DEPTH);
		srcpathname=malloc(MAX_PATHNAME_DEPTH);
 		if(dstpathname==NULL||srcpathname==NULL)res=101;	   
 		if(res==0)
		{
			dstpathname[0]=0;
			srcpathname[0]=0;
			strcat((char*)srcpathname,(const char*)psrc); 	//复制原始源文件路径	
			strcat((char*)dstpathname,(const char*)pdst); 	//复制原始目标文件路径	
		    res=f_opendir(srcdir,(const TCHAR*)psrc); 		//打开源目录
		    if(res==0)//打开目录成功 
			{
  				strcat((char*)dstpathname,(const char*)"/");//加入斜杠
 				fn=exf_get_src_dname(psrc);
				if(fn==0)//卷标拷贝
				{
					dstpathlen=strlen((const char*)dstpathname);
					dstpathname[dstpathlen]=psrc[0];	//记录卷标
					dstpathname[dstpathlen+1]=0;		//结束符 
				}else strcat((char*)dstpathname,(const char*)fn);//加文件名		
 				fcpymsg(fn,0,0X04);//更新文件夹名
				res=f_mkdir((const TCHAR*)dstpathname);//如果文件夹已经存在,就不创建.如果不存在就创建新的文件夹.
				if(res==FR_EXIST)res=0;
				while(res==0)//开始复制文件夹里面的东东
				{
			        res=f_readdir(srcdir,finfo);					//读取目录下的一个文件
			        if(res!=FR_OK||finfo->fname[0]==0)break;		//错误了/到末尾了,退出
			        if(finfo->fname[0]=='.')continue;     			//忽略上级目录
					fn=(char*)finfo->fname; 							//得到文件名
					dstpathlen=strlen((const char*)dstpathname);	//得到当前目标路径的长度
					srcpathlen=strlen((const char*)srcpathname);	//得到源路径长度

					strcat((char*)srcpathname,(const char*)"/");//源路径加斜杠
 					if(finfo->fattrib&0X10)//是子目录(文件属性,0X20,归档文件;0X10,子目录;)
					{
						strcat((char*)srcpathname,(const char*)fn);		//源路径加上子目录名字
						res=exf_fdcopy(fcpymsg,srcpathname,dstpathname,totsize,cpdsize,fwmode);	//拷贝文件夹
					}else //非目录
					{
						strcat((char*)dstpathname,(const char*)"/");//目标路径加斜杠
						strcat((char*)dstpathname,(const char*)fn);	//目标路径加文件名
						strcat((char*)srcpathname,(const char*)fn);	//源路径加文件名
 						fcpymsg(fn,0,0X01);//更新文件名
						res=exf_copy(fcpymsg,srcpathname,dstpathname,*totsize,*cpdsize,fwmode);//复制文件
						*cpdsize+=finfo->fsize;//增加一个文件大小
					}
					srcpathname[srcpathlen]=0;//加入结束符
					dstpathname[dstpathlen]=0;//加入结束符	    
				} 
		    }	  
  			free(dstpathname);
 			free(srcpathname); 
		}
 	}
	free(srcdir);
	free(dstdir);
	free(finfo);
    return res;	  
}


