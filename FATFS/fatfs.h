#ifndef __FATFS_H
#define __FATFS_H
#include "stm32h7xx_sys.h"
#include "ff.h"
#include "my_list.h"

typedef My_List_Root File_Dir;
typedef My_List_Item File_Node;

typedef struct{
	const char** name_buf;
	uint32_t file_num;
}File_Array;

typedef struct{
	const char** pattern;
	uint32_t pattern_num;
}File_Filter;

#define DS_DEFAULT		0
#define DS_NOCASE		1
#define DS_NORECUSIVE	2
#ifdef __cplusplus
 extern "C" {
#endif
bool FATFS_Init(void);
void FATFS_UnInit(void);
bool FATFS_GetFree(const char *drv,uint64_t *total,uint64_t *free);
bool Dir_Scan(const char* dir_path,const File_Filter* pfilter,File_Dir* const proot,uint8_t scan_mode);
void Dir_Free(File_Dir* const proot);
void Dir_File_Filter_Destroy(File_Filter* dest_filter);
bool Dir_File_Filter_Copy(File_Filter* dest_filter,const File_Filter* src_filter);
bool Dir_File_Filter_Add(File_Filter* dest_filter,const char* pattern);
bool Dir_To_Array(File_Dir* proot,File_Array* parray);
void Dir_Array_Free(File_Array* parray);
static inline const char* Dir_Get_FileName(const File_Node* pItem){
	return (const char*)pItem->data;
}
static inline uint32_t Dir_Get_FileNum(const File_Dir* pRoot){
	return My_List_Get_Item_Num(pRoot);
}
	 
#ifdef __cplusplus
}
#endif
#endif
