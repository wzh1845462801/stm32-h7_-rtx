#include "lv_music_spectrum_ex.h"
#include "arm_math.h"
#include <stdlib.h>
#include "music_device.h"
#include "lv_music_ctrl_box_ex.h"

#define MAX_Y_VALUE		30			//单位db
#define MIN_Y_VALUE		-120		//单位db
#define MAX_SPEC_NUM	10		
#define FRESH_PERIOD	200
#define FFT_NUM			256


typedef struct{
	lv_obj_t* p_box;
	lv_obj_t* p_chart;
	lv_timer_t* p_fresh_timer;
	lv_chart_series_t* p_seri; 
}Music_Spectrum_Data_Type;

static inline float pow2(float a){
	return a*a;
}

//刷新频谱显示
//采用波特图式频谱，频率与幅值都取对数
static void music_spectrum_fresh(Music_Spectrum_Data_Type* pdata){
	Music_BufType buf;
	arm_rfft_fast_instance_f32 ins;
	float32_t* fft_in_buf; 
	float32_t* fft_out_buf;
	lv_coord_t* p_cod=lv_chart_get_y_array(pdata->p_chart,pdata->p_seri);
	
	const float scale_fac=((FFT_NUM/2)-1)/log10f(MAX_SPEC_NUM);
	const size_t music_buf_size = FFT_NUM*4;
	const size_t fft_in_buf_size = FFT_NUM*sizeof(float32_t);
	const size_t fft_out_buf_size = fft_in_buf_size*2;//复数需要扩充一倍的范围
	
	if(pdata->p_box==NULL){
		goto fail0;
	}
	
	buf.buf_addr=malloc(music_buf_size);
	buf.buf_size=music_buf_size;
	fft_in_buf=malloc(fft_in_buf_size);
	fft_out_buf=malloc(fft_out_buf_size);
	
	if(buf.buf_addr==NULL||fft_in_buf==NULL||fft_out_buf==NULL){
		goto fail1;
	}
	
	
	if(lv_music_ctrl_box_ex_get_now_buf(pdata->p_box,&buf)==false){
		goto fail1;
	}
	
	if(buf.buf_size<music_buf_size){
		memset((uint8_t*)buf.buf_addr+buf.buf_size,0,music_buf_size-buf.buf_size);
	}
	
	switch(buf.data_size){
	case 16:
		arm_q15_to_float(buf.buf_addr,fft_in_buf,FFT_NUM);
		break;
	case 24:
	case 32:
		arm_q31_to_float(buf.buf_addr,fft_in_buf,FFT_NUM);
		break;
	default:
		printf("音乐位数不支持，无法进行fft变换\r\n");
		goto fail1;
	}
		
	arm_rfft_fast_init_f32(&ins,FFT_NUM);
	arm_rfft_fast_f32(&ins,fft_in_buf,fft_out_buf,0);
		
	for(size_t i=0;i<MAX_SPEC_NUM;i++){
		//计算频率点，采用对数坐标
		size_t j=scale_fac*log10f(i+1);
		//计算频谱幅值，采用对数坐标
		p_cod[i]=10*log10f(pow2(fft_out_buf[j<<1]/(j==0?FFT_NUM:FFT_NUM/2))+\
						  pow2(fft_out_buf[(j<<1)+1]/(j==0?FFT_NUM:FFT_NUM/2)));
	}
	lv_chart_refresh(pdata->p_chart);
	
	free(fft_in_buf);
	free(fft_out_buf);
	free(buf.buf_addr);
	return;
fail1:
	free(fft_in_buf);
	free(fft_out_buf);
	free(buf.buf_addr);
fail0:
	memset(p_cod,0,MAX_SPEC_NUM*sizeof(lv_coord_t));
	lv_chart_refresh(pdata->p_chart);
	return;
}

static void music_spectrum_timer_cb(lv_timer_t* ptimer){
	music_spectrum_fresh(ptimer->user_data);
}

static void music_spectrum_for_box_del(lv_obj_t* p_box,void* p_spectrum){
	Music_Spectrum_Data_Type* pdata=lv_obj_get_user_data(p_spectrum);
	pdata->p_box=NULL;
}

static void music_chart_delete(lv_event_t* e){
	lv_obj_t* p_spectrum=lv_event_get_target(e);
	Music_Spectrum_Data_Type* pdata=lv_obj_get_user_data(p_spectrum);
	if(pdata->p_box!=NULL){
		lv_music_ctrl_box_ex_remove_delete_cb(pdata->p_box,music_spectrum_for_box_del,p_spectrum);
	}
	lv_timer_del(pdata->p_fresh_timer);
	free(pdata);
}


lv_obj_t* lv_music_spectrum_ex_create(lv_obj_t* parent){
	Music_Spectrum_Data_Type* pdata;
	
	pdata=malloc(sizeof(Music_Spectrum_Data_Type));
	if(pdata==NULL){
		goto fail0;
	}
	memset(pdata,0,sizeof(Music_Spectrum_Data_Type));
	
	
	pdata->p_chart=lv_chart_create(parent);
	if(pdata->p_chart==NULL){
		goto fail1;
	}
	lv_obj_set_user_data(pdata->p_chart,pdata);
	
	pdata->p_fresh_timer=lv_timer_create(music_spectrum_timer_cb,FRESH_PERIOD,pdata);
	if(pdata->p_fresh_timer==NULL){
		goto fail2;
	}
	
	lv_chart_set_type(pdata->p_chart,LV_CHART_TYPE_BAR);
	lv_chart_set_range(pdata->p_chart,LV_CHART_AXIS_PRIMARY_Y,MIN_Y_VALUE,MAX_Y_VALUE);
	lv_chart_set_point_count(pdata->p_chart,MAX_SPEC_NUM);
	lv_chart_set_div_line_count(pdata->p_chart,0,0);
	lv_obj_add_event_cb(pdata->p_chart,music_chart_delete,LV_EVENT_DELETE,NULL);
	
	pdata->p_seri=lv_chart_add_series(pdata->p_chart,lv_theme_get_color_primary(pdata->p_chart),LV_CHART_AXIS_PRIMARY_Y);
	if(pdata->p_seri==NULL){
		goto fail3;
	}
	music_spectrum_fresh(pdata);
	
	
	return pdata->p_chart;
fail3:
	lv_timer_del(pdata->p_fresh_timer);
fail2:
	lv_obj_del(pdata->p_chart);
fail1:	
	free(pdata);
fail0:	
	return NULL;
}

void lv_music_spectrum_ex_link_box(lv_obj_t* p_spectrum,lv_obj_t* p_box){
	Music_Spectrum_Data_Type* pdata=lv_obj_get_user_data(p_spectrum);
	
	if(pdata->p_box!=p_box){
		if(pdata->p_box!=NULL){
			lv_music_ctrl_box_ex_remove_delete_cb(pdata->p_box,music_spectrum_for_box_del,p_spectrum);
		}
		lv_music_ctrl_box_ex_add_delete_cb(p_box,music_spectrum_for_box_del,p_spectrum);
	}
	pdata->p_box=p_box;
	music_spectrum_fresh(pdata);
}



