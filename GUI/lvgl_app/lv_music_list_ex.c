#include "lv_music_list_ex.h"
#include "lv_music_ctrl_box_ex.h"

typedef struct {
	lv_obj_t* p_box;
	lv_obj_t** btn_add;
	uint32_t priv_index;
}Music_List_Data;

static void music_list_fresh_index_show(lv_obj_t* p_obj);

static void music_list_change_cb(lv_obj_t* p_box,void* data){
	lv_obj_t* p_obj=data;
	music_list_fresh_index_show(p_obj);
}

static void music_list_fresh_cb(lv_obj_t* p_box,void* data){
	lv_obj_t* p_obj=data;
	lv_music_list_ex_link_ctrl_box(p_obj,p_box);
}

static void music_list_delete_cb(lv_obj_t* p_box,void* data){
	lv_obj_t* p_obj=data;
	Music_List_Data* pdata=lv_obj_get_user_data(p_obj);
	lv_obj_clean(p_obj);
	free(pdata->btn_add);
	memset(pdata,0,sizeof(Music_List_Data));
}

static void music_list_delete(lv_event_t* event){
	lv_obj_t* p_obj=lv_event_get_target(event);
	Music_List_Data* pdata=lv_obj_get_user_data(p_obj);
	if(pdata->p_box!=NULL){
		lv_music_ctrl_box_ex_remove_change_cb(pdata->p_box,music_list_change_cb,p_obj);
		lv_music_ctrl_box_ex_remove_fresh_cb(pdata->p_box,music_list_fresh_cb,p_obj);
		lv_music_ctrl_box_ex_remove_delete_cb(pdata->p_box,music_list_delete_cb,p_obj);
	}
	free(pdata->btn_add);
	free(pdata);
}

static void music_list_fresh_index_show(lv_obj_t* p_obj){
	Music_List_Data* pdata=lv_obj_get_user_data(p_obj);
	if(pdata->btn_add==NULL||pdata->p_box==NULL)
		return;
	lv_obj_clear_state(pdata->btn_add[pdata->priv_index],LV_STATE_CHECKED);
	pdata->priv_index=lv_music_ctrl_box_ex_get_now_index(pdata->p_box);
	lv_obj_add_state(pdata->btn_add[pdata->priv_index],LV_STATE_CHECKED);
	lv_obj_scroll_to_view(pdata->btn_add[pdata->priv_index],LV_ANIM_ON);
}

static void music_list_btn_click(lv_event_t* event){
	lv_obj_t* p_btn=lv_event_get_target(event);
	lv_obj_t* p_obj=lv_obj_get_parent(p_btn);
	Music_List_Data* pdata=lv_obj_get_user_data(p_obj);
	uint32_t  index=(uint32_t)lv_obj_get_user_data(p_btn);
	lv_music_ctrl_box_ex_play_index(pdata->p_box,index);
	music_list_fresh_index_show(p_obj);
}


//创建一个音乐列表
//parent:父对象
//返回值：lv_obj_t* 创建的对象
lv_obj_t* lv_music_list_ex_create(lv_obj_t* parent){
	lv_obj_t* p_obj;
	Music_List_Data* pdata;
	
	pdata=malloc(sizeof(Music_List_Data));
	if(pdata==NULL){
		goto fail0;
	}
	memset(pdata,0,sizeof(Music_List_Data));
	
	p_obj=lv_list_create(parent);
	if(p_obj==NULL){
		goto fail1;
	}
	
	lv_obj_add_event_cb(p_obj,music_list_delete,LV_EVENT_DELETE,NULL);
	lv_obj_set_style_text_font(p_obj,&lv_font_hz16_aw,LV_STATE_DEFAULT);
	lv_obj_set_user_data(p_obj,pdata);
	return p_obj;
fail1:
	free(pdata);
fail0:
	return NULL;
}

//将音乐列表与音乐控制盒连接
//p_obj：音乐列表对象
//p_box：控制盒对象
void lv_music_list_ex_link_ctrl_box(lv_obj_t* p_obj,lv_obj_t* p_box){
	const File_Array* pdir;
	Music_List_Data* pdata=lv_obj_get_user_data(p_obj);
	
	if(pdata->p_box!=p_box&&pdata->p_box!=NULL){
		lv_music_ctrl_box_ex_remove_change_cb(pdata->p_box,music_list_change_cb,p_obj);
		lv_music_ctrl_box_ex_remove_fresh_cb(pdata->p_box,music_list_fresh_cb,p_obj);
		lv_music_ctrl_box_ex_remove_delete_cb(pdata->p_box,music_list_delete_cb,p_obj);
	}
	
	lv_obj_clean(p_obj);
	pdata->priv_index=0;
	free(pdata->btn_add);
	pdata->btn_add=NULL;
	if(p_box==NULL){
		pdata->p_box=p_box;
		return;
	}
	
	pdir=lv_music_ctrl_box_ex_get_dir(p_box);
	if(pdir->file_num!=0){
		pdata->btn_add=malloc(sizeof(lv_obj_t*)*pdir->file_num);
		if(pdata->btn_add==NULL){
			return;
		}
	}
	
	for(size_t i=0;i<pdir->file_num;i++){
		const char* music_name=pdir->name_buf[i];
		const char* sub_name;
		sub_name=strrchr(music_name,'/');
		if(sub_name==NULL){
			sub_name=music_name;
		}
		else{
			sub_name++;
		}
		lv_obj_t* pbtn=lv_list_add_btn(p_obj,LV_SYMBOL_AUDIO,sub_name);
		if(pbtn==NULL){
			free(pdata->btn_add);
			pdata->btn_add=NULL;
			break;
		}
		lv_obj_add_event_cb(pbtn,music_list_btn_click,LV_EVENT_CLICKED,NULL);
		lv_obj_set_style_bg_color(pbtn,lv_palette_main(LV_PALETTE_CYAN),LV_STATE_CHECKED);
		lv_obj_set_user_data(pbtn,(void*)i);
		pdata->btn_add[i]=pbtn;
	}
	
	if(pdata->p_box!=p_box){
		lv_music_ctrl_box_ex_add_change_cb(p_box,music_list_change_cb,p_obj);
		lv_music_ctrl_box_ex_add_fresh_cb(p_box,music_list_fresh_cb,p_obj);
		lv_music_ctrl_box_ex_add_delete_cb(p_box,music_list_delete_cb,p_obj);
	}
	pdata->p_box=p_box;
	music_list_fresh_index_show(p_obj);
}



