#ifndef LV_MUSIC_CTRL_BOX_EX_H__
#define LV_MUSIC_CTRL_BOX_EX_H__

#include "lvgl/lvgl.h"
#include "fatfs.h"
#include "music_device.h"

//音乐控制盒
#ifdef __cplusplus
extern "C" {
#endif

lv_obj_t* lv_music_ctrl_box_ex_create(lv_obj_t* parent);
bool lv_music_ctrl_box_ex_play_index(lv_obj_t* pobj,uint32_t index);
void lv_music_ctrl_box_ex_set_path(lv_obj_t* pobj,const char* path,const File_Filter* filter,uint8_t mode);
void lv_music_ctrl_box_ex_set_sort_method(lv_obj_t* pobj,int (*sort_method)(const char*,const char*));
void lv_music_ctrl_box_ex_fresh(lv_obj_t* pobj);
const File_Array* lv_music_ctrl_box_ex_get_dir(lv_obj_t* pobj);
void lv_music_ctrl_box_ex_add_fresh_cb(lv_obj_t* pobj,void (*fresh_cb)(lv_obj_t*,void*),void* use_data);
void lv_music_ctrl_box_ex_add_change_cb(lv_obj_t* pobj,void (*change_cb)(lv_obj_t*,void*),void* use_data);
void lv_music_ctrl_box_ex_add_delete_cb(lv_obj_t* pobj,void (*del_cb)(lv_obj_t*,void*),void* use_data);
void lv_music_ctrl_box_ex_remove_fresh_cb(lv_obj_t* pobj,void (*fresh_cb)(lv_obj_t*,void*),void* data);
void lv_music_ctrl_box_ex_remove_change_cb(lv_obj_t* pobj,void (*change_hook)(lv_obj_t*,void*),void* data);
void lv_music_ctrl_box_ex_remove_delete_cb(lv_obj_t* pobj,void (*del_cb)(lv_obj_t*,void*),void* data);
void lv_music_ctrl_box_ex_dragabble(lv_obj_t* pobj,bool onoff);
uint32_t lv_music_ctrl_box_ex_get_now_index(lv_obj_t* pobj);
bool lv_music_ctrl_box_ex_get_now_buf(lv_obj_t* pobj,Music_BufType* pbuf);
uint32_t lv_music_ctrl_box_ex_get_now_samplate(lv_obj_t* pobj);
void lv_music_ctrl_box_ex_set_float(lv_obj_t* pobj);
void lv_music_ctrl_box_ex_set_station(lv_obj_t* pobj,lv_obj_t* parent);
void lv_music_ctrl_box_ex_set_order(lv_obj_t* pobj,uint32_t (*play_order)(uint32_t priv,uint32_t max_index));
#ifdef __cplusplus
}
#endif

#endif
