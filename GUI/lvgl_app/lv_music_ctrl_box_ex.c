#include "lv_music_ctrl_box_ex.h"
#include "music_device.h"
#include "fatfs.h"
#include "lvgl_task.h"
#include "my_func_hook.h"
typedef struct {
    lv_obj_t* priv_btn;
    lv_obj_t* next_btn;
    lv_obj_t* ctrl_btn;
    lv_obj_t* play_slider;
    lv_obj_t* name_label;
    lv_obj_t* time_label;
	lv_obj_t* ctrl_label;
	lv_timer_t* fresh_timer;
    char* music_path;
	File_Filter  music_filter;
	Music_Handle music_handle;
	File_Array music_name_array;
	uint32_t now_index;
	int (*sort_method)(const char*,const char*);
	uint32_t (*play_order)(uint32_t priv,uint32_t max_index);
	uint8_t  scan_mode;
	My_Func_Root fresh_hook;
	My_Func_Root music_change_hook;
	My_Func_Root delete_hook;
	bool is_drag;
	bool disable_drag;
	void* user_data;
}Music_Ctrl_Box_Data_Type;

#define NAME_TEXT_COLOR				 lv_color_black()
#define MUSIC_CTRL_BOX_DEFAULT_WIDTH 460
#define MUSIC_CTRL_BOX_DEFAULT_HEIGHT 80
#define MUSIC_BTN_SIZE				 48
#define MUSIC_NAME_HEIGHT			 36
#define MUSIC_BTN_SPACE				 18
#define MUSIC_PRIV_BTN_X			 0
#define MUSIC_PRIV_BTN_Y			 0
#define MUSIC_CTRL_BTN_X			 (MUSIC_PRIV_BTN_X+MUSIC_BTN_SIZE+MUSIC_BTN_SPACE)
#define MUSIC_CTRL_BTN_Y			 MUSIC_PRIV_BTN_Y
#define MUSIC_NEXT_BTN_X			 (MUSIC_CTRL_BTN_X+MUSIC_BTN_SIZE+MUSIC_BTN_SPACE)
#define MUSIC_NEXT_BTN_Y			 MUSIC_CTRL_BTN_Y
#define MUSIC_NAME_TEXT_X			 (MUSIC_NEXT_BTN_X+MUSIC_BTN_SIZE+MUSIC_BTN_SPACE)
#define MUSIC_NAME_TEXT_Y			 (MUSIC_NEXT_BTN_Y+(MUSIC_BTN_SIZE-MUSIC_NAME_HEIGHT)/2)
#define MUSIC_FRESH_PERIOD			 500
#define MUSIC_CTRL_PRIORITY			 0
#define MUSIC_SLIDER_HEIGHT			 3
#define MUSIC_SLIDER_LABEL_SPACE	 3

static lv_style_t btn_press_style;
static lv_style_t btn_unpress_style;
static uint32_t music_ctrl_box_cnt;

static bool music_ctrl_play_index_ready(Music_Ctrl_Box_Data_Type* pdata,uint32_t index,bool start);

							
static void music_ctrl_data_init(Music_Ctrl_Box_Data_Type* pdata){
	memset(pdata,0,sizeof(Music_Ctrl_Box_Data_Type));
}

static void music_ctrl_style_init(lv_obj_t* parent){
	if(music_ctrl_box_cnt==0){
		lv_style_init(&btn_press_style);
		lv_style_init(&btn_unpress_style);
		
		lv_style_set_text_font(&btn_unpress_style, &lv_font_hz16_aw);
		lv_style_set_text_align(&btn_unpress_style, LV_TEXT_ALIGN_CENTER);
		lv_style_set_text_color(&btn_unpress_style, lv_theme_get_color_primary(parent));
		lv_style_set_bg_color(&btn_unpress_style, lv_color_white());
		lv_style_set_radius(&btn_unpress_style, LV_RADIUS_CIRCLE);
		lv_style_set_clip_corner(&btn_unpress_style, true);
		lv_style_set_shadow_color(&btn_unpress_style, lv_theme_get_color_primary(parent));
		lv_style_set_shadow_spread(&btn_unpress_style, 5);
		lv_style_set_shadow_width(&btn_unpress_style, 10);
		lv_style_set_shadow_ofs_y(&btn_unpress_style, 0);
		lv_style_set_opa(&btn_unpress_style,LV_OPA_COVER);
		
		lv_style_set_text_font(&btn_press_style, &lv_font_hz16_aw);
		lv_style_set_text_align(&btn_press_style, LV_TEXT_ALIGN_CENTER);
		lv_style_set_text_color(&btn_press_style, lv_color_white());
		lv_style_set_bg_color(&btn_press_style, lv_theme_get_color_primary(parent));
		lv_style_set_radius(&btn_press_style, LV_RADIUS_CIRCLE);
		lv_style_set_clip_corner(&btn_press_style, true);
		lv_style_set_shadow_color(&btn_press_style, lv_theme_get_color_primary(parent));
		lv_style_set_shadow_spread(&btn_press_style, 5);
		lv_style_set_shadow_width(&btn_press_style, 10);
		lv_style_set_shadow_ofs_y(&btn_press_style, 0);
		lv_style_set_opa(&btn_press_style,LV_OPA_COVER);
	}
	music_ctrl_box_cnt++;
}

static void music_ctrl_style_deinit(void){
	music_ctrl_box_cnt--;
	if(music_ctrl_box_cnt==0){
		lv_style_reset(&btn_press_style);
		lv_style_reset(&btn_unpress_style);
	}
}

static uint32_t music_play_order_default(uint32_t priv,uint32_t max_index){
	return (priv+1)%max_index;
}

static void music_lvgl_process(void* p){
	Music_Ctrl_Box_Data_Type* pdata=p;
	uint32_t (*play_order)(uint32_t priv,uint32_t max_index);
	if(pdata->play_order==NULL){
		play_order=music_play_order_default;
	}
	else{
		play_order=pdata->play_order;
	}
	pdata->now_index=play_order(pdata->now_index,pdata->music_name_array.file_num);
	pdata->now_index=LV_MIN(pdata->now_index,pdata->music_name_array.file_num-1);
	music_ctrl_play_index_ready(pdata,pdata->now_index,true);
}

static void music_ctrl_box_end_hook(void* p){
	LVGL_Protect_Run(music_lvgl_process,p,true);
}

static void music_button_ctrl_fresh(Music_Ctrl_Box_Data_Type* pdata){
	lv_label_set_text(pdata->ctrl_label,Music_Is_Suspend(pdata->music_handle)?LV_SYMBOL_PLAY:LV_SYMBOL_PAUSE);
}


static void music_ctrl_fresh(Music_Ctrl_Box_Data_Type* pdata){
	if(!lv_slider_is_dragged(pdata->play_slider)){
		uint32_t total_sec=Music_Get_TotalSec(pdata->music_handle);
		uint32_t now_sec=(float)Music_Get_NowPos(pdata->music_handle)/(float)Music_Get_TotalPos(pdata->music_handle)*total_sec;
		lv_slider_set_value(pdata->play_slider,now_sec,LV_ANIM_OFF);
		uint32_t total_hour=total_sec/3600;
		if(total_hour==0)
			lv_label_set_text_fmt(pdata->time_label,"%02d:%02d/%02d:%02d",now_sec/60,now_sec%60,total_sec/60,total_sec%60);
		else
			lv_label_set_text_fmt(pdata->time_label,"%02d:%02d:%02d/%02d:%02d:%02d"\
			,now_sec/3600,(now_sec%3600)/60,(now_sec%3600)%60,total_sec/3600,(total_sec%3600)/60,(total_sec%3600)%60);
		} 
}

static bool music_ctrl_play_index_ready(Music_Ctrl_Box_Data_Type* pdata,uint32_t index,bool start){
	char* temp_buf;
	const char* name_start;
	uint32_t node_len,path_len;
	uint32_t temp_len;
	Music_InitType music_info;
	if(index>=pdata->music_name_array.file_num){
		lv_label_set_text(pdata->name_label,"No Music!");
		return false;
	}
	node_len=strlen(pdata->music_name_array.name_buf[index]);
	path_len=strlen(pdata->music_path);
	temp_len=node_len+path_len+2;
	temp_buf=malloc(temp_len);
	if(temp_buf==NULL){
		return false;
	}
	Music_Delete(pdata->music_handle);
	strcpy(temp_buf,pdata->music_path);
	temp_buf[path_len]='\0';
	strcat(temp_buf,"/");
	strcat(temp_buf,pdata->music_name_array.name_buf[index]);
	temp_buf[temp_len-1]='\0';
	music_info.end_hook=music_ctrl_box_end_hook;
	music_info.pDecoder=NULL;
	music_info.priority=MUSIC_CTRL_PRIORITY;
	music_info.token=NULL;
	music_info.use_ptr=pdata;
	pdata->music_handle=Music_Create(temp_buf,&music_info);
	if(start){
		Music_Start(pdata->music_handle);
	}
	name_start=strrchr(temp_buf,'/');
	if(name_start==NULL){
		name_start=temp_buf;
	}
	else{
		name_start++;
	}
	lv_label_set_text(pdata->name_label,name_start);
	free(temp_buf);
	lv_slider_set_range(pdata->play_slider,0,Music_Get_TotalSec(pdata->music_handle));
	music_button_ctrl_fresh(pdata);
	music_ctrl_fresh(pdata);
	pdata->now_index=index;
	
	my_func_hook_run_all(pdata->music_change_hook,lv_obj_get_parent(pdata->ctrl_btn));
	
	return pdata->music_handle!=NULL;
}

static void music_ctrl_timer_cb(lv_timer_t* ptimer){
	music_ctrl_fresh(ptimer->user_data);
}

static void music_ctrl_del_cb(lv_event_t* event){
	Music_Ctrl_Box_Data_Type* pdata=lv_event_get_user_data(event);
	my_func_hook_run_all(pdata->delete_hook,lv_event_get_target(event));
	Music_Delete(pdata->music_handle);
	Dir_Array_Free(&pdata->music_name_array);
	Dir_File_Filter_Destroy(&pdata->music_filter);
	free(pdata->music_path);
	lv_timer_del(pdata->fresh_timer);
	my_func_hook_clear_all(&pdata->fresh_hook);
	my_func_hook_clear_all(&pdata->music_change_hook);
	my_func_hook_clear_all(&pdata->delete_hook);
	free(pdata);
	music_ctrl_style_deinit();
}

static void music_ctrl_btn_call(lv_event_t* event){
	Music_Ctrl_Box_Data_Type* pdata=lv_event_get_user_data(event);
	if(Music_Is_Suspend(pdata->music_handle)){
		Music_Start(pdata->music_handle);
	}
	else{
		Music_Stop(pdata->music_handle);
	}
	music_button_ctrl_fresh(pdata);
}

static void music_priv_btn_call(lv_event_t* event){
	Music_Ctrl_Box_Data_Type* pdata=lv_event_get_user_data(event);
	
	if(pdata->music_name_array.file_num==0){
		return;
	}
	
	if(pdata->now_index==0){
		pdata->now_index=pdata->music_name_array.file_num-1;
	}
	else{
		pdata->now_index--;
	}
	music_ctrl_play_index_ready(pdata,pdata->now_index,true);
}

static void music_next_btn_call(lv_event_t* event){
	Music_Ctrl_Box_Data_Type* pdata=lv_event_get_user_data(event);
	if(pdata->music_name_array.file_num==0){
		return;
	}
	pdata->now_index++;
	if(pdata->now_index>=pdata->music_name_array.file_num){
		pdata->now_index=0;
	}
	music_ctrl_play_index_ready(pdata,pdata->now_index,true);
}

static void music_slider_release_call(lv_event_t* event){
	Music_Ctrl_Box_Data_Type* pdata=lv_event_get_user_data(event);
	uint32_t slider_value=lv_slider_get_value(pdata->play_slider);
	uint32_t total_sec=Music_Get_TotalSec(pdata->music_handle);
	uint32_t total_pos=Music_Get_TotalPos(pdata->music_handle);
	Music_Set_Pos(pdata->music_handle,(float)slider_value/(float)total_sec*total_pos);
	music_ctrl_fresh(pdata);
}

static void music_slider_change_call(lv_event_t* event){
	Music_Ctrl_Box_Data_Type* pdata=lv_event_get_user_data(event);
	uint32_t total_sec=Music_Get_TotalSec(pdata->music_handle);
	uint32_t now_sec=lv_slider_get_value(pdata->play_slider);
	uint32_t total_hour=total_sec/3600;
	if(total_hour==0)
		lv_label_set_text_fmt(pdata->time_label,"%02d:%02d/%02d:%02d",now_sec/60,now_sec%60,total_sec/60,total_sec%60);
	else
		lv_label_set_text_fmt(pdata->time_label,"%02d:%02d:%02d/%02d:%02d:%02d"\
		,now_sec/3600,(now_sec%3600)/60,(now_sec%3600)%60,total_sec/3600,(total_sec%3600)/60,(total_sec%3600)%60);
}

static void music_box_dragable_pressing_call(lv_event_t* event){
	Music_Ctrl_Box_Data_Type* pdata=lv_event_get_user_data(event);
	
	if(pdata->is_drag==false||pdata->disable_drag)
		return;
	
	lv_obj_t * obj = lv_event_get_target(event);
	lv_obj_t * parent = lv_obj_get_parent(obj);
	lv_indev_t * indev = lv_indev_get_act();
	lv_coord_t max_x=lv_obj_get_content_width(parent)-lv_obj_get_width(obj);
	lv_coord_t min_x=0;
	lv_coord_t max_y=lv_obj_get_content_height(parent)-lv_obj_get_height(obj);
	lv_coord_t min_y=0;
	
	if( indev == NULL)  
		return;
	
	lv_point_t vect;
	lv_indev_get_vect(indev, &vect);

	lv_coord_t x = lv_obj_get_x(obj) + vect.x;
	lv_coord_t y = lv_obj_get_y(obj) + vect.y;
	
	lv_obj_set_pos(obj, LV_CLAMP(min_x,x,max_x), LV_CLAMP(min_y,y,max_y));
}

static void music_box_dragable_long_press_call(lv_event_t* event){
	Music_Ctrl_Box_Data_Type* pdata=lv_event_get_user_data(event);
	pdata->is_drag=true;
}

static void music_box_dragable_release_call(lv_event_t* event){
	Music_Ctrl_Box_Data_Type* pdata=lv_event_get_user_data(event);
	pdata->is_drag=false;
}

//创建一个音乐控制盒，注意lv_obj_t的use_data字段被使用，不可以对其调用lv_obj_set_user_data
//parent:父对象
//返回值：lv_obj_t* 创建的对象
lv_obj_t* lv_music_ctrl_box_ex_create(lv_obj_t* parent) {

    lv_obj_t* p_obj;
	lv_obj_t* priv_label,*next_label;
    Music_Ctrl_Box_Data_Type* pdata;

	music_ctrl_style_init(parent);
    pdata = malloc(sizeof(Music_Ctrl_Box_Data_Type));
    if (pdata == NULL) {
        goto fail0;
    }
	music_ctrl_data_init(pdata);
	
	p_obj=lv_obj_create(parent);
	if(p_obj==NULL){
		goto fail1;
	}
	
	lv_obj_set_content_width(p_obj,MUSIC_CTRL_BOX_DEFAULT_WIDTH);
	lv_obj_set_content_height(p_obj,MUSIC_CTRL_BOX_DEFAULT_HEIGHT);
	
	pdata->ctrl_btn		= lv_btn_create(p_obj);
	pdata->name_label	= lv_label_create(p_obj);
	pdata->next_btn		= lv_btn_create(p_obj);
	pdata->play_slider	= lv_slider_create(p_obj);
	pdata->priv_btn		= lv_btn_create(p_obj);
	pdata->time_label	= lv_label_create(p_obj);
	pdata->ctrl_label	= lv_label_create(pdata->ctrl_btn);
	priv_label			= lv_label_create(pdata->priv_btn);
	next_label			= lv_label_create(pdata->next_btn);
	pdata->fresh_timer  = lv_timer_create(music_ctrl_timer_cb,MUSIC_FRESH_PERIOD,pdata);
	
	if(pdata->ctrl_btn		== NULL||\
	   pdata->name_label	== NULL||\
	   pdata->next_btn		== NULL||\
	   pdata->play_slider	== NULL||\
	   pdata->priv_btn		== NULL||\
	   pdata->time_label	== NULL||\
	   pdata->ctrl_label	== NULL||\
	   priv_label			== NULL||\
	   next_label			== NULL||\
	   pdata->fresh_timer   == NULL)
	{
		goto fail2;
	}
	
	lv_obj_set_size(pdata->ctrl_btn,MUSIC_BTN_SIZE,MUSIC_BTN_SIZE);
	lv_obj_set_size(pdata->next_btn,MUSIC_BTN_SIZE,MUSIC_BTN_SIZE);
	lv_obj_set_size(pdata->priv_btn,MUSIC_BTN_SIZE,MUSIC_BTN_SIZE);
	
	lv_obj_add_style(pdata->ctrl_btn,&btn_press_style,LV_STATE_PRESSED | LV_PART_MAIN);
	lv_obj_add_style(pdata->ctrl_btn,&btn_unpress_style,LV_STATE_DEFAULT | LV_PART_MAIN);
	
	lv_obj_add_style(pdata->next_btn,&btn_press_style,LV_STATE_PRESSED | LV_PART_MAIN);
	lv_obj_add_style(pdata->next_btn,&btn_unpress_style,LV_STATE_DEFAULT | LV_PART_MAIN);
	
	lv_obj_add_style(pdata->priv_btn,&btn_press_style,LV_STATE_PRESSED | LV_PART_MAIN);
	lv_obj_add_style(pdata->priv_btn,&btn_unpress_style,LV_STATE_DEFAULT | LV_PART_MAIN);
	
	lv_obj_set_pos(pdata->priv_btn,MUSIC_PRIV_BTN_X,MUSIC_PRIV_BTN_Y);
	lv_obj_set_pos(pdata->next_btn,MUSIC_NEXT_BTN_X,MUSIC_NEXT_BTN_Y);
	lv_obj_set_pos(pdata->ctrl_btn,MUSIC_CTRL_BTN_X,MUSIC_CTRL_BTN_Y);
	
	lv_label_set_text(priv_label,LV_SYMBOL_PREV);
	lv_label_set_text(pdata->ctrl_label,LV_SYMBOL_PLAY);
	lv_label_set_text(next_label,LV_SYMBOL_NEXT);
	
	lv_obj_set_style_align(priv_label,LV_ALIGN_CENTER,LV_STATE_DEFAULT);
	lv_obj_set_style_align(pdata->ctrl_label,LV_ALIGN_CENTER,LV_STATE_DEFAULT);
	lv_obj_set_style_align(next_label,LV_ALIGN_CENTER,LV_STATE_DEFAULT);
	
	lv_obj_set_width(pdata->name_label,MUSIC_CTRL_BOX_DEFAULT_WIDTH-MUSIC_NAME_TEXT_X);
	lv_obj_set_height(pdata->name_label,MUSIC_NAME_HEIGHT);
	lv_label_set_long_mode(pdata->name_label,LV_LABEL_LONG_SCROLL_CIRCULAR);
	lv_obj_set_style_text_color(pdata->name_label,NAME_TEXT_COLOR,LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(pdata->name_label,&lv_font_hz24_aw,LV_STATE_DEFAULT);
	lv_obj_set_pos(pdata->name_label,MUSIC_NAME_TEXT_X,MUSIC_NAME_TEXT_Y);
	lv_obj_set_style_opa(pdata->name_label,LV_OPA_COVER,LV_STATE_DEFAULT);

	lv_obj_set_style_text_color(pdata->time_label,NAME_TEXT_COLOR,LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(pdata->time_label,&lv_font_montserrat_14,LV_STATE_DEFAULT);
	lv_obj_set_align(pdata->time_label,LV_ALIGN_BOTTOM_RIGHT);
	lv_obj_set_x(pdata->time_label,0);
	lv_obj_set_y(pdata->time_label,-(MUSIC_SLIDER_HEIGHT+MUSIC_SLIDER_LABEL_SPACE));
	lv_obj_set_style_opa(pdata->time_label,LV_OPA_COVER,LV_STATE_DEFAULT);
	
	lv_obj_set_align(pdata->play_slider,LV_ALIGN_BOTTOM_LEFT);
	lv_obj_set_pos(pdata->play_slider,0,0);
	lv_obj_set_size(pdata->play_slider,MUSIC_CTRL_BOX_DEFAULT_WIDTH,MUSIC_SLIDER_HEIGHT);
	lv_obj_set_style_pad_all(pdata->play_slider,2,LV_PART_KNOB);
	lv_obj_set_style_opa(pdata->play_slider,LV_OPA_COVER,LV_STATE_DEFAULT);
	
	lv_obj_add_event_cb(p_obj,music_ctrl_del_cb,LV_EVENT_DELETE,pdata);
	lv_obj_add_event_cb(p_obj,music_box_dragable_pressing_call,LV_EVENT_PRESSING,pdata);
	lv_obj_add_event_cb(p_obj,music_box_dragable_long_press_call,LV_EVENT_LONG_PRESSED,pdata);
	lv_obj_add_event_cb(p_obj,music_box_dragable_release_call,LV_EVENT_RELEASED,pdata);
	lv_obj_add_event_cb(pdata->ctrl_btn,music_ctrl_btn_call,LV_EVENT_CLICKED,pdata);
	lv_obj_add_event_cb(pdata->priv_btn,music_priv_btn_call,LV_EVENT_CLICKED,pdata);
	lv_obj_add_event_cb(pdata->next_btn,music_next_btn_call,LV_EVENT_CLICKED,pdata);
	lv_obj_add_event_cb(pdata->play_slider,music_slider_release_call,LV_EVENT_RELEASED,pdata);
	lv_obj_add_event_cb(pdata->play_slider,music_slider_change_call,LV_EVENT_VALUE_CHANGED,pdata);
	
	lv_obj_set_user_data(p_obj,pdata);
	lv_obj_add_flag(p_obj,LV_OBJ_FLAG_FLOATING);
	
	return p_obj;
fail2:	
	lv_obj_del(p_obj);
	lv_timer_del(pdata->fresh_timer);
fail1:
    free(pdata);
fail0:
	music_ctrl_style_deinit();
    return NULL;
}

//播放指定索引的音频
//pobj：对象句柄
//index：索引
//返回值：true播放成功
//        false播放失败
bool lv_music_ctrl_box_ex_play_index(lv_obj_t* pobj,uint32_t index){
	Music_Ctrl_Box_Data_Type* pdata=lv_obj_get_user_data(pobj);
	return music_ctrl_play_index_ready(pdata,index,true);
}

static void music_ctrl_box_fresh_file(Music_Ctrl_Box_Data_Type* pdata){
	File_Dir file_dir;
	Music_Delete(pdata->music_handle);
	pdata->music_handle=NULL;
	Dir_Array_Free(&pdata->music_name_array);
	
	My_List_Init(&file_dir);
	
	if(Dir_Scan(pdata->music_path,&pdata->music_filter,&file_dir,pdata->scan_mode)==false){
		printf("%s:扫描失败\r\n",pdata->music_path);
	}
	
	if(pdata->sort_method!=NULL){
		My_List_Sort(&file_dir,(int(*)(const void*,const void*))pdata->sort_method);
	}
	
	if(Dir_To_Array(&file_dir,&pdata->music_name_array)==false){
		Dir_Free(&file_dir);
		return;
	}
	my_func_hook_run_all(pdata->fresh_hook,lv_obj_get_parent(pdata->ctrl_btn));
	music_ctrl_play_index_ready(pdata,0,false);
}

//设置播放音乐的路径
//pobj：句柄
//path：播放路径
//filter：文件过滤（filter对应的数组不可以是局部变量）
//mode：扫描模式
void lv_music_ctrl_box_ex_set_path(lv_obj_t* pobj,const char* path,const File_Filter* filter,uint8_t mode){
	Music_Ctrl_Box_Data_Type* pdata=lv_obj_get_user_data(pobj);
	char* buf_path=NULL;
	uint32_t path_len=strlen(path)+1;
	
	buf_path=realloc(pdata->music_path,path_len);
	if(buf_path==NULL){
		return;
	}
	memcpy(buf_path,path,path_len);
	pdata->music_path=buf_path;
	Dir_File_Filter_Copy(&pdata->music_filter,filter);
	pdata->scan_mode = mode;
	music_ctrl_box_fresh_file(pdata);
}

//设置音乐排序方法
//pobj：句柄
//sort_method：排序方法
void lv_music_ctrl_box_ex_set_sort_method(lv_obj_t* pobj,int (*sort_method)(const char*,const char*)){
	Music_Ctrl_Box_Data_Type* pdata=lv_obj_get_user_data(pobj);
	pdata->sort_method=sort_method;
	music_ctrl_box_fresh_file(pdata);
}

//刷新对象文件目录
//pobj：句柄
void lv_music_ctrl_box_ex_fresh(lv_obj_t* pobj){
	Music_Ctrl_Box_Data_Type* pdata=lv_obj_get_user_data(pobj);
	music_ctrl_box_fresh_file(pdata);
}

//向控制盒添加刷新回调函数,此函数会在控制盒文件目录更改时调用
//pobj：句柄
//fresh_cb：刷新回调函数
//use_data：回调函数参数
void lv_music_ctrl_box_ex_add_fresh_cb(lv_obj_t* pobj,void (*fresh_cb)(lv_obj_t*,void*),void* use_data){
	Music_Ctrl_Box_Data_Type* pdata=lv_obj_get_user_data(pobj);
	my_func_hook_add(&pdata->fresh_hook,(My_Func)fresh_cb,use_data);
}


//获取当前控制盒的文件目录
//pobj：句柄
const File_Array* lv_music_ctrl_box_ex_get_dir(lv_obj_t* pobj){
	Music_Ctrl_Box_Data_Type* pdata=lv_obj_get_user_data(pobj);
	return &pdata->music_name_array;
}

//向控制盒添加更改回调函数,此函数会在控制盒切换音乐时调用
//pobj：句柄
//change_cb：切换回调函数
//use_data：回调函数参数
void lv_music_ctrl_box_ex_add_change_cb(lv_obj_t* pobj,void (*change_cb)(lv_obj_t*,void*),void* use_data){
	Music_Ctrl_Box_Data_Type* pdata=lv_obj_get_user_data(pobj);
	my_func_hook_add(&pdata->music_change_hook,(My_Func)change_cb,use_data);
}

//向控制盒添加更改删除函数,此函数会在控制盒被删除时调用
//pobj：句柄
//del_cb：删除回调函数
//use_data：回调函数参数
void lv_music_ctrl_box_ex_add_delete_cb(lv_obj_t* pobj,void (*del_cb)(lv_obj_t*,void*),void* use_data){
	Music_Ctrl_Box_Data_Type* pdata=lv_obj_get_user_data(pobj);
	my_func_hook_add(&pdata->delete_hook,(My_Func)del_cb,use_data);
}

//移除指定的刷新回调函数
//pobj：句柄
//fresh_cb：被移除的刷新回调函数
//use_data：被移除的回调函数参数
void lv_music_ctrl_box_ex_remove_fresh_cb(lv_obj_t* pobj,void (*fresh_cb)(lv_obj_t*,void*),void* data){
	Music_Ctrl_Box_Data_Type* pdata=lv_obj_get_user_data(pobj);
	my_func_hook_remove(&pdata->fresh_hook,(My_Func)fresh_cb,data);
}

//移除指定的切换回调函数
//pobj：句柄
//change_cb：被移除的切换回调函数
//use_data：被移除的回调函数参数
void lv_music_ctrl_box_ex_remove_change_cb(lv_obj_t* pobj,void (*change_cb)(lv_obj_t*,void*),void* data){
	Music_Ctrl_Box_Data_Type* pdata=lv_obj_get_user_data(pobj);
	my_func_hook_remove(&pdata->music_change_hook,(My_Func)change_cb,data);
}

//移除指定的删除回调函数
//pobj：句柄
//del_cb：被移除的删除回调函数
//use_data：被移除的回调函数参数
void lv_music_ctrl_box_ex_remove_delete_cb(lv_obj_t* pobj,void (*del_cb)(lv_obj_t*,void*),void* data){
	Music_Ctrl_Box_Data_Type* pdata=lv_obj_get_user_data(pobj);
	my_func_hook_remove(&pdata->delete_hook,(My_Func)del_cb,data);
}

//设置控制盒是否可以被拖拽，默认开启可拖拽，长按触发拖拽
//pobj：句柄
//onoff：true开启，false关闭
void lv_music_ctrl_box_ex_dragabble(lv_obj_t* pobj,bool onoff){
	Music_Ctrl_Box_Data_Type* pdata=lv_obj_get_user_data(pobj);
	pdata->disable_drag=!onoff;
}

//获取控制盒当前播放的音乐索引
//pobj：句柄
//返回值：当前索引
uint32_t lv_music_ctrl_box_ex_get_now_index(lv_obj_t* pobj){
	Music_Ctrl_Box_Data_Type* pdata=lv_obj_get_user_data(pobj);
	return pdata->now_index;
}

bool lv_music_ctrl_box_ex_get_now_buf(lv_obj_t* pobj,Music_BufType* pbuf){
	Music_Ctrl_Box_Data_Type* pdata=lv_obj_get_user_data(pobj);
	return Music_Get_Buf_Data(pdata->music_handle,pbuf);
}

uint32_t lv_music_ctrl_box_ex_get_now_samplate(lv_obj_t* pobj){
	Music_Ctrl_Box_Data_Type* pdata=lv_obj_get_user_data(pobj);
	return Music_Get_Samplate(pdata->music_handle);
}


void lv_music_ctrl_box_ex_set_float(lv_obj_t* pobj){
	lv_obj_add_flag(pobj,LV_OBJ_FLAG_FLOATING);
	lv_music_ctrl_box_ex_dragabble(pobj,true);
	lv_obj_set_style_opa(pobj,LV_OPA_30,LV_STATE_DEFAULT);
	lv_obj_set_parent(pobj,lv_layer_top());
}

void lv_music_ctrl_box_ex_set_station(lv_obj_t* pobj,lv_obj_t* parent){
	lv_obj_set_parent(pobj,parent);
	lv_music_ctrl_box_ex_dragabble(pobj,false);
	lv_obj_set_style_opa(pobj,LV_OPA_0,LV_STATE_DEFAULT);
	lv_obj_clear_flag(pobj,LV_OBJ_FLAG_FLOATING);
}

void lv_music_ctrl_box_ex_set_order(lv_obj_t* pobj,uint32_t (*play_order)(uint32_t priv,uint32_t max_index)){
	Music_Ctrl_Box_Data_Type* pdata=lv_obj_get_user_data(pobj);
	pdata->play_order=play_order;
}

