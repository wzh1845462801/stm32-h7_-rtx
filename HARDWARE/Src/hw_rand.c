#include "hw_rand.h"

static RNG_HandleTypeDef RNG_Handle;

static void Rand_Base_Init(void){	
	__HAL_RCC_RNG_CLK_ENABLE();
}


bool HW_Rand_Init(void){
	uint16_t retry=0;
	
	Rand_Base_Init();
	
	RNG_Handle.Instance=RNG;
	HAL_RNG_DeInit(&RNG_Handle);
	HAL_RNG_Init(&RNG_Handle);
	
	while(__HAL_RNG_GET_FLAG(&RNG_Handle,RNG_FLAG_DRDY)==RESET){
		retry++;
		delay_ms(10);
		if(retry>100)
			return false;
	}
	return true;
}

uint32_t RNG_Get_RandomNum(void){
	uint32_t data;
	HAL_RNG_GenerateRandomNumber(&RNG_Handle,&data);
	return data;
}


