#include "hw_mdma.h"

static MDMA_HandleTypeDef MDMA_Handle[16]={
	{.Instance=MDMA_Channel0},
	{.Instance=MDMA_Channel1},
	{.Instance=MDMA_Channel2},
	{.Instance=MDMA_Channel3},
	{.Instance=MDMA_Channel4},
	{.Instance=MDMA_Channel5},
	{.Instance=MDMA_Channel6},
	{.Instance=MDMA_Channel7},
	{.Instance=MDMA_Channel8},
	{.Instance=MDMA_Channel9},
	{.Instance=MDMA_Channel10},
	{.Instance=MDMA_Channel11},
	{.Instance=MDMA_Channel12},
	{.Instance=MDMA_Channel13},
	{.Instance=MDMA_Channel14},
	{.Instance=MDMA_Channel15},
};

bool HW_MDMA_Init(uint8_t channel,MDMA_InitTypeDef* init){
	static bool isInit=false;
	if(isInit==false){
		__HAL_RCC_MDMA_CLK_ENABLE();
		HAL_NVIC_EnableIRQ(MDMA_IRQn);
		HAL_NVIC_SetPriority(MDMA_IRQn,MDMA_ISR_PRIO,0);
	}
	MDMA_Handle[channel].Init=*init;
	isInit=true;
	return HAL_MDMA_Init(&MDMA_Handle[channel])==HAL_OK;
}	

bool HW_MDMA_DeInit(uint8_t channel){
	return HAL_MDMA_DeInit(&MDMA_Handle[channel])==HAL_OK;
}


bool HW_MDMA_Set_CallBack(uint8_t channel,void(*callback)(MDMA_HandleTypeDef*)){
	return HAL_MDMA_RegisterCallback(&MDMA_Handle[channel],HAL_MDMA_XFER_CPLT_CB_ID,callback)==HAL_OK;
}

bool HW_MDMA_Start_Transe(uint8_t channel,const void* psrc,void* pdest,uint32_t size){
	return HAL_MDMA_Start_IT(&MDMA_Handle[channel],(uint32_t)psrc,(uint32_t)pdest,size,1)==HAL_OK;
}

MDMA_HandleTypeDef* HW_MDMA_Get_Handle(uint8_t channel){
	return &MDMA_Handle[channel];
}

void MDMA_IRQHandler(void){
	for(uint8_t i=0;i<sizeof(MDMA_Handle)/sizeof(MDMA_Handle[0]);i++){
		HAL_MDMA_IRQHandler(&MDMA_Handle[i]);
	}
}


