#include "hw_lcd.h"

#define HBP  80	// 根据屏幕的手册进行设置
#define VBP  40
#define HSW  1
#define VSW  1
#define HFP  200
#define VFP  22

void LCD_BK_Ctrl(bool on){
	HAL_GPIO_WritePin(GPIOH,GPIO_PIN_6,on?GPIO_PIN_SET:GPIO_PIN_RESET);
}

// 函数：IO口配置初始化
//	
static void LTDC_GPIO_Init(void){
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	
	__HAL_RCC_LTDC_CLK_ENABLE();	// 使能LTDC时钟

	__HAL_RCC_GPIOE_CLK_ENABLE();	// 使能IO口时钟
	__HAL_RCC_GPIOI_CLK_ENABLE();
	__HAL_RCC_GPIOF_CLK_ENABLE();
	__HAL_RCC_GPIOA_CLK_ENABLE();
	__HAL_RCC_GPIOH_CLK_ENABLE();
	__HAL_RCC_GPIOG_CLK_ENABLE();
	__HAL_RCC_GPIOD_CLK_ENABLE();

/*------------------------------LTDC GPIO Configuration--------------------------
		
    PG13    ------> LTDC_R0		PE5     ------> LTDC_G0			PG14    ------> LTDC_B0
    PA2     ------> LTDC_R1		PE6     ------> LTDC_G1			PG12    ------> LTDC_B1
    PH8     ------> LTDC_R2		PH13    ------> LTDC_G2			PD6     ------> LTDC_B2
    PH9     ------> LTDC_R3		PH14    ------> LTDC_G3			PA8     ------> LTDC_B3
    PH10    ------> LTDC_R4		PH15    ------> LTDC_G4			PI4     ------> LTDC_B4
    PH11    ------> LTDC_R5		PI0     ------> LTDC_G5			PI5     ------> LTDC_B5
    PH12    ------> LTDC_R6		PI1     ------> LTDC_G6			PI6     ------> LTDC_B6
    PG6     ------> LTDC_R7		PI2     ------> LTDC_G7			PI7     ------> LTDC_B7
	 
    PG7     ------> LTDC_CLK	
    PF10    ------> LTDC_DE	 
    PI9     ------> LTDC_VSYNC
    PI10    ------> LTDC_HSYNC
--*/	 
		
	GPIO_InitStruct.Pin = GPIO_PIN_5|GPIO_PIN_6;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate = GPIO_AF14_LTDC;
	HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = GPIO_PIN_9|GPIO_PIN_10|GPIO_PIN_0|GPIO_PIN_1
						  |GPIO_PIN_2|GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_6
						  |GPIO_PIN_7;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate = GPIO_AF14_LTDC;
	HAL_GPIO_Init(GPIOI, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = GPIO_PIN_10;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate = GPIO_AF14_LTDC;
	HAL_GPIO_Init(GPIOF, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = GPIO_PIN_2;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate = GPIO_AF14_LTDC;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = GPIO_PIN_8|GPIO_PIN_9|GPIO_PIN_10|GPIO_PIN_11
						  |GPIO_PIN_12|GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate = GPIO_AF14_LTDC;
	HAL_GPIO_Init(GPIOH, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = GPIO_PIN_6|GPIO_PIN_7|GPIO_PIN_12|GPIO_PIN_13
						  |GPIO_PIN_14;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate = GPIO_AF14_LTDC;
	HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = GPIO_PIN_8;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate = GPIO_AF13_LTDC;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = GPIO_PIN_6;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate = GPIO_AF14_LTDC;
	HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

	// 初始化背光引脚

	GPIO_InitStruct.Pin 	= GPIO_PIN_6;				// 背光引脚
	GPIO_InitStruct.Mode 	= GPIO_MODE_OUTPUT_PP;			// 推挽输出模式
	GPIO_InitStruct.Pull 	= GPIO_NOPULL;						// 无上下拉
	GPIO_InitStruct.Speed 	= GPIO_SPEED_FREQ_LOW;			// 速度等级低
	HAL_GPIO_Init(GPIOH, &GPIO_InitStruct);	// 初始化

	LCD_BK_Ctrl(false);	// 先关闭背光引脚，初始化之后再开启
}

void HW_LCD_Init(LTDC_HandleTypeDef* hltdc){
	
	__HAL_RCC_LTDC_CLK_ENABLE();	// 开启LTDC时钟
	__HAL_RCC_DMA2D_CLK_ENABLE();	// 使能DMA2D
	LTDC_GPIO_Init();
	hltdc->Instance			= LTDC;		
	hltdc->Init.HSPolarity 	= LTDC_HSPOLARITY_AL;	// 低电平有效
	hltdc->Init.VSPolarity 	= LTDC_VSPOLARITY_AL;	// 低电平有效
	hltdc->Init.DEPolarity 	= LTDC_DEPOLARITY_AL;	// 低电平有效，要注意的是，很多面板都是高电平有效，但是429需要设置成低电平才能正常显示
	hltdc->Init.PCPolarity 	= LTDC_PCPOLARITY_IPC;	// 正常时钟信号

	hltdc->Init.HorizontalSync 		= HSW - 1;									// 根据屏幕设置参数即可
	hltdc->Init.VerticalSync 		= VSW - 1;
	hltdc->Init.AccumulatedHBP		= HBP + HSW -1;
	hltdc->Init.AccumulatedVBP 		= VBP + VSW -1;
	hltdc->Init.AccumulatedActiveW 	= LCD_Width  + HSW + HBP -1;
	hltdc->Init.AccumulatedActiveH 	= LCD_Height + VSW + VBP -1;
	hltdc->Init.TotalWidth 			= LCD_Width  + HSW + HBP + HFP - 1; 
	hltdc->Init.TotalHeigh 			= LCD_Height + VSW + VBP + VFP - 1;
	hltdc->Init.Backcolor.Blue 		= 0;
	hltdc->Init.Backcolor.Green 	= 0;
	hltdc->Init.Backcolor.Red 		= 0;
	
	HAL_LTDC_Init(hltdc);	// 初始化LTDC
	HAL_NVIC_SetPriority(LTDC_IRQn,LTDC_ISR_PRIO,0);
	HAL_NVIC_EnableIRQ(LTDC_IRQn); 
	HAL_LTDC_ProgramLineEvent(hltdc, 0);//开启行中断
}

void HW_LCD_Layer_Config(LTDC_HandleTypeDef* hltdc,void* Frame_Buf,uint32_t color_format){
	LTDC_LayerCfgTypeDef pLayerCfg = {0};		// layer0 相关参数
	pLayerCfg.WindowX0 			= 0;										// 水平起点
	pLayerCfg.WindowX1 			= LCD_Width;							// 水平终点
	pLayerCfg.WindowY0 			= 0;										// 垂直起点
	pLayerCfg.WindowY1 			= LCD_Height;							// 垂直终点
	pLayerCfg.ImageWidth 		= LCD_Width;                     // 显示区域宽度
	pLayerCfg.ImageHeight 		= LCD_Height;                    // 显示区域高度	
	pLayerCfg.PixelFormat 		= color_format;							// 颜色格式	

	pLayerCfg.Alpha 				= 255;									// 取值范围0~255，255表示不透明，0表示完全透明

// 设置 layer0 的层混合系数，最终写入 LTDC_LxBFCR 寄存器 
// 该参数用于设置 layer0 和 底层背景 之间的颜色混合系数，计算公式为 ：
// 混合后的颜色 =  BF1 * layer0的颜色 + BF2 * 底层背景的颜色
// 如果 layer0 使用了透明色，则必须配置成 LTDC_BLENDING_FACTOR1_PAxCA 和 LTDC_BLENDING_FACTOR2_PAxCA，否则ARGB中的A通道不起作用
//	更多的介绍可以查阅 参考手册关于 LTDC_LxBFCR 寄存器的介绍
	pLayerCfg.BlendingFactor1 	= LTDC_BLENDING_FACTOR1_CA;		// 混合系数1
	pLayerCfg.BlendingFactor2 	= LTDC_BLENDING_FACTOR2_CA;      // 混合系数2
	
	pLayerCfg.FBStartAdress 	= (uint32_t)Frame_Buf;                 // 显存地址

// 配置 layer0 的初始默认颜色，包括A,R,G,B 的值 ，最终写入 LTDC_LxDCCR 寄存器 
	pLayerCfg.Alpha0 				= 0;			// 初始颜色，A
	pLayerCfg.Backcolor.Blue 	= 0;        //	初始颜色，R
	pLayerCfg.Backcolor.Green 	= 0;        //	初始颜色，G
	pLayerCfg.Backcolor.Red 	= 0;			//	初始颜色，B 
  
	HAL_LTDC_ConfigLayer(hltdc, &pLayerCfg, 0);		// 配置layer0
}


