#include "hw_temp.h"
#include "iic_software.h"

#define LM75A_ADDR		0x48
#define LM75A_TEMP		0x00
#define LM75A_SET		0x01
#define LM75A_THYST		0x02
#define LM75A_TOS		0x03

#define LM75A_IIC_SDA_PORT	GPIOI
#define LM75A_IIC_SDA_PIN	GPIO_PIN_3
#define LM75A_IIC_SCL_PORT	GPIOD
#define LM75A_IIC_SCL_PIN	GPIO_PIN_7
								
__STATIC_FORCEINLINE bool LM75A_Read(uint8_t addr,uint8_t* buf,uint16_t len){
	return IIC_Read_General(LM75A_IIC_SCL_PORT,LM75A_IIC_SDA_PORT,\
							LM75A_IIC_SCL_PIN,LM75A_IIC_SDA_PIN,LM75A_ADDR,addr,buf,len);
}

__STATIC_FORCEINLINE bool LM75A_Write(uint8_t addr,const uint8_t* buf,uint16_t len){
	return IIC_Write_General(LM75A_IIC_SCL_PORT,LM75A_IIC_SDA_PORT,\
							LM75A_IIC_SCL_PIN,LM75A_IIC_SDA_PIN,LM75A_ADDR,addr,buf,len);
}

__STATIC_FORCEINLINE bool LM75A_WriteOneByte(uint8_t addr,uint8_t data){
	return LM75A_Write(addr,&data,1);
}

//温度上限
void Temp_Set_Tos(float temp){
	uint16_t data=temp/(float)0.5;
	uint8_t buf[2];
	buf[0]=data>>1;
	buf[1]=data<<7;
	LM75A_Write(LM75A_TOS,buf,2);
}
//温度下限
void Temp_Set_Thyst(float temp){
	uint16_t data=temp/(float)0.5;
	uint8_t buf[2];
	buf[0]=data>>1;
	buf[1]=data<<7;
	LM75A_Write(LM75A_THYST,buf,2);
}

void HW_Temp_Init(void){
	GPIO_InitTypeDef GPIO_InitStruct;
	
	__HAL_RCC_GPIOD_CLK_ENABLE();
	__HAL_RCC_GPIOI_CLK_ENABLE();
	
	GPIO_InitStruct.Mode=GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pin=GPIO_PIN_3;
	GPIO_InitStruct.Pull=GPIO_PULLUP;
	GPIO_InitStruct.Speed=GPIO_SPEED_LOW;
	
	HAL_GPIO_Init(GPIOI,&GPIO_InitStruct);
	
	GPIO_InitStruct.Pin=GPIO_PIN_7;
	
	HAL_GPIO_Init(GPIOD,&GPIO_InitStruct);
	LM75A_WriteOneByte(LM75A_SET,0);
	Temp_Set_Tos(34);
	Temp_Set_Thyst(30);
}

float Get_Temp(void){
	uint16_t data=0;
	uint8_t buf[2];
	LM75A_Read(0,buf,2);
	data|=buf[0]<<8;
	data|=buf[1];
	data>>=5;
//	printf("raw temp data:0x%x\r\n",data);
	return data&(1<<10) ? (int16_t)(data|0xf800)*0.125:data*0.125;
}


