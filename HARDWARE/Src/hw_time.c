#include "hw_time.h"

static RTC_HandleTypeDef RTC_Handler;
RTC_TimeTypeDef RTC_Time;
RTC_DateTypeDef RTC_Date;

static void RTC_Base_Init(void){
	RCC_OscInitTypeDef RCC_OscInitStruct;
    RCC_PeriphCLKInitTypeDef PeriphClkInitStruct;
    __HAL_RCC_RTC_CLK_ENABLE();     //使能RTC时钟
	HAL_PWR_EnableBkUpAccess();     //取消备份区域写保护
    
    RCC_OscInitStruct.OscillatorType=RCC_OSCILLATORTYPE_LSE;//LSE配置
    RCC_OscInitStruct.PLL.PLLState=RCC_PLL_NONE;
    RCC_OscInitStruct.LSEState=RCC_LSE_ON;                  //RTC使用LSE
    HAL_RCC_OscConfig(&RCC_OscInitStruct);

    PeriphClkInitStruct.PeriphClockSelection=RCC_PERIPHCLK_RTC;//外设为RTC
    PeriphClkInitStruct.RTCClockSelection=RCC_RTCCLKSOURCE_LSE;//RTC时钟源为LSE
    HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct);
        
    __HAL_RCC_RTC_ENABLE();//RTC时钟使能
}

bool RTC_Set_Time(uint8_t hour,uint8_t min,uint8_t sec){
	RTC_TimeTypeDef RTC_TimeStructure;
	
	RTC_TimeStructure.Hours=hour;
	RTC_TimeStructure.Minutes=min;
	RTC_TimeStructure.Seconds=sec;
	RTC_TimeStructure.TimeFormat=RTC_HOURFORMAT12_PM;
	RTC_TimeStructure.DayLightSaving=RTC_DAYLIGHTSAVING_NONE;
    RTC_TimeStructure.StoreOperation=RTC_STOREOPERATION_RESET;
	return HAL_RTC_SetTime(&RTC_Handler,&RTC_TimeStructure,RTC_FORMAT_BIN)==HAL_OK;	
}

bool RTC_Set_Date(uint8_t year,uint8_t month,uint8_t date,uint8_t week){
	RTC_DateTypeDef RTC_DateStructure;
    
	RTC_DateStructure.Date=date;
	RTC_DateStructure.Month=month;
	RTC_DateStructure.WeekDay=week;
	RTC_DateStructure.Year=year;
	return HAL_RTC_SetDate(&RTC_Handler,&RTC_DateStructure,RTC_FORMAT_BIN)==HAL_OK;
}

bool RTC_Get_Time(RTC_TimeTypeDef* RTC_TimeStructure){
	return HAL_RTC_GetTime(&RTC_Handler,RTC_TimeStructure,RTC_FORMAT_BIN)==HAL_OK;
}

bool RTC_Get_Date(RTC_DateTypeDef* RTC_DateStructure){
	return HAL_RTC_GetDate(&RTC_Handler,RTC_DateStructure,RTC_FORMAT_BIN)==HAL_OK;
}

bool RTC_Time_Fresh(void){
	return RTC_Get_Time(&RTC_Time)&&RTC_Get_Date(&RTC_Date);
}

bool HW_RTC_Init(void){
	RTC_Base_Init();
	RTC_Handler.Instance=RTC;
    RTC_Handler.Init.HourFormat=RTC_HOURFORMAT_24;//RTC设置为24小时格式 
    RTC_Handler.Init.AsynchPrediv=0X7F;           //RTC异步分频系数(1~0X7F)
    RTC_Handler.Init.SynchPrediv=0XFF;            //RTC同步分频系数(0~7FFF)   
    RTC_Handler.Init.OutPut=RTC_OUTPUT_DISABLE;     
    RTC_Handler.Init.OutPutPolarity=RTC_OUTPUT_POLARITY_HIGH;
    RTC_Handler.Init.OutPutType=RTC_OUTPUT_TYPE_OPENDRAIN;
    if(HAL_RTC_Init(&RTC_Handler)!=HAL_OK) 
		return false;
      
    if(HAL_RTCEx_BKUPRead(&RTC_Handler,RTC_BKP_DR0)!=0X5050)//是否第一次配置
    { 
        RTC_Set_Time(19,20,0);	        //设置时间 ,根据实际时间修改
		RTC_Set_Date(21,7,11,7);		                    //设置日期
        HAL_RTCEx_BKUPWrite(&RTC_Handler,RTC_BKP_DR0,0X5050);//标记已经初始化过了
    }
    return RTC_Time_Fresh();
}






