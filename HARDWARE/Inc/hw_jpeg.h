#ifndef HW_JPEG_H__
#define HW_JPEG_H__

#include "stm32h7xx_sys.h"
#ifdef __cplusplus
 extern "C" {
#endif
bool HW_JPEG_Init(void);
bool HW_JPEG_Decode_DMA(uint8_t* pdata_in,uint32_t InLen, uint8_t* pdata_out,uint32_t OutLen);
bool HW_JPEG_Get_Info(JPEG_ConfTypeDef* info);
extern void HAL_JPEG_InfoReadyCallback(JPEG_HandleTypeDef *hjpeg, JPEG_ConfTypeDef *pInfo);
extern void HAL_JPEG_GetDataCallback(JPEG_HandleTypeDef *hjpeg, uint32_t NbDecodedData);
extern void HAL_JPEG_DataReadyCallback (JPEG_HandleTypeDef *hjpeg, uint8_t *pDataOut, uint32_t OutDataLength);
extern void HAL_JPEG_DecodeCpltCallback(JPEG_HandleTypeDef *hjpeg);
#ifdef __cplusplus
}
#endif

#endif
