#ifndef HW_MUSIC_H__
#define HW_MUSIC_H__

#include "stm32h7xx_sys.h"
#ifdef __cplusplus
 extern "C" {
#endif

bool HW_Music_Init(void);
bool HW_Muisc_Config_Check_Param(uint32_t datasize,uint32_t samplerate,void* buf0,void* buf1,uint32_t data_len);
bool HW_Music_Config(uint32_t datasize,uint32_t samplerate,void* buf0,void* buf1,uint32_t data_len);
void HW_Music_Start(void);
void HW_Music_Stop(void);
void HW_Muisc_Set_HP_Vol(uint8_t l_vol,uint8_t r_vol);
void HW_Music_Set_SPK_Vol(uint8_t vol);
uint8_t HW_Music_Get_Buf_Index(void);
void Music_Buf0_Over(void);
void Music_Buf1_Over(void);


#ifdef __cplusplus
}
#endif

#endif
