#ifndef HW_QSPI_FLASH_H__
#define HW_QSPI_FLASH_H__

#include "stm32h7xx_sys.h"

#define W25Qxx_OK           			0		// W25Qxx通信正常
#define W25Qxx_ERROR_INIT         		-1		// 初始化错误
#define W25Qxx_ERROR_WriteEnable       -2		// 写使能错误
#define W25Qxx_ERROR_AUTOPOLLING       -3		// 轮询等待错误，无响应
#define W25Qxx_ERROR_Erase         		-4		// 擦除错误
#define W25Qxx_ERROR_TRANSMIT         	-5		// 传输错误
#define W25Qxx_ERROR_MemoryMapped		-6    // 内存映射模式错误
#define W25Qxx_FlashSize       		0x800000		// W25Q64大小，8M字节
#define W25Qxx_Mem_Addr				0x90000000 	// 内存映射模式的地址
#ifdef __cplusplus
 extern "C" {
#endif
int8_t HW_QSPI_Flash_Init(void);
int8_t HW_QSPI_Flash_WriteBuffer_NoCheck(uint8_t* pBuffer, uint32_t WriteAddr, uint32_t Size);
int8_t HW_QSPI_Flash_ReadBuffer(uint8_t* pBuffer, uint32_t ReadAddr, uint32_t NumByteToRead);
int8_t HW_QSPI_Flash_ChipErase (void);
int8_t HW_QSPI_Flash_SectorErase(uint32_t SectorAddress);
int8_t HW_QSPI_Flash_BlockErase_32K (uint32_t SectorAddress);
int8_t HW_QSPI_Flash_BlockErase_64K (uint32_t SectorAddress);
int8_t HW_QSPI_Flash_MemoryMappedMode(void);
#ifdef __cplusplus
}
#endif

#endif
