#ifndef HW_LED_H_
#define HW_LED_H_

#include "stm32h7xx_sys.h"

__STATIC_FORCEINLINE void LED0(bool n){
	HAL_GPIO_WritePin(GPIOH,GPIO_PIN_7,n?GPIO_PIN_RESET:GPIO_PIN_SET);
}

__STATIC_FORCEINLINE void LED0_Toggle(void){
	HAL_GPIO_TogglePin(GPIOH,GPIO_PIN_7);
}

#ifdef __cplusplus
 extern "C" {
#endif

void HW_LED_Init(void);

#ifdef __cplusplus
}
#endif
#endif
