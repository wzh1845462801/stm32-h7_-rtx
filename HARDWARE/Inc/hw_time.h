#ifndef HW_TIME_H__
#define HW_TIME_H__

#include "stm32h7xx_sys.h"
#ifdef __cplusplus
 extern "C" {
#endif
extern RTC_TimeTypeDef RTC_Time;
extern RTC_DateTypeDef RTC_Date;
bool HW_RTC_Init(void);
bool RTC_Set_Time(uint8_t hour,uint8_t min,uint8_t sec);
bool RTC_Set_Date(uint8_t year,uint8_t month,uint8_t date,uint8_t week);
bool RTC_Get_Time(RTC_TimeTypeDef* RTC_TimeStructure);
bool RTC_Get_Date(RTC_DateTypeDef* RTC_DateStructure);
bool RTC_Time_Fresh(void);
#ifdef __cplusplus
}
#endif
#endif
