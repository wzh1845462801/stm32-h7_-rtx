#ifndef HW_TEMP_H__
#define HW_TEMP_H__

#include "stm32h7xx_sys.h"
#ifdef __cplusplus
 extern "C" {
#endif
void HW_Temp_Init(void);
float Get_Temp(void);
void Temp_Set_Tos(float temp);
void Temp_Set_Thyst(float temp);
#ifdef __cplusplus
}
#endif
#endif
