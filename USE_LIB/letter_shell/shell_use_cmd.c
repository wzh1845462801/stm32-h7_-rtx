#include "shell.h"
#include "dl_lib.h"
#include "stm32h7xx_sys.h"
#include "log.h"
#include "sdcard_rtos.h"
#include "music_device.h"
#include "setting_save.h"
#include "shell_ext.h"
#include "fatfs.h"

static int run(int argc,char* argv[] );
SHELL_EXPORT_CMD(
SHELL_CMD_PERMISSION(0)|SHELL_CMD_TYPE(SHELL_TYPE_CMD_MAIN),
run, 
"run an elf file\r\n"
"run [filename] [argv...]\r\n");

static int run(int argc,char* argv[] ){
	DL_Err_Type err;
	int ret;
	if(argc==1){
		printf("%s",shellDescrun);
		return 0;
	}
	else if(argc<2){
		logError("\r\n参数错误：命令执行失败\r\n");
		return -1;
	}
	err=dl_exec_file(argv[1],argc-1,argv+1,&ret);
	switch(err){
		case DL_NO_ERR:
			return ret;
		case DL_FILE_INVALID:
			logError("%s:文件信息错误\r\n",argv[1]);
			return -1;
		case DL_MALLOC_FAIL:
			logError("%s:内存分配失败\r\n",argv[1]);
			return -1;
		case DL_MEMSIZE_ERR:
			logError("%s:分配内存信息错误\r\n",argv[1]);
			return -1;
		case DL_RELOCATE_ERR:
			logError("%s:重定向操作不支持\r\n",argv[1]);
			return -1;
		case DL_UNDEF_SYM_ERR:
			logError("%s:文件解析时出现未定义符号表\r\n",argv[1]);
			return -1;
		case DL_FILE_OPEN_ERR:
			logError("%s:文件读取失败\r\n",argv[1]);
			return -1;
		case DL_PLATFORM_ERR:
			logError("%s:elf文件不支持当前的硬件\r\n",argv[1]);
			return -1;
		case DL_ELF_NOT_SUPPRORT:
			logError("%s:当前elf文件格式不支持\r\n",argv[1]);
			return -1;
		case DL_ELF_NO_ENTRY:
			logError("%s:当前elf文件无程序入口点\r\n",argv[1]);
			return -1;
		default:
			return -1;
	}
}	



static int reset(int argc,char* argv[]){
	HAL_NVIC_SystemReset();
	return 0;
}

SHELL_EXPORT_CMD(SHELL_CMD_PERMISSION(0)|SHELL_CMD_TYPE(SHELL_TYPE_CMD_MAIN)|SHELL_CMD_DISABLE_RETURN,
reset,
"reset system");


static int echo(int argc,char* argv[]);
SHELL_EXPORT_CMD(SHELL_CMD_PERMISSION(0)|SHELL_CMD_TYPE(SHELL_TYPE_CMD_MAIN)|SHELL_CMD_DISABLE_RETURN,
echo,
"print string\r\n"
"echo [string]\r\n");
static int echo(int argc,char* argv[]){
	if(argc==1){
		printf("%s",shellDescecho);
		return 0;
	}
	else if(argc!=2){
		logError("\r\n参数错误：命令执行失败\r\n");
		return -1;
	}
	printf("%s\r\n",argv[1]);
	return 0;
}


static void Byte_To_Any_Unit(uint64_t bytes,float* num,const char** pstr){
	static const char* use_unit[]={
		"B","KB","MB","GB","TB"
	};
	uint32_t i;
	uint64_t buf_byte=bytes;
	for(i=0;i<sizeof(use_unit)/sizeof(use_unit[0]);i++){
		if(bytes<1024)
			break;
		bytes/=1024;
	}
	if(i==sizeof(use_unit)/sizeof(use_unit[0]))
		i--;
	*num=(float)buf_byte/pow(1024.0f,i);
	*pstr=use_unit[i];
}

static uint32_t get_dec_bits(uint32_t num){
	uint32_t i=1;
	while(num/10){
		num/=10;
		i++;
	}
	return i;
}

static void print_heap_state(void(*get_heap_state)(Sys_Heap_State*)){
	Sys_Heap_State buf_state;
	float buf_num;
	const char* use_unit;
	uint32_t max_dec_bits=0;
	
	uint32_t temp_buf[7];
	
	get_heap_state(&buf_state);
	printf("free_node_num     :%d\r\n",buf_state.free_node_num);
	printf("used_node_num     :%d\r\n",buf_state.used_node_num);
	
	temp_buf[0]=buf_state.max_free_node_size;
	temp_buf[1]=buf_state.min_free_node_size;
	temp_buf[2]=buf_state.max_used_node_size;
	temp_buf[3]=buf_state.min_used_node_size;
	temp_buf[4]=buf_state.total_size;
	temp_buf[5]=buf_state.free_size;
	temp_buf[6]=buf_state.used_size;
	
	for(size_t i=0;i<7;i++){
		temp_buf[i]=get_dec_bits(temp_buf[i]);
		if(max_dec_bits==0||temp_buf[i]>max_dec_bits)
			max_dec_bits=temp_buf[i];
	}
	
	Byte_To_Any_Unit(buf_state.max_free_node_size,&buf_num,&use_unit);
	printf("max_free_node_size:%-*d - %-7.2f%s\r\n",max_dec_bits,buf_state.max_free_node_size,buf_num,use_unit);
	
	Byte_To_Any_Unit(buf_state.min_free_node_size,&buf_num,&use_unit);
	printf("min_free_node_size:%-*d - %-7.2f%s\r\n",max_dec_bits,buf_state.min_free_node_size,buf_num,use_unit);
	
	Byte_To_Any_Unit(buf_state.max_used_node_size,&buf_num,&use_unit);
	printf("max_used_node_size:%-*d - %-7.2f%s\r\n",max_dec_bits,buf_state.max_used_node_size,buf_num,use_unit);
	
	Byte_To_Any_Unit(buf_state.min_used_node_size,&buf_num,&use_unit);
	printf("min_used_node_size:%-*d - %-7.2f%s\r\n",max_dec_bits,buf_state.min_used_node_size,buf_num,use_unit);
	
	Byte_To_Any_Unit(buf_state.total_size,&buf_num,&use_unit);
	printf("total_size        :%-*d - %-7.2f%s\r\n",max_dec_bits,buf_state.total_size,buf_num,use_unit);
	
	Byte_To_Any_Unit(buf_state.free_size,&buf_num,&use_unit);
	printf("free_size         :%-*d - %-7.2f%s\r\n",max_dec_bits,buf_state.free_size,buf_num,use_unit);
	
	Byte_To_Any_Unit(buf_state.used_size,&buf_num,&use_unit);
	printf("used_size         :%-*d - %-7.2f%s\r\n",max_dec_bits,buf_state.used_size,buf_num,use_unit);
	
	printf("remain percent    : %4.2f%%\r\n",(float)buf_state.free_size/(float)buf_state.total_size*100.0);
}

static int heap_dump(int argc,char* argv[]){
	printf("<-------Heap State------->\r\n");
	print_heap_state(sys_get_heap_state);
	printf("<-----RTOS Heap State---->\r\n");
	print_heap_state(rtos_get_heap_state);
	return 0;
}
SHELL_EXPORT_CMD(SHELL_CMD_PERMISSION(0)|SHELL_CMD_TYPE(SHELL_TYPE_CMD_MAIN)|SHELL_CMD_DISABLE_RETURN,
heap_dump,
"dump heap state");

static int usb(int argc,char* argv[]);
SHELL_EXPORT_CMD(SHELL_CMD_PERMISSION(0)|SHELL_CMD_TYPE(SHELL_TYPE_CMD_MAIN)|SHELL_CMD_DISABLE_RETURN,
usb,
"usb ctrl\r\n"
"usb [param]\r\n"
"     param: -ow open usb write\r\n"
"            -or open usb read\r\n"
"            -c  close usb\r\n"
);
static int usb(int argc,char* argv[]){
	if(argc==1){
		printf("%s",shellDescusb);
		return 0;
	}
	if(argc!=2){
		logError("\r\n参数错误：命令执行失败\r\n");
		return 0;
	}
	if(strcmp(argv[1],"-ow")==0){
		SDCard_Set_State(NO_FATFS_USB_WR);
	}
	else if(strcmp(argv[1],"-or")==0){
		SDCard_Set_State(FATFS_R_USB_R);
	}
	else if(strcmp(argv[1],"-c")==0){
		SDCard_Set_State(FATFS_WR_NO_USB);
	}
	else{
		logError("\r\n参数错误：命令执行失败\r\n");
		return 0;
	}
	return 0;
}


static void music_use_end_hook(void* p){
	Music_Handle* ppHandle=p;
	Music_Delete(*ppHandle);
	*ppHandle=NULL;
}

static int music(int argc,char* argv[]);
SHELL_EXPORT_CMD(SHELL_CMD_PERMISSION(0)|SHELL_CMD_TYPE(SHELL_TYPE_CMD_MAIN)|SHELL_CMD_DISABLE_RETURN,
music,
"music player\r\n"
"music [param]\r\n"
"       param: -c [filename] create and open music\r\n"
"              -r remove music\r\n"
"              -s [subparam] set music volume\r\n"
"                  subparam: -spk [num] set spk volume\r\n"
"                            -hp [num] set hp volume\r\n"
"                            -gain [num] set gain num\r\n"
);
static int music(int argc,char* argv[]){
	static Music_Handle use_handle=NULL;
	if(argc==1){
		printf("%s",shellDescmusic);
		return 0;
	}
	if(strcmp(argv[1],"-c")==0){
		Music_InitType info;
		if(argc!=3){
			logError("\r\n参数错误：命令执行失败\r\n");
			return 0;
		}
		Music_Delete(use_handle);
		memset(&info,0,sizeof(info));
		info.end_hook=music_use_end_hook;
		info.priority=1;
		info.use_ptr=&use_handle;
		use_handle=Music_Create(argv[2],&info);
		if(use_handle==NULL){
			logError("\r\n音乐播放失败\r\n");
			return 0;
		}
		Music_Start(use_handle);
	}
	else if(strcmp(argv[1],"-r")==0){
		if(argc!=2){
			logError("\r\n参数错误：命令执行失败\r\n");
			return 0;
		}
		Music_Delete(use_handle);
		use_handle=NULL;
	}
	else if(strcmp(argv[1],"-s")==0){
		if(argc!=4){
			logError("\r\n参数错误：命令执行失败\r\n");
			return 0;
		}
		if(strcmp(argv[2],"-spk")==0){
			Music_Set_SPK_Vol((int)shellExtParsePara(shellGetCurrent(),argv[3]));
		}
		else if(strcmp(argv[2],"-hp")==0){
			int vol=(int)shellExtParsePara(shellGetCurrent(),argv[3]);
			Music_Set_HP_Vol(vol,vol);
		}
		else if(strcmp(argv[2],"-gain")==0){
			Music_Set_Volume_Gain((int)shellExtParsePara(shellGetCurrent(),argv[3]));
		}
		else{
			logError("\r\n参数错误：命令执行失败\r\n");
			return 0;
		}
	}
	return 0;
}

static const char* System_Head_Text[]={
	"task name",
	"task prio",
	"task state",
	"stack remain",
};
static const char* Task_State_To_Str(osThreadState_t pstate){
	switch(pstate){
		case osThreadInactive :
			return "inactive";
		case osThreadReady :
			return "ready";
		case osThreadRunning :
			return "running";
		case osThreadBlocked:
			return "blocked";
		case osThreadTerminated:
			return "terminated";
		default:
			return "unknown";
	}
}

static const uint32_t str_buf_size=20;
static bool System_Task_View_Text_Buf_Create(char**** pBuf,uint32_t* task_num){
	uint32_t real_num;
	
	size_t i,j;
	*task_num=osThreadGetCount();
	osThreadId_t* id_buf=(osThreadId_t*)malloc(sizeof(osThreadId_t)*(*task_num));
	if(id_buf==NULL){
		goto fail0;
	}
	real_num=osThreadEnumerate(id_buf,*task_num);
	if(real_num<*task_num){
		*task_num=real_num;
	}
	*pBuf=(char***)malloc(sizeof(char**)*(*task_num));
	if(*pBuf==NULL){
		goto fail1;
	}
	for(i=0;i<*task_num;i++){
		const char* pstr; 
		(*pBuf)[i]=(char**)malloc(sizeof(char*)*(sizeof(System_Head_Text)/sizeof(System_Head_Text[0])));
		if((*pBuf)[i]==NULL){
			goto fail2;
		}
		for(j=0;j<sizeof(System_Head_Text)/sizeof(System_Head_Text[0]);j++){
			(*pBuf)[i][j]=(char*)malloc(sizeof(char)*str_buf_size);
			if((*pBuf)[i][j]==NULL){
				goto fail3;
			}
		}
		pstr=osThreadGetName(id_buf[i]);
		if(pstr==NULL)
			pstr="anonymous thread";
		strncpy((*pBuf)[i][0],pstr,str_buf_size);
		snprintf((*pBuf)[i][1],str_buf_size,"%d",osThreadGetPriority(id_buf[i]));
		strncpy((*pBuf)[i][2],Task_State_To_Str(osThreadGetState(id_buf[i])),str_buf_size);
		snprintf((*pBuf)[i][3],str_buf_size,"%2.1f%%",100*((float)osThreadGetStackSpace(id_buf[i]))/((float)osThreadGetStackSize(id_buf[i])));
	}
	free(id_buf);
	return true;
fail3:
	for(size_t k=0;k<j;k++){
		free((*pBuf)[i][k]);
	}
	free((*pBuf)[i]);
fail2:
	for(size_t k=0;k<i;k++){
		for(size_t m=0;m<sizeof(System_Head_Text)/sizeof(System_Head_Text[0]);m++){
			free((*pBuf)[k][m]);
		}
		free((*pBuf)[k]);
	}
	free(*pBuf);
fail1:
	free(id_buf);
fail0:
	*pBuf=NULL;
	*task_num=0;
	return false;
}	

static void System_Task_View_Text_Buf_Delete(char*** pBuf,uint32_t task_num){
	for(size_t i=0;i<task_num;i++){
		for(size_t j=0;j<sizeof(System_Head_Text)/sizeof(System_Head_Text[0]);j++){
			free(pBuf[i][j]);
		}
		free(pBuf[i]);
	}
	free(pBuf);
}

static int list_thread(int argc,char* argv[]){
	char*** text_buf;
	uint32_t task_num;
	if(System_Task_View_Text_Buf_Create(&text_buf,&task_num)==false){
		logError("\r\n内存不足\r\n");
		return 0;
	}
	
	for(size_t i=0;i<(str_buf_size+2)*sizeof(System_Head_Text)/sizeof(System_Head_Text[0]);i++){
		putchar('-');
	}
	printf("\r\n");
	
	for(size_t i=0;i<sizeof(System_Head_Text)/sizeof(System_Head_Text[0]);i++){
		printf("|%-*s|",str_buf_size,System_Head_Text[i]);
	}
	printf("\r\n");
	
	for(size_t i=0;i<(str_buf_size+2)*sizeof(System_Head_Text)/sizeof(System_Head_Text[0]);i++){
		putchar('-');
	}
	printf("\r\n");
	
	for(size_t i=0;i<task_num;i++){
		for(size_t j=0;j<sizeof(System_Head_Text)/sizeof(System_Head_Text[0]);j++){
			printf("|%-*s|",str_buf_size,text_buf[i][j]);
		}
		printf("\r\n");
		for(size_t i=0;i<(str_buf_size+2)*sizeof(System_Head_Text)/sizeof(System_Head_Text[0]);i++){
			putchar('-');
		}
		printf("\r\n");
	}
	System_Task_View_Text_Buf_Delete(text_buf,task_num);
	return 0;
}

SHELL_EXPORT_CMD(SHELL_CMD_PERMISSION(0)|SHELL_CMD_TYPE(SHELL_TYPE_CMD_MAIN)|SHELL_CMD_DISABLE_RETURN,
list_thread,
"list all thread\r\n"
);

static int reburn(int argc,char* argv[]){
	bool need_reset;
	
	if(argc==1)
		need_reset=true;
	else if(argc==2){
		if(strcmp(argv[1],"-n")!=0){
			logError("\r\n参数错误\r\n");
			return 0;
		}
		need_reset=false;
	}
	else{
		logError("\r\n参数错误\r\n");
		return 0;
	}
	Common_Setting.fresh_burn=true;
	Create_New_Setting();
	if(need_reset){
		HAL_NVIC_SystemReset();
	}
	return 0;
}

SHELL_EXPORT_CMD(SHELL_CMD_PERMISSION(0)|SHELL_CMD_TYPE(SHELL_TYPE_CMD_MAIN)|SHELL_CMD_DISABLE_RETURN,
reburn,
"reburn resource\r\n"
"re_burn [param]\r\n"
"         param: -n don't reset(default is reset)\r\n"
);

static int save(int argc,char* argv[]){
	Create_New_Setting();
	return 0;
}
SHELL_EXPORT_CMD(SHELL_CMD_PERMISSION(0)|SHELL_CMD_TYPE(SHELL_TYPE_CMD_MAIN)|SHELL_CMD_DISABLE_RETURN,
save,
"save setting"
);

static int show_setting(int argc,char* argv[]){
	FILE* pfile;
	uint32_t file_size;
	char* buf;
	pfile=fopen(setting_path,"r");
	if(pfile==NULL){
		logError("\r\n配置文件缺失\r\n");
		return 0;
	}
	fseek(pfile,0,SEEK_END);
	file_size=ftell(pfile);
	fseek(pfile,0,SEEK_SET);
	buf=malloc(file_size+1);
	if(buf==NULL){
		logError("\r\n内存不足\r\n");
		fclose(pfile);
		return 0;
	}
	fread(buf,file_size,1,pfile);
	fclose(pfile);
	buf[file_size]='\0';
	printf("%s:\r\n%s\r\n",setting_path,buf);
	free(buf);
	return 0;
}
SHELL_EXPORT_CMD(SHELL_CMD_PERMISSION(0)|SHELL_CMD_TYPE(SHELL_TYPE_CMD_MAIN)|SHELL_CMD_DISABLE_RETURN,
show_setting,
"show setting"
);

static int ls(int argc,char* argv[]){
	File_Dir pdir;
	File_Filter music_select={.pattern=NULL,.pattern_num=0};
	argc--;
	argv++;
	char* path;
	if(argc<1){
		path="0:";
		Dir_File_Filter_Add(&music_select,"*");
	}
	else{
		path=argv[0];
		argv++;
		argc--;
		if(argc==0){
			Dir_File_Filter_Add(&music_select,"*");
		}
		else{
			for(size_t i=0;i<argc;i++){
				Dir_File_Filter_Add(&music_select,argv[i]);
			}
		}
	}
	Dir_Scan(path,&music_select,&pdir,0);
	printf("total num:%d\r\n",pdir.item_num);
	TRAVERSE_MY_LIST(pItem,&pdir){
		printf("%s/%s\r\n",path,(char*)pItem->data);
	}
	Dir_Free(&pdir);
	Dir_File_Filter_Destroy(&music_select);
	return 0;
}
SHELL_EXPORT_CMD(SHELL_CMD_PERMISSION(0)|SHELL_CMD_TYPE(SHELL_TYPE_CMD_MAIN)|SHELL_CMD_DISABLE_RETURN,
ls,
"ls"
);


