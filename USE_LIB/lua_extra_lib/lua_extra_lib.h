#ifndef LUA_EXTRA_LIB_H_
#define LUA_EXTRA_LIB_H_
//对LUA的库进行扩展，
#include "stm32h7xx_sys.h"
#include "lua.h"

#define LUA_EXTARLIBNAME			"extra"

#ifdef __cplusplus
 extern "C" {
#endif
LUAMOD_API int luaopen_extra (lua_State *L);
#ifdef __cplusplus
}
#endif
#endif
