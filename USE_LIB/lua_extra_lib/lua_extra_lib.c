#include "lua_extra_lib.h"
#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
#include "hw_led.h"

static int Lua_Led_Ctrl(lua_State* L){
	bool a=luaL_checkinteger(L,1);
	LED0(a);
	return 1;
}

static int Lua_Sleep(lua_State* L){
#if SUPPORT_OS==FREE_RTOS
	vTaskSuspend(NULL);
#elif SUPPORT_OS==RTX_RTOS
	osThreadSuspend(osThreadGetId());
#endif
	return 1;
}

static int Lua_Delay(lua_State* L){
	lua_Integer a=luaL_checkinteger(L,1);
	delay_ms(a);
	return 1;
}

static const struct luaL_Reg mylib [] = {
	{"led", Lua_Led_Ctrl}, 
	{"sleep",Lua_Sleep},
	{"delay",Lua_Delay},
	{NULL, NULL}
};

LUAMOD_API int luaopen_extra (lua_State *L) {
	luaL_newlib(L, mylib);
	return 1;
}
