#ifndef MY_QUEUE_H__
#define MY_QUEUE_H__
//封装了队列的常用操作
#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif 

typedef struct{
	uint8_t* buf;
	size_t buf_size;
	size_t write_ptr;
	size_t read_ptr;
}My_Queue;

static __inline bool My_Queue_Is_Empty(My_Queue* pqueue){
	return pqueue->read_ptr == pqueue->write_ptr;
}

static __inline bool My_Queue_Is_Full(My_Queue* pqueue){
	return (pqueue->write_ptr + 1)%pqueue->buf_size == pqueue->read_ptr;
}

static __inline size_t My_Queue_Data_Num(My_Queue* pqueue){
	return (pqueue->write_ptr - pqueue->read_ptr+pqueue->buf_size)%pqueue->buf_size;
}

void My_Queue_Init(My_Queue* pqueue,uint8_t* buf,size_t buf_size);
size_t My_Queue_Write(My_Queue* pqueue,const uint8_t* buf,size_t size);
size_t My_Queue_Read(My_Queue* pqueue,uint8_t* buf,size_t size);

#ifdef __cplusplus
}
#endif 

#endif
