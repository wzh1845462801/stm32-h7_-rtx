#ifndef SETTING_SAVE_H_
#define SETTING_SAVE_H_
//本文件用来实现相关数据的掉电保存
//数据以结构体的形式呈现，以JSON的格式保存在内存卡中
#include "stm32h7xx_sys.h"
#ifdef __cplusplus
extern "C" {
#endif

typedef struct{
	uint8_t lcd_light;
	const char* back_pic;
	uint8_t spk_volume;
	uint8_t hp_r_volume;
	uint8_t hp_l_volume;
	int16_t soft_volume_gain;
	bool fresh_burn;
}SettingTypedef;

extern SettingTypedef Common_Setting;
extern const char setting_path[];
bool Create_New_Setting(void);
bool Load_Setting(void);
#ifdef __cplusplus
}
#endif
#endif
