#include "flash_burn.h"
#include "hw_qspi_flash.h"
#include "ff.h"
#include "fatfs.h"
#include "setting_save.h"


__STATIC_FORCEINLINE void Flash_Burn_Read(uint8_t* buf,uint32_t addr,uint32_t len){
	HW_QSPI_Flash_ReadBuffer(buf,addr,len);
}

__STATIC_FORCEINLINE void Flash_Burn_Write(uint8_t* buf,uint32_t addr,uint32_t len){
	HW_QSPI_Flash_WriteBuffer_NoCheck(buf,addr,len);
}

/* 字库目录信息类型 */
typedef struct 
{
	char 	      name[RES_NAME_LEN];  /* 资源的名字 */
	uint32_t	  size;      /* 资源的大小 */ 
	uint32_t 	  offset;    /* 资源相对于基地址的偏移 */
}CatalogTypeDef;

//将目录下的文件烧录至flash中
bool flash_burn(const char* dir_path){
	uint32_t 	flash_addr		=BURN_RES_ADDR;
	uint32_t 	real_burn_addr	=flash_addr+MAX_FILE_TO_BURN_NUM*sizeof(CatalogTypeDef);
	FRESULT 	res				=FR_OK;
	uint32_t 	file_num		=0;
	
	FILINFO* 	fileinfo		=(FILINFO*)malloc(sizeof(FILINFO));		//文件信息
	DIR* 		dir				=(DIR*)malloc(sizeof(DIR));  	//目录
	char* 		full_name_buf	=(char*)malloc(FILE_FULL_NAME_LEN);
	FIL* 		file			=(FIL*)malloc(sizeof(FIL));
	uint8_t*    read_buf		=(uint8_t*)malloc(BURN_READ_BUF_SIZE);
	uint8_t*	check_buf		=(uint8_t*)malloc(BURN_READ_BUF_SIZE);
	CatalogTypeDef* pcat		=(CatalogTypeDef*)malloc(sizeof(CatalogTypeDef));
	float		burn_process	=0;
	float    	process_add		=0;
	
	if(read_buf==NULL||file==NULL||dir==NULL||\
		fileinfo==NULL||full_name_buf==NULL||pcat==NULL){
		printf("内存不足！\r\n");
		goto fail0;
	}

	res=f_opendir(dir,dir_path);
	if(res!=FR_OK){
		printf("目录打开失败！\r\n");
		goto fail0;
	}

	while(true){
		UINT br=0;
		char* file_name=NULL;
		res=f_readdir(dir,fileinfo);
		if (res != FR_OK || fileinfo->fname[0] == 0){
			break;  						//错误了/到末尾了,退出
		}
		if(fileinfo->fattrib&AM_DIR)//跳过目录'
			continue;
		if(file_num>=MAX_FILE_TO_BURN_NUM){
			printf("文件个数大于最大烧录数量！\r\n");
			goto fail1;
		}
		file_name=fileinfo->fname;
		printf("准备烧录文件：%s\r\n",file_name);
		
		if(strlen(file_name)+1>RES_NAME_LEN){
			printf("文件名过长无法烧录！\r\n");
			goto fail1;
		}
		if(strlen(dir_path)+strlen(file_name)+2 >FILE_FULL_NAME_LEN){
			printf("文件名和路径名长度之和大于缓冲区长度！\r\n");
			goto fail1;
		}
		snprintf(full_name_buf,FILE_FULL_NAME_LEN,"%s/%s",dir_path,file_name);
		
		res=f_open(file,full_name_buf,FA_READ);
		if(res!=FR_OK){
			printf("烧录文件打开失败！\r\n");
			goto fail1;
		}
		pcat->offset=real_burn_addr;
		strncpy(pcat->name,file_name,RES_NAME_LEN);
		pcat->size=f_size(file);
		burn_process=0;
		process_add=100*(((float)BURN_READ_BUF_SIZE)/f_size(file));
		printf("准备完成，正在烧录。。。\r\n");
		do{
			res=f_read(file,read_buf,BURN_READ_BUF_SIZE,&br);
			if(res!=FR_OK){
				printf("烧录文件读取失败！\r\n");
				goto fail2;
			}
			Flash_Burn_Write(read_buf,real_burn_addr,br);//开始烧录文件
			Flash_Burn_Read(check_buf,real_burn_addr,br);
			for(uint32_t i=0;i<br;i++){
				if(read_buf[i]!=check_buf[i]){
					printf("地址0x%x处烧录错误！\r\n",real_burn_addr+i);
					goto fail2;
				}
			}
			real_burn_addr+=br;
			if(real_burn_addr>W25Qxx_FlashSize){
				printf("FLASH剩余空间不足！\r\n");
				goto fail2;
			}
			burn_process+=process_add;
			if(burn_process>=100)
				burn_process=100;
			printf("烧录进度：%2.2f%%\r",burn_process),fflush(stdout);
		}while(br==BURN_READ_BUF_SIZE);//判断是否读取完毕，若读取完毕则结束本文件烧录
		f_close(file);
		Flash_Burn_Write((uint8_t*)pcat,flash_addr,sizeof(CatalogTypeDef));
		printf("\n文件名：%s\r\n",pcat->name);
		printf("文件大小：%d\r\n",pcat->size);
		printf("文件烧录至flash的地址：0x%x\r\n",pcat->offset);
		flash_addr+=sizeof(CatalogTypeDef);
		file_num++;
	}
	f_closedir(dir);
	free(full_name_buf);
	free(fileinfo);
	free(dir);
	free(file);
	free(read_buf);
	free(check_buf);
	free(pcat);
	printf("所有文件均烧录成功！\r\n");
	return true;
fail2:
	f_close(file);
fail1:
	f_closedir(dir);
fail0:
	free(full_name_buf);
	free(fileinfo);
	free(dir);
	free(file);
	free(read_buf);
	free(check_buf);
	free(pcat);
	printf("烧录失败！\r\n");
	return false;
}

/*
获取资源信息：
resname:资源名称
flash_addr:目录地址
*/
static bool GetResInfo(const char *res_name,uint32_t* size,uint32_t* offset){
	CatalogTypeDef* pcat=malloc(sizeof(CatalogTypeDef));
	if(pcat==NULL){
		printf("内存不足！\r\n");
		*size=0;
		*offset=0;
		return false;
	}
	for(int i=0;i<MAX_FILE_TO_BURN_NUM;i++){
		Flash_Burn_Read((uint8_t*)pcat,BURN_RES_ADDR+i*sizeof(CatalogTypeDef),sizeof(CatalogTypeDef));
		if(strcmp(res_name,pcat->name)==0){
			*offset=pcat->offset;
			*size=pcat->size;
			free(pcat);
			return true;
		}
	}
	*size=0;
	*offset=0;
	free(pcat);
	return false;
}

uint32_t FontHZ24_Offset;
uint32_t FontHZ16_Offset;
bool flash_data_init(void){
	bool res=true;
	uint32_t psize;
	res&=GetResInfo("lv_font_hz16_aw.bin",&psize,&FontHZ16_Offset);
	res&=GetResInfo("lv_font_hz24_aw.bin",&psize,&FontHZ24_Offset);
	if(res==false){
		printf("flash数据预读取失败\r\n");
	}
	if(Common_Setting.fresh_burn){
		printf("flash数据刷新\r\n");
	}
	if(res==false||Common_Setting.fresh_burn){	
		printf("烧录资源文件。。。\r\n");
		printf("全片擦除中。。。\r\n");
		HW_QSPI_Flash_ChipErase();
		printf("开始烧录。。。\r\n");
		if(flash_burn("0:/burn")){
			Common_Setting.fresh_burn=false;
			Create_New_Setting();
			HAL_NVIC_SystemReset();
		}
		else
			while(true);
	}
	else
		printf("flash数据预读取成功\r\n");
	HW_QSPI_Flash_MemoryMappedMode();
	return res;
}

void flash_get_data(uint32_t addr,void* buf,uint32_t len){
	memcpy(buf,(const void*)(W25Qxx_Mem_Addr+addr),len);
}





