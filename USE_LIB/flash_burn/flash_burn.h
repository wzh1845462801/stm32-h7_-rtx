#ifndef FLASH_BURN_H_
#define FLASH_BURN_H_
//本文件封装了向SPI FLASH烧录资源的相关函数
//使用flash_burn烧录一个文件到SPI_FLASH的指定地址
//使用GetResInfo获取烧录后的资源
#include "stm32h7xx_sys.h"

//资源烧录地址
#define BURN_RES_ADDR		(0)

//烧录目录下文件个数限制
#define MAX_FILE_TO_BURN_NUM	10

//烧录文件带路径名长度限制
#define FILE_FULL_NAME_LEN		512

//读取烧录文件的缓存大小
#define BURN_READ_BUF_SIZE		4096

//烧录文件文件名限制
#define RES_NAME_LEN			40
#ifdef __cplusplus
extern "C" {
#endif
bool flash_data_init(void);
/*
将指定目录下的所有文件烧录进spi_flash指定地址处，
无需事先擦除flash
*/
bool flash_burn(const char* dir_path);

void flash_get_data(uint32_t addr,void* buf,uint32_t len);
#ifdef __cplusplus
}
#endif

#endif

