#include "my_tree.h"

//初始化一颗空树
//pTree:树的指针
void My_Tree_Init(My_Tree* const pTree) {
	pTree->num = 0;
	pTree->root_node.data = NULL;
	pTree->root_node.del_call = NULL;
	pTree->root_node.pTree = pTree;
	pTree->root_node.p_FirstChild = NULL;
	pTree->root_node.p_RightBrother = NULL;
	pTree->root_node.p_Parent = NULL;
}

//后序遍历树，深度优先，遍历pNode的所有右兄弟节点和子节点
//pNode：节点指针
//num：遍历次数
//call：遍历调用的回调函数
static void _My_Tree_Node_Traverse_Back_Use(My_Tree_Node* const pNode, size_t* num, void(*call)(My_Tree_Node*)) {
	if (pNode == NULL)
		return;
	_My_Tree_Node_Traverse_Back_Use(pNode->p_RightBrother, num, call);
	_My_Tree_Node_Traverse_Back_Use(pNode->p_FirstChild, num, call);
	(*num)++;
	if (call != NULL)
		call(pNode);
}

//前序遍历树，深度优先，遍历pNode的所有右兄弟节点和子节点
//pNode：节点指针
//num：遍历次数
//call：遍历调用的回调函数
static void _My_Tree_Node_Traverse_Front_Use(My_Tree_Node* const pNode, size_t* num, void(*call)(My_Tree_Node*)) {
	if (pNode == NULL)
		return;
	(*num)++;
	if (call != NULL)
		call(pNode);
	_My_Tree_Node_Traverse_Front_Use(pNode->p_FirstChild, num, call);
	_My_Tree_Node_Traverse_Front_Use(pNode->p_RightBrother, num, call);
}

//后序遍历树，广度优先，遍历pNode的所有右兄弟节点和子节点
//pNode：节点指针
//num：遍历次数
//call：遍历调用的回调函数
static void _My_Tree_Node_Traverse_Back_L_Use(My_Tree_Node* const pNode, size_t* num, void(*call)(My_Tree_Node*)) {
	if (pNode == NULL)
		return;
	_My_Tree_Node_Traverse_Back_L_Use(pNode->p_FirstChild, num, call);
	_My_Tree_Node_Traverse_Back_L_Use(pNode->p_RightBrother, num, call);
	(*num)++;
	if (call != NULL)
		call(pNode);
}

//前序遍历树，广度优先，遍历pNode的所有右兄弟节点和子节点
//pNode：节点指针
//num：遍历次数
//call：遍历调用的回调函数
static void _My_Tree_Node_Traverse_Front_L_Use(My_Tree_Node* const pNode, size_t* num, void(*call)(My_Tree_Node*)) {
	if (pNode == NULL)
		return;
	(*num)++;
	if (call != NULL)
		call(pNode);
	_My_Tree_Node_Traverse_Front_L_Use(pNode->p_RightBrother, num, call);
	_My_Tree_Node_Traverse_Front_L_Use(pNode->p_FirstChild, num, call);
}

//用于移除节点调用的回调函数，仅适合后序遍历调用
//pNode：节点指针
static void _My_Tree_Node_Remove_Call(My_Tree_Node* pNode) {
	pNode->pTree = NULL;
	pNode->p_FirstChild = NULL;
	pNode->p_Parent = NULL;
	pNode->p_RightBrother = NULL;
	if (pNode->del_call != NULL)
		pNode->del_call(pNode);
}

//用于改变节点的树指针，仅适合前序遍历调用
//pNode：节点指针
static void _My_Tree_Node_Change_Tree(My_Tree_Node* pNode) {
	pNode->pTree = pNode->p_Parent->pTree;
}

//将子树与主树进行分离,注意此函数调用后，子树中所有的节点的树指针无效
//调用节点不能是根节点
//pNode：子树根节点
//返回值：true 分离成功；false 分离失败（节点信息错误的时候）
static bool _My_Tree_Node_Detach(My_Tree_Node* const pNode) {
	if (My_Tree_Is_RootNode(pNode))
		return false;
	if (pNode == pNode->p_Parent->p_FirstChild) {
		pNode->p_Parent->p_FirstChild = pNode->p_RightBrother;
	}
	else {
		My_Tree_Node* p_LeftBrother = My_Tree_Node_Get_LeftBrother(pNode);
		if (p_LeftBrother == NULL) {
			return false;
		}
		p_LeftBrother->p_RightBrother = pNode->p_RightBrother;
	}
	pNode->p_RightBrother = NULL;
	pNode->p_Parent = NULL;
	pNode->pTree = NULL;
	return true;
}

//分离并设置子树的树节点
//pTree：要设置的树节点
//pNode：子树的根节点
//返回值：true 设置成功；false 设置失败
static bool _My_Tree_Node_Set_Tree(My_Tree* const pTree, My_Tree_Node* const pNode) {
	My_Tree* const priv_tree = pNode->pTree;
	if (priv_tree != NULL) {
		size_t node_num = 0;
		if (_My_Tree_Node_Detach(pNode) == false)
			return false;
		pNode->pTree = pTree;
		if (pTree != priv_tree) {
			_My_Tree_Node_Traverse_Front_Use(pNode->p_FirstChild, &node_num, _My_Tree_Node_Change_Tree);
			priv_tree->num -= node_num + 1;
			pTree->num += node_num + 1;
		}
	}
	else {
		pNode->pTree = pTree;
		pTree->num++;
	}
	return true;
}

//将一个节点插入到另一个节点右面
//pNode：被插入的节点
//pInsert：插入的节点
//返回值：true 成功 ；false 失败
bool My_Tree_Node_Insert_NodeRight(My_Tree_Node* const pNode, My_Tree_Node* const pInsert) {

	if (My_Tree_Is_RootNode(pNode))
		return false;

	if (_My_Tree_Node_Set_Tree(pNode->pTree, pInsert) == false)
		return false;

	pInsert->p_Parent = pNode->p_Parent;
	pInsert->p_RightBrother = pNode->p_RightBrother;
	pNode->p_RightBrother = pInsert;
	return true;
}

//将一个节点插入到另一个节点左面
//pNode：被插入的节点
//pInsert：插入的节点
//返回值：true 成功 ；false 失败
bool My_Tree_Node_Insert_NodeLeft(My_Tree_Node* const pNode, My_Tree_Node* const pInsert) {
	
	if (My_Tree_Is_RootNode(pNode))
		return false;

	if (_My_Tree_Node_Set_Tree(pNode->pTree, pInsert) == false)
		return false;

	pInsert->p_Parent = pNode->p_Parent;
	pInsert->p_RightBrother = pNode;

	if (pNode == pNode->p_Parent->p_FirstChild) {	
		pInsert->p_Parent->p_FirstChild = pInsert;
	}
	else {
		My_Tree_Node* p_LeftBrother = My_Tree_Node_Get_LeftBrother(pNode);
		if (p_LeftBrother == NULL) {
			return false;
		}
		p_LeftBrother->p_RightBrother = pInsert;
	}
	return true;
}

//将一个节点插入一个另一个父节点的第一个子节点中
//pParent：父节点
//pInsert：待插入节点
//返回值：true 插入成功；false 插入失败
bool My_Tree_Node_Insert_FirstChild(My_Tree_Node* const pParent, My_Tree_Node* const pInsert) {

	if (_My_Tree_Node_Set_Tree(pParent->pTree, pInsert) == false)
		return false;

	pInsert->p_Parent = pParent;
	pInsert->p_RightBrother = pParent->p_FirstChild;
	pParent->p_FirstChild = pInsert;
	return true;
}

//将一个节点插入一个另一个父节点的最后一个子节点中
//pParent：父节点
//pInsert：待插入节点
//返回值：true 插入成功；false 插入失败
bool My_Tree_Node_Insert_LastChild(My_Tree_Node* const pParent, My_Tree_Node* const pInsert) {
	My_Tree_Node* p_LastChild = My_Tree_Node_Get_LastChild(pParent);

	if (_My_Tree_Node_Set_Tree(pParent->pTree, pInsert) == false)
		return false;

	pInsert->p_Parent = pParent;
	pInsert->p_RightBrother = NULL;

	if (p_LastChild == NULL) {
		pParent->p_FirstChild = pInsert;
	}
	else {
		p_LastChild->p_RightBrother = pInsert;
	}
	return true;
}



//移除节点本身及其所有的子节点
//pNode：移除的节点
//返回值：true 移除成功；false 移除失败
bool My_Tree_Node_Remove(My_Tree_Node* const pNode) {
	My_Tree* const pTree = pNode->pTree;
	size_t remove_num = 0;

	if (_My_Tree_Node_Detach(pNode) == false)
		return false;

	_My_Tree_Node_Traverse_Back_Use(pNode->p_FirstChild, &remove_num, _My_Tree_Node_Remove_Call);
	_My_Tree_Node_Remove_Call(pNode);
	pTree->num -= remove_num + 1;
	return true;
}

//初始化一个空节点
//pNode：节点指针
//data：自定义数据指针
//del_call：节点被移除时自动调用的回调函数
void My_Tree_Node_Init(My_Tree_Node* const pNode, void* data, void(*del_call)(My_Tree_Node*)) {
	pNode->data = data;
	pNode->del_call = del_call;
	pNode->pTree = NULL;
	pNode->p_FirstChild = NULL;
	pNode->p_Parent = NULL;
	pNode->p_RightBrother = NULL;
}

//移除树内除根节点外的全部节点
//pTree：树指针
void My_Tree_Remove_All(My_Tree* const pTree) {
	My_Tree_Node* p_RightBrother;
	My_Tree_Node* pNode = pTree->root_node.p_FirstChild;
	while (pNode != NULL) {
		p_RightBrother = pNode->p_RightBrother;
		My_Tree_Node_Remove(pNode);
		pNode = p_RightBrother;
	}
}

void My_Tree_Move(My_Tree* const pSrcTree, My_Tree* const pDestTree) {
	while (My_Tree_Node_Get_FirstChild(My_Tree_Get_RootNode(pSrcTree)) != NULL) {
		My_Tree_Node_Insert_LastChild(My_Tree_Get_RootNode(pDestTree), My_Tree_Node_Get_FirstChild(My_Tree_Get_RootNode(pSrcTree)));
	}
}

//后序遍历子树（不包含根节点）
//pNode：子树的根节点
//call：回调函数
//返回值：遍历次数
size_t My_Tree_Node_Traverse_Back(My_Tree_Node* const pNode, void(*call)(My_Tree_Node*)) {
	size_t node_num = 0;
	_My_Tree_Node_Traverse_Back_Use(pNode->p_FirstChild, &node_num, call);
	return node_num;
}

//前序遍历子树（不包含根节点）
//pNode：子树的根节点
//call：回调函数
//返回值：遍历次数
size_t My_Tree_Node_Traverse_Front(My_Tree_Node* const pNode, void(*call)(My_Tree_Node*)) {
	size_t node_num = 0;
	_My_Tree_Node_Traverse_Front_Use(pNode->p_FirstChild, &node_num, call);
	return node_num;
}

//后序遍历子树，广度优先（不包含根节点）
//pNode：子树的根节点
//call：回调函数
//返回值：遍历次数
size_t My_Tree_Node_Traverse_Back_L(My_Tree_Node* const pNode, void(*call)(My_Tree_Node*)) {
	size_t node_num = 0;
	_My_Tree_Node_Traverse_Back_L_Use(pNode->p_FirstChild, &node_num, call);
	return node_num;
}

//前序遍历子树，广度优先（不包含根节点）
//pNode：子树的根节点
//call：回调函数
//返回值：遍历次数
size_t My_Tree_Node_Traverse_Front_L(My_Tree_Node* const pNode, void(*call)(My_Tree_Node*)) {
	size_t node_num = 0;
	_My_Tree_Node_Traverse_Front_L_Use(pNode->p_FirstChild, &node_num, call);
	return node_num;
}


//后序遍历树（不包含根节点）
//pTree：子树的树节点
//call：回调函数
void My_Tree_Traverse_Back(My_Tree* const pTree, void(*call)(My_Tree_Node*)) {
	size_t node_num = 0;
	_My_Tree_Node_Traverse_Back_Use(pTree->root_node.p_FirstChild, &node_num, call);
}

//前序遍历树（不包含根节点）
//pTree：子树的树节点
//call：回调函数
void My_Tree_Traverse_Front(My_Tree* const pTree, void(*call)(My_Tree_Node*)) {
	size_t node_num = 0;
	_My_Tree_Node_Traverse_Front_Use(pTree->root_node.p_FirstChild, &node_num, call);
}

//后序遍历树（不包含根节点）
//pTree：子树的树节点
//call：回调函数
void My_Tree_Traverse_Back_L(My_Tree* const pTree, void(*call)(My_Tree_Node*)) {
	size_t node_num = 0;
	_My_Tree_Node_Traverse_Back_L_Use(pTree->root_node.p_FirstChild, &node_num, call);
}

//前序遍历树（不包含根节点）
//pTree：子树的树节点
//call：回调函数
void My_Tree_Traverse_Front_L(My_Tree* const pTree, void(*call)(My_Tree_Node*)) {
	size_t node_num = 0;
	_My_Tree_Node_Traverse_Front_L_Use(pTree->root_node.p_FirstChild, &node_num, call);
}

