#ifndef MUSIC_DEVICE_H_
#define MUSIC_DEVICE_H_

#include <stdint.h>
#include <stdbool.h>

#define MUSIC_GAIN_MAX			(36)
#define MUSIC_GAIN_MIN			(-80)

typedef void* Music_Decoder_Handle;
typedef void* Music_Handle;

typedef struct{
	const char* const *surport_file;						//支持的文件类型，后缀名
	Music_Decoder_Handle (*pCreate)(const char*,void*);		//根据文件名称创建解码器句柄
	void (*pDelete)(Music_Decoder_Handle);					//释放解码器句柄
	bool (*pFillBuf)(Music_Decoder_Handle,void*);			//填充pGetBufMax获取长度的缓冲区，播放完毕后返回false
	uint32_t (*pGetSamplate)(Music_Decoder_Handle);			//获取音频采样率
	uint32_t (*pGetBufMax)(Music_Decoder_Handle);			//获取单次解码器填充的缓冲区长度
	uint32_t (*pGetDataSize)(Music_Decoder_Handle);			//获取音频位数,如16位、24位...
	bool (*pSetPos)(Music_Decoder_Handle,uint32_t);			//设置播放进度
	uint32_t (*pGetNowPos)(Music_Decoder_Handle);			//获取当前播放进度
	uint32_t (*pGetTotalPos)(Music_Decoder_Handle);			//获取总播放进度
	uint32_t (*pGetTotalSec)(Music_Decoder_Handle);			//获取总播放时间
}Music_Decoder;

typedef struct{
	int priority;					//音频优先级
	void(*end_hook)(void*);			//结束时调用的钩子函数
	void* use_ptr;					//钩子函数参数
	const Music_Decoder* pDecoder;	//音频解码器
	void* token;					//用于传递给解码器的参数
}Music_InitType;

typedef struct{
	void* buf_addr;
	uint32_t buf_size;
	uint32_t data_size;
}Music_BufType;

typedef void (*Music_Decoder_Walker)(Music_Decoder* pDecoder,void* token);

#ifdef __cplusplus
 extern "C" {
#endif
Music_Handle Music_Create(const char* name,Music_InitType* pInit);
void Music_Delete(Music_Handle pHandle);
void Music_Start(Music_Handle pHandle);
void Music_Stop(Music_Handle pHandle);
bool Music_Is_Suspend(Music_Handle pHandle);
bool Music_Is_End(Music_Handle pHandle);
int Music_Get_Priority(Music_Handle pHandle);
bool Music_Set_Priority(Music_Handle use_handle,int priority);
Music_Handle Music_Get_Current(void);
void Music_Set_HP_Vol(uint8_t l_vol,uint8_t r_vol);
void Music_Set_SPK_Vol(uint8_t vol);
void Music_Set_Volume_Gain(int16_t gain);//设置增益，单位db
void Music_Set_EndHook(Music_Handle pHandle,void(*hook)(void*));
void Music_Set_UsePtr(Music_Handle pHandle,void* pdata);
uint32_t Music_Get_TotalPos(Music_Handle use_handle);
uint32_t Music_Get_NowPos(Music_Handle use_handle);
uint32_t Music_Get_TotalSec(Music_Handle use_handle);
bool Music_Set_Pos(Music_Handle use_handle,uint32_t pos);
bool Music_Get_Buf_Data(Music_Handle use_handle,Music_BufType* pBuf);
uint32_t Music_Get_Buf_Size(Music_Handle use_handle);
uint32_t Music_Get_Data_Size(Music_Handle use_handle);
uint32_t Music_Get_Samplate(Music_Handle use_handle);
uint32_t Music_Get_FillBuf_Size(Music_Handle use_handle);
bool Music_Decoder_Register(const Music_Decoder* pDecoder);
bool Music_Decoder_UnRegister(const Music_Decoder* pDecoder);
void Music_Decoder_Walk(Music_Decoder_Walker walk_func,void* token);
bool Music_Device_Init(void);
#ifdef __cplusplus
}
#endif

#endif
