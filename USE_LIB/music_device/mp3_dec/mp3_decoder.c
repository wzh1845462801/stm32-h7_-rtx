#include "music_device.h"
#include "spiritMP3Dec.h"
#include "ff.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MP3_FILL_BUF_SIZE		(2034*4)
#define MP3_FRAME_SIZE			(2034*2)

typedef struct{
	TSpiritMP3Decoder* pDecoder;
	TSpiritMP3Info mp3_Info;
	FIL file;
	uint32_t data_start;
	uint32_t total_sec;
}Mp3_Handle;


static inline unsigned long unsync(unsigned long b0,
                            unsigned long b1,
                            unsigned long b2,
                            unsigned long b3)
{
   return (((long)(b0 & 0x7F) << (3*7)) |
           ((long)(b1 & 0x7F) << (2*7)) |
           ((long)(b2 & 0x7F) << (1*7)) |
           ((long)(b3 & 0x7F) << (0*7)));
}
/*
 * Calculates the size of the ID3v2 tag.
 *
 * Arguments: file - the file to search for a tag.
 *
 * Returns: the size of the tag or 0 if none was found
 */
static inline int getid3v2len(FIL* fd){
    char buf[6];
    int offset;
	UINT br;

    /* Make sure file has a ID3 tag */
    if((FR_OK != f_lseek(fd, 0)) ||
       (f_read(fd, buf, 6,&br) != FR_OK) ||
		br!=6||
       (strncmp(buf, "ID3", strlen("ID3")) != 0))
        offset = 0;
    /* Now check what the ID3v2 size field says */
    else{
        if(f_read(fd, buf, 4,&br) != FR_OK||br!=4)
            offset = 0;
        else
            offset = unsync(buf[0], buf[1], buf[2], buf[3]) + 10;
    }
    f_lseek(fd, 0);
    return offset;
}

static unsigned int Mp3_ReadCallBack(void* data,unsigned int len,void* use_handle){
	Mp3_Handle* pHandle=use_handle;
	UINT br;
	if(f_read(&pHandle->file,data,len,&br)==FR_OK){
		return br;
	}
	else
		return 0;
}

static Music_Decoder_Handle Mp3_Create(const char* name,void* token){
	Mp3_Handle* pHandle;
	UINT br;
	pHandle=malloc(sizeof(Mp3_Handle));
	if(pHandle==NULL)
		goto fail0;
	pHandle->pDecoder=malloc(SpiritMP3DecoderGetPersistentSize());
	if(pHandle->pDecoder==NULL)
		goto fail1;
	if(f_open(&pHandle->file,name,FA_READ)!=FR_OK){
		printf("mp3文件打开失败\r\n");
		goto fail3;
	}
	
	pHandle->data_start=getid3v2len(&pHandle->file);
	if(f_lseek(&pHandle->file,pHandle->data_start)!=FR_OK)
		goto fail3;
	SpiritMP3DecoderInit(pHandle->pDecoder,Mp3_ReadCallBack,NULL,pHandle);
	do{
		SpiritMP3Decode(pHandle->pDecoder,NULL,0,&pHandle->mp3_Info);
		//此处还需后续测试
	}while(!pHandle->mp3_Info.IsGoodStream||pHandle->mp3_Info.nBitrateKbps==0||pHandle->mp3_Info.nSampleRateHz==0);
	pHandle->total_sec=(f_size(&pHandle->file)-pHandle->data_start)/(pHandle->mp3_Info.nBitrateKbps/8*1000);
	return pHandle;
fail3:
	f_close(&pHandle->file);
fail2:
	free(pHandle->pDecoder);
fail1:
	free(pHandle);
fail0:
	return NULL;
}

static void Mp3_Delete(Music_Decoder_Handle use_handle){
	Mp3_Handle* pHandle=use_handle;
	free(pHandle->pDecoder);
	f_close(&pHandle->file);
	free(pHandle);
}

static uint32_t Mp3_GetSamplerate(Music_Decoder_Handle use_handle){
	return ((Mp3_Handle*)use_handle)->mp3_Info.nSampleRateHz;
}

static uint32_t Mp3_GetDataSize(Music_Decoder_Handle use_handle){
	return 16;
}

static uint32_t Mp3_GetMaxBuf(Music_Decoder_Handle use_handle){
	return MP3_FILL_BUF_SIZE;
}

static bool Mp3_FillBuf(Music_Decoder_Handle use_handle,void* buf){
	Mp3_Handle* pHandle=use_handle;
	uint32_t fill_samples;
	fill_samples=SpiritMP3Decode(pHandle->pDecoder,buf,MP3_FILL_BUF_SIZE/4,NULL);
	fill_samples*=4;
	if(fill_samples<MP3_FILL_BUF_SIZE){
		memset((uint8_t*)buf+fill_samples,0,MP3_FILL_BUF_SIZE-fill_samples);
	}
	return fill_samples==MP3_FILL_BUF_SIZE;
}

static uint32_t Mp3_GetTotalPos(Music_Decoder_Handle use_handle){
	Mp3_Handle* pHandle=use_handle;
	return (f_size(&pHandle->file)-pHandle->data_start)/MP3_FRAME_SIZE;
}

static uint32_t Mp3_GetNowPos(Music_Decoder_Handle use_handle){
	Mp3_Handle* pHandle=use_handle;
	return (f_tell(&pHandle->file)-pHandle->data_start)/MP3_FRAME_SIZE;
}

static bool Mp3_SetPos(Music_Decoder_Handle use_handle,uint32_t pos){
	Mp3_Handle* pHandle=use_handle;
	if(pos>Mp3_GetTotalPos(use_handle)){
		pos=Mp3_GetTotalPos(use_handle);
	}
	pos*=MP3_FRAME_SIZE;
	pos+=pHandle->data_start;
	return f_lseek(&pHandle->file,pos)==FR_OK;
}

static uint32_t Mp3_GetTotalSec(Music_Decoder_Handle use_handle){
	Mp3_Handle* pHandle=use_handle;
	return pHandle->total_sec;
}

static const char*const file_surport[]={
	"mp3","mp2","mp1",NULL
};

const Music_Decoder Mp3_Decoder_Device={
	.pCreate=Mp3_Create,
	.pDelete=Mp3_Delete,
	.pFillBuf=Mp3_FillBuf,
	.pGetBufMax=Mp3_GetMaxBuf,
	.pGetDataSize=Mp3_GetDataSize,
	.pGetSamplate=Mp3_GetSamplerate,
	.pGetTotalPos=Mp3_GetTotalPos,
	.pGetNowPos=Mp3_GetNowPos,
	.pSetPos=Mp3_SetPos,
	.pGetTotalSec=Mp3_GetTotalSec,
	.surport_file=file_surport
};



