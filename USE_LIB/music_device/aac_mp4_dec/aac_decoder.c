#include "neaacdec.h"
#include "mp4ff/mp4ff.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "music_device.h"
#include "ff.h"


#define DebugOut(fmt,...)	printf(fmt,##__VA_ARGS__)
#define BUFFER_SIZE	(FAAD_MIN_STREAMSIZE*64)
#define FILE_UNKNOW     0
#define FILE_MP4        1
#define FILE_AAC        2



#define DEOCDE_BUF_SIZE	(1024*8)
#define DEFAULT_SAMPLATE	44100


typedef struct {
	uint8_t file_type;
	FIL file;
	NeAACDecHandle 			decoder;
	uint8_t*				decode_buf;
	uint32_t				decode_len;
	unsigned long 			samplerate;
	uint8_t 				channels;
	NeAACDecFrameInfo 		frameInfo;
	union{
		struct{
			mp4ff_callback_t*		mp4cb;
			mp4ff_t*				infile;
			int 					mp4track;
			int 					numSamples;
			int						sampleID;
			mp4AudioSpecificConfig 	mp4ASC;
			uint32_t				total_sec;
		}Mp4;
		struct{
			uint8_t*	read_buffer;
			uint32_t	byte_left;
			uint32_t	byte_consumed;
			bool		is_vbr;
			uint32_t	kbitrate;
			uint32_t	frequency;
			uint32_t	first_frame;
			uint32_t	data_start;
			uint8_t		aac_type;
		}AAC;
	};
}AAC_Handle;

/*
 * find AAC track
*/

static int getAACTrack(mp4ff_t *infile){
	int i, rc;
	int numTracks = mp4ff_total_tracks(infile);

//	printf("total-tracks: %d\n", numTracks);
	for(i=0; i<numTracks; i++){
		unsigned char*	buff = 0;
		unsigned int	buff_size = 0;
		mp4AudioSpecificConfig mp4ASC;
//		printf("testing-track: %d\n", i);
		mp4ff_get_decoder_config(infile, i, &buff, &buff_size);
		if(buff){
			rc = NeAACDecAudioSpecificConfig(buff, buff_size, &mp4ASC);
			free(buff);
			if(rc < 0)
				continue;
			return(i);
		}
	}
	return(-1);
}

static uint8_t mp4_get_file_type(FIL *mp4file)
{
	unsigned char header[10] = {0};
	UINT br;
	f_lseek(mp4file, 0);
	f_read(mp4file,header, 8, &br);
	if(header[4]=='f' &&
		header[5]=='t' &&
		header[6]=='y' &&
		header[7]=='p'){
		return FILE_MP4;
	}
	else{
		return FILE_AAC;
	}
}

static uint32_t read_callback(void *user_data, void *buffer, uint32_t length)
{
	UINT br;
	if(f_read((FIL*)user_data,buffer,length,&br)==FR_OK){
		return br;
	}
	else
		return 0;
}

static uint32_t seek_callback(void *user_data, uint64_t position)
{
  return f_lseek((FIL*)user_data, position)!=FR_OK;
}


static mp4ff_callback_t *getMP4FF_cb(FIL *mp4file)
{
	mp4ff_callback_t* mp4cb = malloc(sizeof(mp4ff_callback_t));
	if(mp4cb==NULL){
		return NULL;
	}
	mp4cb->read = read_callback;
	mp4cb->seek = seek_callback;
	mp4cb->truncate = NULL;
	mp4cb->write = NULL;
	mp4cb->user_data = mp4file;
	return mp4cb;
}

static inline unsigned long unsync(unsigned long b0,
                            unsigned long b1,
                            unsigned long b2,
                            unsigned long b3)
{
   return (((long)(b0 & 0x7F) << (3*7)) |
           ((long)(b1 & 0x7F) << (2*7)) |
           ((long)(b2 & 0x7F) << (1*7)) |
           ((long)(b3 & 0x7F) << (0*7)));
}
/*
 * Calculates the size of the ID3v2 tag.
 *
 * Arguments: file - the file to search for a tag.
 *
 * Returns: the size of the tag or 0 if none was found
 */
static inline int getid3v2len(FIL* fd)
{
    char buf[6];
    int offset;
	UINT br;

    /* Make sure file has a ID3 tag */
    if((FR_OK != f_lseek(fd, 0)) ||
       (f_read(fd, buf, 6,&br) != FR_OK) ||
		br!=6||
       (strncmp(buf, "ID3", strlen("ID3")) != 0))
        offset = 0;
    /* Now check what the ID3v2 size field says */
    else
    {
        if(f_read(fd, buf, 4,&br) != FR_OK||br!=4)
            offset = 0;
        else
            offset = unsync(buf[0], buf[1], buf[2], buf[3]) + 10;
    }
    f_lseek(fd, 0);
    return offset;
}

static const int sample_rates[] ={
    96000, 88200, 64000, 48000,
    44100, 32000, 24000, 22050,
    16000, 12000, 11025, 8000,
    7350, 0, 0, 0
};

static inline uint32_t read_uint16be(FIL* fd, uint16_t* buf){
    unsigned char tmp[2];
	UINT br;
	f_read(fd,tmp,2,&br);
    *buf = (tmp[0] << 8) | tmp[1];
    return br;
}

static inline int read_uint32be(FIL* fd, uint32_t* buf){
    unsigned char tmp[4];
    UINT br;
	f_read(fd,tmp,4,&br);
    *buf = (tmp[0] << 24) | (tmp[1] << 16) | (tmp[2] << 8) | tmp[3];
    return br;
}

//检查同步字
static inline bool check_adts_syncword(FIL* fd){
    uint16_t syncword;
    read_uint16be(fd, &syncword);
    return (syncword & 0xFFF6) == 0xFFF0;
}

//寻找内存中同步字的偏移
static inline int find_adts_sync_word(uint8_t* buf,int buf_len){
	for(size_t i=0;i<buf_len-1;i++){
		if(buf[i+0]==0xFF && (buf[i+1]&0xF6)==0xF0){
			return i;
		}			
	}
	return -1;
}	

static bool Mp4_Handle_Create(AAC_Handle* pHandle){
	uint8_t* buffer		=NULL;
	uint32_t bufferSize	=0;
	
	pHandle->Mp4.mp4cb=getMP4FF_cb(&pHandle->file);
	if(pHandle->Mp4.mp4cb==NULL){
		goto fail0;
	}
	
	pHandle->Mp4.infile=mp4ff_open_read(pHandle->Mp4.mp4cb);
	if(pHandle->Mp4.infile==NULL){
		goto fail0;
	}
	
	pHandle->Mp4.mp4track=getAACTrack(pHandle->Mp4.infile);
	if(pHandle->Mp4.mp4track<0){
		goto fail1;
	}
	
	mp4ff_get_decoder_config(pHandle->Mp4.infile,pHandle->Mp4.mp4track,&buffer,&bufferSize);
	if(NeAACDecInit2(pHandle->decoder,\
					buffer,\
					bufferSize,\
					&pHandle->samplerate,\
					&pHandle->channels)<0)
	{
		goto fail1;
	}
		
	if(pHandle->channels==0){
		goto fail2;
	}
					
	pHandle->Mp4.sampleID=0;
	pHandle->Mp4.numSamples=mp4ff_num_samples(pHandle->Mp4.infile,pHandle->Mp4.mp4track);
	{
		float f = 1024.0;
		if(pHandle->Mp4.mp4ASC.sbr_present_flag == 1)
			f = f * 2.0;
		pHandle->Mp4.total_sec = ((float)pHandle->Mp4.numSamples*(float)(f-1.0)/(float)pHandle->samplerate);
	}
	free(buffer);
	return true;
fail2:	
	free(buffer);
fail1:
	free(pHandle->Mp4.infile);
fail0:	
	free(pHandle->Mp4.mp4cb);
	return false;
}

static bool AAC_Fresh_Buf(AAC_Handle* pHandle){
	if(pHandle->AAC.byte_consumed>0){
		UINT br;
		memmove(pHandle->AAC.read_buffer,\
				&pHandle->AAC.read_buffer[pHandle->AAC.byte_consumed],\
				pHandle->AAC.byte_left-pHandle->AAC.byte_consumed);
		pHandle->AAC.byte_left-=pHandle->AAC.byte_consumed;
		if(f_read(	&pHandle->file,\
					&pHandle->AAC.read_buffer[pHandle->AAC.byte_left],\
					BUFFER_SIZE-pHandle->AAC.byte_left,\
					&br)!=FR_OK){
			return false;
		}
		pHandle->AAC.byte_left+=br;
		if(pHandle->AAC.byte_left==0)
			return false;
		pHandle->AAC.byte_consumed=0;
	}
	return true;
}

static bool AAC_Handle_Init(AAC_Handle* pHandle){
	unsigned char buf[5];
	pHandle->AAC.first_frame=getid3v2len(&pHandle->file);
	f_lseek(&pHandle->file,pHandle->AAC.first_frame);
	if (check_adts_syncword(&pHandle->file)){
        int frames;
        int stat_length;
        uint64_t total;
		uint32_t br;
		pHandle->AAC.aac_type=ADTS;
        if (f_read(&pHandle->file, buf, 5,&br) !=FR_OK||br!=5)
            return false;
        pHandle->AAC.frequency = sample_rates[(buf[0] >> 2) & 0x0F];
        pHandle->AAC.is_vbr = ((buf[3] & 0x1F) == 0x1F)&&((buf[4] & 0xFC) == 0xFC);
        stat_length = pHandle->AAC.frequency >> ((pHandle->AAC.is_vbr) ? 5 : 7);
        for (frames = 1, total = 0; frames < stat_length; frames++)
        {
            unsigned int frame_length = (((unsigned int)buf[1] & 0x3) << 11)
              | ((unsigned int)buf[2] << 3)
              | ((unsigned int)buf[3] >> 5);
            total += frame_length;
            if (frame_length < 7)
                break;
            if (FR_OK != f_lseek(&pHandle->file, frame_length - 7 + f_tell(&pHandle->file)))
                break;
            if (!check_adts_syncword(&pHandle->file))
                break;
            if (f_read(&pHandle->file, buf, 5,&br) !=FR_OK||br!=5)
                break;
        }
        pHandle->AAC.kbitrate = (unsigned int)((total * pHandle->AAC.frequency / frames + 64000) / 128000);
        if (pHandle->AAC.frequency <= 24000)
        {
            pHandle->AAC.frequency <<= 1;
        }
    }
    else{
        uint32_t bitrate;
		uint32_t br;
		if(f_lseek(&pHandle->file,pHandle->AAC.first_frame)!=FR_OK){
			return false;
		}
        if (f_read(&pHandle->file, buf, 5,&br) !=FR_OK||br!=5)
            return false;
        if (memcmp(buf, "ADIF", 4))
            return false;
		pHandle->AAC.aac_type=ADIF;
        if (FR_OK != f_lseek(&pHandle->file, (buf[4] & 0x80) ? (pHandle->AAC.first_frame + 9) : pHandle->AAC.first_frame))
            return false;
        read_uint32be(&pHandle->file, &bitrate);
        pHandle->AAC.is_vbr = (bitrate & 0x10000000) != 0;
        pHandle->AAC.kbitrate = ((bitrate & 0xFFFFFE0) + 16000) / 32000;
        read_uint32be(&pHandle->file, (uint32_t*)(&(pHandle->AAC.frequency)));
        pHandle->AAC.frequency = sample_rates[(pHandle->AAC.frequency >> (pHandle->AAC.is_vbr ? 23 : 3)) & 0x0F];
    }
	f_lseek(&pHandle->file,pHandle->AAC.first_frame);
	return true;
}

static bool AAC_Handle_Create(AAC_Handle* pHandle){
	NeAACDecConfigurationPtr config;
	
	pHandle->AAC.read_buffer=malloc(BUFFER_SIZE);
	if(pHandle->AAC.read_buffer==NULL){
		goto fail0;	
	}
	config = NeAACDecGetCurrentConfiguration(pHandle->decoder);
    config->useOldADTSFormat = 0;
    NeAACDecSetConfiguration(pHandle->decoder, config);
	
	if(AAC_Handle_Init(pHandle)==false){
		goto fail1;
	}
	
	if(f_read(&pHandle->file,pHandle->AAC.read_buffer,BUFFER_SIZE,&pHandle->AAC.byte_left)!=FR_OK){
		goto fail1;
	}
	
	if(pHandle->AAC.byte_left==0){
		goto fail1;
	}
	
	pHandle->AAC.byte_consumed=NeAACDecInit(pHandle->decoder,\
											pHandle->AAC.read_buffer,\
											pHandle->AAC.byte_left,\
											&pHandle->samplerate,\
											&pHandle->channels);
	pHandle->AAC.data_start=pHandle->AAC.byte_consumed+pHandle->AAC.first_frame;
	AAC_Fresh_Buf(pHandle);
	return true;
fail1:
	free(pHandle->AAC.read_buffer);
fail0:	
	return false;
}

static Music_Decoder_Handle AAC_Decoder_Create(const char* file_name,void* token){
	AAC_Handle* pHandle;
	NeAACDecConfigurationPtr pConfig;
	bool res;
	
	pHandle=malloc(sizeof(AAC_Handle));
	if(pHandle==NULL){
		goto fail0;
	}
	memset(pHandle,0,sizeof(AAC_Handle));
	
	pHandle->decoder=NeAACDecOpen();
	if(pHandle->decoder==NULL){
		goto fail1;
	}
	pConfig=NeAACDecGetCurrentConfiguration(pHandle->decoder);
	pConfig->outputFormat=FAAD_FMT_16BIT;//设置音频信号输出格式为16bit
	pConfig->defSampleRate=DEFAULT_SAMPLATE;
	pConfig->downMatrix=1;//设置为双声道输出
	NeAACDecSetConfiguration(pHandle->decoder,pConfig);
	
	if(f_open(&pHandle->file,file_name,FA_READ)!=FR_OK){
		goto fail2;
	}
	
	pHandle->file_type=mp4_get_file_type(&pHandle->file);
	f_lseek(&pHandle->file, 0);
	
	if(pHandle->file_type==FILE_MP4){
		res=Mp4_Handle_Create(pHandle);
	}
	else{
		res=AAC_Handle_Create(pHandle);
	}
	if(res==false){
		goto fail3;
	}
	return pHandle;
fail3:	
	f_close(&pHandle->file);
fail2:	
	NeAACDecClose(pHandle->decoder);
fail1:
	free(pHandle);
fail0:
	return NULL;
}

static bool AAC_Fill_Buf_Once(AAC_Handle* pHandle,void* buf,uint32_t* real_len,uint32_t target_len){
	void* sampleBuffer;
	if(AAC_Fresh_Buf(pHandle)==false)
		return false;
	sampleBuffer=NeAACDecDecode(pHandle->decoder,\
								&pHandle->frameInfo,\
								pHandle->AAC.read_buffer,\
								pHandle->AAC.byte_left);
	if(pHandle->frameInfo.error){
		DebugOut("FAAD: decode error '%s'\n", NeAACDecGetErrorMessage(pHandle->frameInfo.error));
		return false;
	}
	pHandle->AAC.byte_consumed+=pHandle->frameInfo.bytesconsumed;
	if(pHandle->frameInfo.samples<=0 && sampleBuffer==NULL){
		*real_len=0;
		return true;
	}
	if((pHandle->frameInfo.samples<<1)>target_len){
		memcpy(buf,sampleBuffer,target_len);
		pHandle->decode_buf=(uint8_t*)sampleBuffer +target_len;
		pHandle->decode_len=(pHandle->frameInfo.samples<<1)-target_len;
		*real_len=target_len;
	}
	else{
		memcpy(buf,sampleBuffer,pHandle->frameInfo.samples<<1);
		pHandle->decode_buf=NULL;
		pHandle->decode_len=0;
		*real_len=pHandle->frameInfo.samples<<1;
	}
	return true;
}

static bool AAC_Fill(Music_Decoder_Handle use_handle,void* buf){
	uint8_t* target_buf=buf;
	uint32_t target_len=DEOCDE_BUF_SIZE;
	AAC_Handle* pHandle=use_handle;

	while(target_len!=0){
		uint32_t real_len;
		if(pHandle->decode_len!=0){
			if(pHandle->decode_len>=target_len){
				memcpy(target_buf,pHandle->decode_buf,target_len);
				pHandle->decode_buf+=target_len;
				pHandle->decode_len-=target_len;
				break;
			}
			else{
				memcpy(target_buf,pHandle->decode_buf,pHandle->decode_len);
				target_buf+=pHandle->decode_len;
				target_len-=pHandle->decode_len;
				pHandle->decode_buf=NULL;
				pHandle->decode_len=0;
			}
		}
		if(AAC_Fill_Buf_Once(pHandle,target_buf,&real_len,target_len)==false)
			return false;
		target_buf+=real_len;
		target_len-=real_len;
	}
	return true;
}

static bool Mp4_Fill_Buf_Once(AAC_Handle* pHandle,void* buf,uint32_t* real_len,uint32_t target_len){
	int rc;
	void* sampleBuffer;
	uint8_t* buffer=NULL;
	uint32_t bufferSize = 0;
	rc=mp4ff_read_sample(pHandle->Mp4.infile,\
						pHandle->Mp4.mp4track,\
						pHandle->Mp4.sampleID++,\
						&buffer,\
						&bufferSize);
	if(rc==0||buffer==NULL){
		if(buffer)
			free(buffer);
		return false;
	}
	sampleBuffer=NeAACDecDecode(pHandle->decoder,\
								&pHandle->frameInfo,\
								buffer,\
								bufferSize);
	if(buffer)
		free(buffer);
	if(pHandle->frameInfo.error>0){
		DebugOut("FAAD: decode error '%s'\n", NeAACDecGetErrorMessage(pHandle->frameInfo.error));
		return false;
	}
	if((pHandle->frameInfo.samples<<1)>target_len){
		memcpy(buf,sampleBuffer,target_len);
		pHandle->decode_buf=(uint8_t*)sampleBuffer +target_len;
		pHandle->decode_len=(pHandle->frameInfo.samples<<1)-target_len;
		*real_len=target_len;
	}
	else{
		memcpy(buf,sampleBuffer,pHandle->frameInfo.samples<<1);
		pHandle->decode_buf=NULL;
		pHandle->decode_len=0;
		*real_len=pHandle->frameInfo.samples<<1;
	}
	return true;
}

static bool Mp4_Fill(Music_Decoder_Handle use_handle,void* buf){
	uint8_t* target_buf=buf;
	uint32_t target_len=DEOCDE_BUF_SIZE;
	AAC_Handle* pHandle=use_handle;

	if(pHandle->Mp4.sampleID>=pHandle->Mp4.numSamples)
		return false;

	while(target_len!=0){
		uint32_t real_len;
		if(pHandle->decode_len!=0){
			if(pHandle->decode_len>=target_len){
				memcpy(target_buf,pHandle->decode_buf,target_len);
				pHandle->decode_buf+=target_len;
				pHandle->decode_len-=target_len;
				break;
			}
			else{
				memcpy(target_buf,pHandle->decode_buf,pHandle->decode_len);
				target_buf+=pHandle->decode_len;
				target_len-=pHandle->decode_len;
				pHandle->decode_buf=NULL;
				pHandle->decode_len=0;
			}
		}
		if(Mp4_Fill_Buf_Once(pHandle,target_buf,&real_len,target_len)==false)
			return false;
		target_buf+=real_len;
		target_len-=real_len;
		if(pHandle->Mp4.sampleID>=pHandle->Mp4.numSamples){
			memset(target_buf,0,target_len);
			break;
		}
	}
	return true;
}

static bool AAC_Decoder_Fill(Music_Decoder_Handle use_handle,void* buf){
	AAC_Handle* pHandle=use_handle;
	if(pHandle->file_type==FILE_MP4)
		return Mp4_Fill(use_handle,buf);
	else if(pHandle->file_type==FILE_AAC)
		return AAC_Fill(use_handle,buf);
	return false;
}

static void AAC_Decoder_Delete(Music_Decoder_Handle use_handle){
	AAC_Handle* pHandle=use_handle;
	if(pHandle->file_type==FILE_MP4){
		if(pHandle->Mp4.infile)
			mp4ff_close(pHandle->Mp4.infile);
		if(pHandle->Mp4.mp4cb)
			free(pHandle->Mp4.mp4cb);
	}
	else if(pHandle->file_type==FILE_AAC){
		if(pHandle->AAC.read_buffer)
			free(pHandle->AAC.read_buffer);
	}
	f_close(&pHandle->file);
	NeAACDecClose(pHandle->decoder);
	free(pHandle);
}

static uint32_t AAC_Deocder_GetBufMax(Music_Decoder_Handle use_handle){
	return DEOCDE_BUF_SIZE;
}

static uint32_t AAC_Deocder_GetDataSize(Music_Decoder_Handle use_handle){
	return 16;
}

static uint32_t AAC_Deocder_Get_Now_Pos(Music_Decoder_Handle use_handle){
	AAC_Handle* pHandle=use_handle;
	if(pHandle->file_type==FILE_MP4){
		return pHandle->Mp4.sampleID;
	}
	else if(pHandle->file_type==FILE_AAC){
		return (f_tell(&pHandle->file)-pHandle->AAC.data_start-pHandle->AAC.byte_left)/FAAD_MIN_STREAMSIZE;
	}
	return 0;
}

static uint32_t AAC_Decoder_GetSamplate(Music_Decoder_Handle use_handle){
	AAC_Handle* pHandle=use_handle;
	return pHandle->samplerate;
}

static uint32_t AAC_Decoder_GetTotalPos(Music_Decoder_Handle use_handle){
	AAC_Handle* pHandle=use_handle;
	if(pHandle->file_type==FILE_MP4){
		return pHandle->Mp4.numSamples;
	}
	else if(pHandle->file_type==FILE_AAC){
		return (f_size(&pHandle->file)-pHandle->AAC.data_start)/FAAD_MIN_STREAMSIZE;
	}
	return 0;
}

static uint32_t AAC_Decoder_GetTotalSec(Music_Decoder_Handle use_handle){
	AAC_Handle* pHandle=use_handle;
	if(pHandle->file_type==FILE_MP4){
		return pHandle->Mp4.total_sec;
	}
	else if(pHandle->file_type==FILE_AAC){
		return (f_size(&pHandle->file)-pHandle->AAC.data_start)*8/(pHandle->AAC.kbitrate*1000);
	}
	return 0;
}

static bool AAC_Decoder_SetPos(Music_Decoder_Handle use_handle,uint32_t pos){
	AAC_Handle* pHandle=use_handle;
	uint32_t total_pos=AAC_Decoder_GetTotalPos(use_handle);
	if(pos>total_pos){
		pos=total_pos;
	}
	if(pHandle->file_type==FILE_MP4){
		pHandle->Mp4.sampleID=pos;
		pHandle->decode_buf=NULL;
		pHandle->decode_len=0;
		return true;
	}
	else if(pHandle->file_type==FILE_AAC){
		if(pHandle->AAC.aac_type==ADTS){
			int sync_local;
			pos*=FAAD_MIN_STREAMSIZE;
			pos+=pHandle->AAC.data_start;
			if(f_lseek(&pHandle->file,pos)!=FR_OK){
				goto fail;
			}
try_read:
			if(f_read(&pHandle->file,pHandle->AAC.read_buffer,BUFFER_SIZE,&pHandle->AAC.byte_left)!=FR_OK){
				goto fail;
			}
			if(pHandle->AAC.byte_left==0){
				goto fail;
			}
			sync_local=find_adts_sync_word(pHandle->AAC.read_buffer,pHandle->AAC.byte_left);
			if(sync_local<0){
				goto try_read;
			}
			pHandle->AAC.byte_consumed=sync_local;
			pHandle->decode_buf=NULL;
			pHandle->decode_len=0;
			NeAACDecPostSeekReset(pHandle->decoder,0);
			return true;
		}
		//ADIF格式调整播放进度不容易实现，暂不支持
	}
fail:
	return false;
}

static const char* const file_surpport[]={
	"aac",
	"mp4",
	"m4a",
	NULL
};

const Music_Decoder AAC_Decoder_Device={
	.pCreate=AAC_Decoder_Create,
	.pDelete=AAC_Decoder_Delete,
	.pFillBuf=AAC_Decoder_Fill,
	.pGetBufMax=AAC_Deocder_GetBufMax,
	.pGetDataSize=AAC_Deocder_GetDataSize,
	.pGetNowPos=AAC_Deocder_Get_Now_Pos,
	.pGetSamplate=AAC_Decoder_GetSamplate,
	.pGetTotalPos=AAC_Decoder_GetTotalPos,
	.pGetTotalSec=AAC_Decoder_GetTotalSec,
	.pSetPos=AAC_Decoder_SetPos,
	.surport_file=file_surpport
};



