#ifndef _FLAC_DECODER_H
#define _FLAC_DECODER_H
 
#include "music_device.h"

#ifdef __cplusplus
 extern "C" {
#endif

extern const Music_Decoder Flac_Decoder_Device;
#ifdef __cplusplus
}
#endif

#endif
