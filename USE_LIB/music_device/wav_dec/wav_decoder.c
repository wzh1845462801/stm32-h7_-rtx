#include "music_device.h"
#include "ff.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define WAV_SAI_TX_DMA_BUFSIZE    6144		//定义WAV TX DMA 数组大小(播放192Kbps@24bit的时候,需要设置4096大才不会卡)
 
#define WAV_FRAME_SIZE			  1536
#ifndef   __PACKED_STRUCT
  #define __PACKED_STRUCT                        struct __attribute__((packed, aligned(1)))
#endif
//RIFF块
typedef __PACKED_STRUCT
{
    uint32_t ChunkID;		   	//chunk id;这里固定为"RIFF",即0X46464952
    uint32_t ChunkSize ;		   	//集合大小;文件总大小-8
    uint32_t Format;	   			//格式;WAVE,即0X45564157
}ChunkRIFF ;
//fmt块
typedef __PACKED_STRUCT
{
    uint32_t ChunkID;		   	//chunk id;这里固定为"fmt ",即0X20746D66
    uint32_t ChunkSize ;		   	//子集合大小(不包括ID和Size);这里为:20.
    uint16_t AudioFormat;	  	//音频格式;0X01,表示线性PCM;0X11表示IMA ADPCM
	uint16_t NumOfChannels;		//通道数量;1,表示单声道;2,表示双声道;
	uint32_t SampleRate;			//采样率;0X1F40,表示8Khz
	uint32_t ByteRate;			//字节速率; 
	uint16_t BlockAlign;			//块对齐(字节); 
	uint16_t BitsPerSample;		//单个采样数据大小;4位ADPCM,设置为4
//	uint16_t ByteExtraData;		//附加的数据字节;2个; 线性PCM,没有这个参数
}ChunkFMT;	   
//fact块 
typedef __PACKED_STRUCT 
{
    uint32_t ChunkID;		   	//chunk id;这里固定为"fact",即0X74636166;
    uint32_t ChunkSize ;		   	//子集合大小(不包括ID和Size);这里为:4.
    uint32_t NumOfSamples;	  	//采样的数量; 
}ChunkFACT;
//LIST块 
typedef __PACKED_STRUCT 
{
    uint32_t ChunkID;		   	//chunk id;这里固定为"LIST",即0X74636166;
    uint32_t ChunkSize ;		   	//子集合大小(不包括ID和Size);这里为:4. 
}ChunkLIST;

//data块 
typedef __PACKED_STRUCT
{
    uint32_t ChunkID;		   	//chunk id;这里固定为"data",即0X5453494C
    uint32_t ChunkSize ;		   	//子集合大小(不包括ID和Size) 
}ChunkDATA;

//wav头
typedef __PACKED_STRUCT
{ 
	ChunkRIFF riff;	//riff块
	ChunkFMT fmt;  	//fmt块
//	ChunkFACT fact;	//fact块 线性PCM,没有这个结构体	 
	ChunkDATA data;	//data块		 
}__WaveHeader; 

//wav 播放控制结构体
typedef struct
{ 
    uint16_t audioformat;			//音频格式;0X01,表示线性PCM;0X11表示IMA ADPCM
	uint16_t nchannels;				//通道数量;1,表示单声道;2,表示双声道; 
	uint16_t blockalign;			//块对齐(字节);  
	uint32_t datasize;				//WAV数据大小 

    uint32_t totsec ;				//整首歌时长,单位:秒
    uint32_t cursec ;				//当前播放时长
	
    uint32_t bitrate;	   			//比特率(位速)
	uint32_t samplerate;				//采样率 
	uint16_t bps;					//位数,比如16bit,24bit,32bit
	
	uint32_t datastart;				//数据帧开始的位置(在文件里面的偏移)
}__wavctrl; 

typedef struct{
	FIL file;
	__wavctrl ctrl;
}Wav_Handle;

//WAV解析初始化
//fname:文件路径+文件名
//wavx:wav 信息存放结构体指针
//返回值:0,成功;1,打开文件失败;2,非WAV文件;3,DATA区域未找到.
static bool wav_decode_init(const char* fname,__wavctrl* wavx)
{
	FIL*		ftemp=(FIL*)malloc(sizeof(FIL));
	uint8_t*	buf=malloc(512); 
	uint32_t 	br=0;
	FRESULT 	res=FR_OK;
	
	ChunkRIFF *riff;
	ChunkFMT *fmt;
	ChunkFACT *fact;
	ChunkDATA *data;
	
	
	if(ftemp==NULL||buf==NULL){
		printf("wav内存不足！\r\n");
		goto fail0;
	}
	
	res=f_open(ftemp,fname,FA_READ);
	
	if(res!=FR_OK){
		printf("wav文件打开失败！\r\n");
		goto fail0;
	}
	
	res=f_read(ftemp,buf,512,&br);
	if(res!=FR_OK){
		printf("wav文件读取失败！\r\n");
		goto fail1;
	}
	
	riff=(ChunkRIFF *)buf;		//获取RIFF块
	
	if(riff->Format!=0X45564157){
		printf("非wav文件，无法解析！\r\n");
		goto fail1;
	}
	fmt=(ChunkFMT *)(buf+12);	//获取FMT块 
	fact=(ChunkFACT *)(buf+12+8+fmt->ChunkSize);//读取FACT块
	if(fact->ChunkID==0X74636166||fact->ChunkID==0X5453494C)
		wavx->datastart=12+8+fmt->ChunkSize+8+fact->ChunkSize;//具有fact/LIST块的时候(未测试)
	else 
		wavx->datastart=12+8+fmt->ChunkSize;  
	data=(ChunkDATA *)(buf+wavx->datastart);	//读取DATA块
	
	if(data->ChunkID!=0X61746164){
		printf("wav DATA区域寻找不到！\r\n");
		goto fail1;
	}
	wavx->audioformat=fmt->AudioFormat;		//音频格式
	wavx->nchannels=fmt->NumOfChannels;		//通道数
	wavx->samplerate=fmt->SampleRate;		//采样率
	wavx->bitrate=fmt->ByteRate*8;			//得到位速
	wavx->blockalign=fmt->BlockAlign;		//块对齐
	wavx->bps=fmt->BitsPerSample;			//位数,16/24/32位
	
	wavx->datasize=data->ChunkSize;			//数据块大小
	wavx->datastart=wavx->datastart+8;		//数据流开始的地方. 
	 
//	printf("wavx->audioformat:%d\r\n",wavx->audioformat);
//	printf("wavx->nchannels:%d\r\n",wavx->nchannels);
//	printf("wavx->samplerate:%d\r\n",wavx->samplerate);
//	printf("wavx->bitrate:%d\r\n",wavx->bitrate);
//	printf("wavx->blockalign:%d\r\n",wavx->blockalign);
//	printf("wavx->bps:%d\r\n",wavx->bps);
//	printf("wavx->datasize:%d\r\n",wavx->datasize);
//	printf("wavx->datastart:%d\r\n",wavx->datastart);  
	
	f_close(ftemp);
	free(ftemp);//释放内存
	free(buf); 
	return true;
fail1:
	f_close(ftemp);
fail0:
	free(ftemp);//释放内存
	free(buf); 
	memset(wavx,0,sizeof(__wavctrl));
	return false;
}

static Music_Decoder_Handle Wav_Create(const char* name,void* token){
	Wav_Handle* pHandle;
	pHandle=malloc(sizeof(Wav_Handle));
	if(pHandle==NULL)
		goto fail0;
	
	if(wav_decode_init(name,&pHandle->ctrl)==false)
		goto fail1;
	
	if(f_open(&pHandle->file,name,FA_READ)!=FR_OK)
		goto fail1;
	
	if(f_lseek(&pHandle->file,pHandle->ctrl.datastart)!=FR_OK)
		goto fail2;
	return pHandle;
fail2:
	f_close(&pHandle->file);
fail1:
	free(pHandle);
fail0:
	return NULL;
}

static void Wav_Delete(Music_Decoder_Handle use_handle){
	Wav_Handle* pHandle=use_handle;
	f_close(&pHandle->file);
	free(pHandle);
}

static bool Wav_Buf_Fill(Music_Decoder_Handle use_handle,void* buf){
	Wav_Handle* pHandle=use_handle;
	UINT br;
	if(pHandle->ctrl.bps==16||pHandle->ctrl.bps==32){
		if(f_read(&pHandle->file,buf,WAV_SAI_TX_DMA_BUFSIZE,&br)!=FR_OK)
			return false;
	}
	else if(pHandle->ctrl.bps==24){
		uint32_t* pbuf=buf;
		uint8_t* temp_buf=buf;
		if(f_read(&pHandle->file,buf,WAV_SAI_TX_DMA_BUFSIZE/4*3,&br)!=FR_OK)
			return false;
		br/=3;
		for(int32_t i=br-1;i>=0;i--){
			pbuf[i]=temp_buf[3*i]|(temp_buf[3*i+1]<<8)|(temp_buf[3*i+2]<<16);
		}
		br*=4;
	}
	else
		return false;
	if(br<WAV_SAI_TX_DMA_BUFSIZE)
		memset((uint8_t*)buf+br,0,WAV_SAI_TX_DMA_BUFSIZE-br);
	return br==WAV_SAI_TX_DMA_BUFSIZE;
}

static uint32_t Wav_GetSamplerate(Music_Decoder_Handle use_handle){
	return ((Wav_Handle*)use_handle)->ctrl.samplerate;
}

static uint32_t Wav_GetMaxBuf(Music_Decoder_Handle use_handle){
	return WAV_SAI_TX_DMA_BUFSIZE;
}

static uint32_t Wav_GetDataSize(Music_Decoder_Handle use_handle){
	return ((Wav_Handle*)use_handle)->ctrl.bps;
}

static uint32_t Wav_GetTotalPos(Music_Decoder_Handle use_handle){
	Wav_Handle* pHandle=use_handle;
	return (f_size(&pHandle->file)-pHandle->ctrl.datastart)/WAV_FRAME_SIZE;
}

static uint32_t Wav_GetNowPos(Music_Decoder_Handle use_handle){
	Wav_Handle* pHandle=use_handle;
	return (f_tell(&pHandle->file)-pHandle->ctrl.datastart)/WAV_FRAME_SIZE;
}

static bool Wav_SetPos(Music_Decoder_Handle use_handle,uint32_t set_pos){
	Wav_Handle* pHandle=use_handle;
	if(set_pos>Wav_GetTotalPos(use_handle)){
		set_pos=Wav_GetTotalPos(use_handle);
	}
	set_pos*=WAV_FRAME_SIZE;
	set_pos+=pHandle->ctrl.datastart;
	return f_lseek(&pHandle->file,set_pos)==FR_OK;
}

static uint32_t Wav_GetTotalSec(Music_Decoder_Handle use_handle){
	Wav_Handle* pHandle=use_handle;
	return (f_size(&pHandle->file)-pHandle->ctrl.datastart)/(pHandle->ctrl.bitrate/8);
}

static const char*const file_surport[]={
	"wav",NULL
};

const Music_Decoder Wav_Decoder_Device={
	.pCreate=Wav_Create,
	.pDelete=Wav_Delete,
	.pFillBuf=Wav_Buf_Fill,
	.pGetBufMax=Wav_GetMaxBuf,
	.pGetDataSize=Wav_GetDataSize,
	.pGetSamplate=Wav_GetSamplerate,
	.pSetPos=Wav_SetPos,
	.pGetNowPos=Wav_GetNowPos,
	.pGetTotalPos=Wav_GetTotalPos,
	.pGetTotalSec=Wav_GetTotalSec,
	.surport_file=file_surport
};


