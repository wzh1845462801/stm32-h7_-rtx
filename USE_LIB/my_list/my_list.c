#include "my_list.h"

void My_List_Init(My_List_Root* const proot){
	proot->item_num=0;
	proot->Item_End.data=NULL;
	proot->Item_End.pnext=&(proot->Item_End);
	proot->Item_End.ppriv=&(proot->Item_End);
	proot->Item_End.proot=proot;
}

int My_List_Item_Get_Index(const My_List_Item* const pItem){
	int index=0;
	TRAVERSE_MY_LIST(item,My_List_Get_Root(pItem)){
		if(item==pItem){
			return index;
		}
		index++;
	}
	return -1;
}

const My_List_Item* My_List_Index_Get_Item(const My_List_Root* const pRoot,int index){
	unsigned int loop_num=0;
	if(index<0||My_List_Get_Item_Num(pRoot)==0)
		return NULL;
	index%=My_List_Get_Item_Num(pRoot);
	TRAVERSE_MY_LIST(pItem,pRoot){
		if(loop_num>=index)
			return pItem;
		loop_num++;
	}
	return NULL;
}

void My_List_Insert_Item_Back(My_List_Item* const pInsert,My_List_Item* const pItem){
	My_List_Item* const pItem_priv=pInsert;
	My_List_Item* const pItem_next=pInsert->pnext;
	
	pItem->pnext=pItem_next;
	pItem->ppriv=pItem_priv;
	pItem->proot=pInsert->proot;
	
	pItem_priv->pnext=pItem;
	pItem_next->ppriv=pItem;
	
	pInsert->proot->item_num++;
}

void My_List_Insert_End(My_List_Root* const pRoot,My_List_Item* const pItem){
	My_List_Insert_Item_Back(pRoot->Item_End.ppriv,pItem);
}

void My_List_Insert_Start(My_List_Root* const pRoot,My_List_Item* const pItem){
	My_List_Insert_Item_Back(&pRoot->Item_End,pItem);
}

void My_List_Insert_Item_Front(My_List_Item* const pInsert,My_List_Item* const pItem){
	My_List_Insert_Item_Back(pInsert->ppriv,pItem);
}

bool My_List_Remove_Item(My_List_Item* const pItem){
	My_List_Item* const pItem_priv=pItem->ppriv;
	My_List_Item* const pItem_next=pItem->pnext;
	
	if(Is_My_List_RNode(pItem)){
		return false;
	}
	pItem_priv->pnext=pItem_next;
	pItem_next->ppriv=pItem_priv;
	
	pItem->proot->item_num--;
	pItem->proot=NULL;
	pItem->pnext=NULL;
	pItem->ppriv=NULL;
	
	return true;
}

void My_List_Change_Root(My_List_Root* const srcRoot,My_List_Root* const destRoot){
	My_List_Item* const pItem_Start=My_List_Get_Start(srcRoot);
	My_List_Item* const pItem_End=My_List_Get_End(srcRoot);
	
	for(My_List_Item* pItem=pItem_Start;pItem!=&srcRoot->Item_End;pItem=pItem->pnext){
		pItem->proot=destRoot;
	}
	pItem_Start->ppriv=&destRoot->Item_End;
	pItem_End->pnext=&destRoot->Item_End;
	destRoot->Item_End.pnext=pItem_Start;
	destRoot->Item_End.ppriv=pItem_End;
	destRoot->Item_End.data=NULL;
	destRoot->Item_End.proot=destRoot;
	destRoot->item_num=srcRoot->item_num;
	
	My_List_Init(srcRoot);
}

void My_List_Insert_Root_Back(My_List_Item* const pItem,My_List_Root* const pRoot){
	My_List_Item* const pItem_priv=pItem;
	My_List_Item* const pItem_next=pItem->pnext;
	My_List_Item* const pRoot_Start=My_List_Get_Start(pRoot);
	My_List_Item* const pRoot_End  =My_List_Get_End(pRoot);
	const My_List_Item* const pRoot_Node =My_List_Get_Root_Node(pRoot); 
	
	for(My_List_Item* buf_item=pRoot_Start;buf_item!=pRoot_Node;buf_item=buf_item->pnext){
		buf_item->proot=pItem->proot;
	}
	
	pItem_priv->pnext=pRoot_Start;
	pItem_next->ppriv=pRoot_End;
	pRoot_Start->ppriv=pItem_priv;
	pRoot_End->pnext=pItem_next;
	
	pItem->proot->item_num +=pRoot->item_num;
	
	My_List_Init(pRoot);
}

void My_List_Insert_Root_Front(My_List_Item* const pItem,My_List_Root* const pRoot){
	My_List_Insert_Root_Back(pItem->ppriv,pRoot);
}

void My_List_Append_Back(My_List_Root* const pMainRoot,My_List_Root* const pSubRoot){
	My_List_Insert_Root_Back(My_List_Get_End(pMainRoot),pSubRoot);
}

void My_List_Append_Front(My_List_Root* const pMainRoot,My_List_Root* const pSubRoot){
	My_List_Insert_Root_Front(My_List_Get_Start(pMainRoot),pSubRoot);
}

bool My_List_Sort(My_List_Root* const pRoot,int(*pcmp)(const void* ,const void*)){
	void** pbuf=malloc(sizeof(void*)*pRoot->item_num);
	size_t i=0;
	if(pbuf==NULL)
		return false;
	TRAVERSE_MY_LIST(item,pRoot){
		pbuf[i]=item->data;
		i++;
	}
	qsort(pbuf,pRoot->item_num,sizeof(void*),pcmp);
	i=0;
	TRAVERSE_MY_LIST(item,pRoot){
		item->data=pbuf[i];
		i++;
	}
	free(pbuf);
	return true;
}

