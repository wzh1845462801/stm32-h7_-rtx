#ifndef MY_LIST_H__
#define MY_LIST_H__
//封装了链表的常用操作
#include <stdbool.h>
#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

struct My_List_Root;

typedef struct My_List_Item{
	struct My_List_Item* pnext;//链表节点后一项指针
	struct My_List_Item* ppriv;//链表节点前一项指针
	struct My_List_Root* proot;//所属根链表
	void*  data;			   //存储的数据指针
}My_List_Item;//链表节点


typedef struct My_List_Root{
	size_t item_num;//链表当前所拥有的链表节点个数
	My_List_Item Item_End;//此项data不放置有效数据，主要作为根节点
}My_List_Root;//链表主体

//判断链表节点是否为根节点
static __inline bool Is_My_List_RNode(const My_List_Item* const pItem){
	return pItem==&(pItem->proot->Item_End);
}

//获取链表节点个数
static __inline size_t My_List_Get_Item_Num(const My_List_Root* const pRoot){
	return pRoot->item_num;
}

//获取链表的起始节点
static __inline My_List_Item* My_List_Get_Start(const My_List_Root* const pRoot){
	return pRoot->Item_End.pnext;
}

//获取链表的终止节点
static __inline My_List_Item* My_List_Get_End(const My_List_Root* const pRoot){
	return pRoot->Item_End.ppriv;
}

//获取链表根节点
static __inline const My_List_Item* My_List_Get_Root_Node(const My_List_Root* const pRoot){
	return &(pRoot->Item_End);
}

//判断链表节点是否被移除
static __inline bool Is_My_List_Item_Removed(const My_List_Item* const pItem){
	return pItem->proot==NULL;
}

//获取链表主体
static __inline My_List_Root* My_List_Get_Root(const My_List_Item* const pItem){
	return pItem->proot;
}

//正向遍历链表
#define TRAVERSE_MY_LIST(item,root)  for(My_List_Item* item=My_List_Get_Start(root);!Is_My_List_RNode(item);item=item->pnext)
//反向遍历链表
#define TRAVERSE_R_MY_LIST(item,root) for(My_List_Item* item=My_List_Get_End(root);!Is_My_List_RNode(item);item=item->ppriv)

void My_List_Init(My_List_Root* const proot);//初始化链表
int My_List_Item_Get_Index(const My_List_Item* const pItem);//获取表项相对于链表起点的偏移
const My_List_Item* My_List_Index_Get_Item(const My_List_Root* const pRoot,int index);//根据偏移获取表项
void My_List_Insert_End(My_List_Root* const pRoot,My_List_Item* const pItem);//在链表尾部插入一个节点
void My_List_Insert_Start(My_List_Root* const pRoot,My_List_Item* const pItem);//在链表头部插入一个节点
void My_List_Insert_Item_Back(My_List_Item* const pInsert,My_List_Item* const pItem);//在一节点后面插入节点
void My_List_Insert_Item_Front(My_List_Item* const pInsert,My_List_Item* const pItem);//在一节点前面插入节点
bool My_List_Remove_Item(My_List_Item* const pItem);//将节点移除，若移除的节点为根节点返回false，否则返回true
void My_List_Change_Root(My_List_Root* const srcRoot,My_List_Root* const destRoot);//改变一个链表的根节点，将srcroot的节点改为destroot
void My_List_Insert_Root_Back(My_List_Item* const pItem,My_List_Root* const pRoot);//将链表插入到另一个链表节点后方
void My_List_Insert_Root_Front(My_List_Item* const pItem,My_List_Root* const pRoot);//将链表插入到另一个链表节点前方
void My_List_Append_Back(My_List_Root* const pMainRoot,My_List_Root* const pSubRoot);//将一个链表附加在另一个链表后方
void My_List_Append_Front(My_List_Root* const pMainRoot,My_List_Root* const pSubRoot);//将一个链表附加在另一个链表前方
bool My_List_Sort(My_List_Root* const pRoot,int(*pcmp)(const void* ,const void*));//链表排序
#ifdef __cplusplus
}
#endif

#endif
