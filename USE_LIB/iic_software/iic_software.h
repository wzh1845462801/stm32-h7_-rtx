#ifndef IIC_SOFTWARE_H_
#define IIC_SOFTWARE_H_
//软件实现iic通信
#include "stm32h7xx_sys.h"
#ifdef __cplusplus
 extern "C" {
#endif
void IIC_Start_General(GPIO_TypeDef* scl_port, GPIO_TypeDef* sda_port,\
                        uint16_t scl_pin,uint16_t sda_pin);

void IIC_Stop_General(GPIO_TypeDef* scl_port, GPIO_TypeDef* sda_port,\
                        uint16_t scl_pin,uint16_t sda_pin);

bool IIC_Send_Byte_General(GPIO_TypeDef* scl_port, GPIO_TypeDef* sda_port,\
                        uint16_t scl_pin,uint16_t sda_pin,uint8_t txd);

uint8_t IIC_Read_Byte_General(GPIO_TypeDef* scl_port, GPIO_TypeDef* sda_port,\
                        uint16_t scl_pin,uint16_t sda_pin,uint8_t ack);
bool IIC_Write_General(GPIO_TypeDef* scl_port, GPIO_TypeDef* sda_port,\
                        uint16_t scl_pin,uint16_t sda_pin,uint8_t dev_addr,uint8_t addr,const uint8_t* buf,uint16_t len);
bool IIC_Read_General(GPIO_TypeDef* scl_port, GPIO_TypeDef* sda_port,\
                        uint16_t scl_pin,uint16_t sda_pin,uint8_t dev_addr,uint8_t addr,uint8_t* buf,uint16_t len);

#ifdef __cplusplus
}
#endif
#endif

