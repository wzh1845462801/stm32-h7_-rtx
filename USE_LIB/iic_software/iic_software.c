/**
  ******************************************************************************
  * @file    iic_software.c
  * @author  wzh
  * @brief   general method for anologging IIC
  *          
  *
  @verbatim
  ==============================================================================
                     ##### How to use this driver #####
  ==============================================================================
    [..]
    use #define User_IIC() to repalce IIC_xxx_General() 

  @endverbatim
  ******************************************************************************
  * @attention
  *
  *
  ******************************************************************************
  */ 
#include "iic_software.h"

#define IIC_SCL(n)  HAL_GPIO_WritePin(scl_port,scl_pin,(n)?GPIO_PIN_SET:GPIO_PIN_RESET)
#define IIC_SDA(n)  HAL_GPIO_WritePin(sda_port,sda_pin,(n)?GPIO_PIN_SET:GPIO_PIN_RESET)
#define READ_SDA()  HAL_GPIO_ReadPin(sda_port,sda_pin)
#define SDA_OUT()   do{\
                        uint8_t i=0;\
                        for(;(sda_pin>>i)^1;i++);\
                        sda_port->MODER&=~(3<<(2*i));\
                        sda_port->MODER|=1<<(2*i);\
                      }while(0)
#define SDA_IN()    do{\
                        uint8_t i=0;\
                        for(;(sda_pin>>i)^1;i++);\
                        sda_port->MODER&=~(3<<(2*i));\
                        sda_port->MODER|=0<<(2*i);\
                      }while(0)

/*
通用IIC启动信号
*/
void IIC_Start_General(GPIO_TypeDef* scl_port, GPIO_TypeDef* sda_port,\
                        uint16_t scl_pin,uint16_t sda_pin)
{
	SDA_OUT();
	IIC_SDA(1);
	IIC_SCL(1);
	delay_us(4);
	IIC_SDA(0);
	delay_us(4);
	IIC_SCL(0);
}
/*
通用IIC停止信号
*/
void IIC_Stop_General(GPIO_TypeDef* scl_port, GPIO_TypeDef* sda_port,\
                        uint16_t scl_pin,uint16_t sda_pin)
{
	SDA_OUT();
	IIC_SCL(0);
	IIC_SDA(0);
	delay_us(4);
	IIC_SCL(1);
	IIC_SDA(1);
	delay_us(4);
}
static inline char GPIO_TO_Char(GPIO_TypeDef* port){
	switch((uint32_t)port){
		case (uint32_t)GPIOA:return 'A';
		case (uint32_t)GPIOB:return 'B';
		case (uint32_t)GPIOC:return 'C';	
		case (uint32_t)GPIOD:return 'D';
		case (uint32_t)GPIOE:return 'E';
		case (uint32_t)GPIOF:return 'F';
		case (uint32_t)GPIOG:return 'G';
		case (uint32_t)GPIOH:return 'H';
		case (uint32_t)GPIOI:return 'I';
		default:return '?';
	}
}
static inline int8_t PIN_TO_Num(uint16_t pin){
	switch(pin){
		case GPIO_PIN_0:return 0;
		case GPIO_PIN_1:return 1;
		case GPIO_PIN_2:return 2;
		case GPIO_PIN_3:return 3;
		case GPIO_PIN_4:return 4;
		case GPIO_PIN_5:return 5;
		case GPIO_PIN_6:return 6;
		case GPIO_PIN_7:return 7;
		case GPIO_PIN_8:return 8;
		case GPIO_PIN_9:return 9;
		case GPIO_PIN_10:return 10;
		case GPIO_PIN_11:return 11;
		case GPIO_PIN_12:return 12;
		case GPIO_PIN_13:return 13;
		case GPIO_PIN_14:return 14;
		case GPIO_PIN_15:return 15;
		default: return -1;
	}
}
/*
*/
static inline bool IIC_Wait_Ack_General(GPIO_TypeDef* scl_port, GPIO_TypeDef* sda_port,\
                        uint16_t scl_pin,uint16_t sda_pin)
{
	volatile uint8_t ucErrTime=0;
	
	SDA_IN();
	IIC_SDA(1);
	delay_us(1);
	IIC_SCL(1);
	delay_us(1);
	while(READ_SDA())
	{
		ucErrTime++;
		if(ucErrTime>250)
		{
			IIC_Stop_General(scl_port,sda_port,scl_pin,sda_pin);
			printf("IIC通讯异常:scl,P%c%d,sda,P%c%d\r\n",GPIO_TO_Char(scl_port),\
						PIN_TO_Num(scl_pin),GPIO_TO_Char(sda_port),PIN_TO_Num(sda_pin));
			return false;
		}
	}
	IIC_SCL(0);
	return true;
}

static inline void IIC_Ack_General(GPIO_TypeDef* scl_port, GPIO_TypeDef* sda_port,\
                        uint16_t scl_pin,uint16_t sda_pin)
{
	IIC_SCL(0);
	SDA_OUT();
	IIC_SDA(0);
	delay_us(2);
	IIC_SCL(1);
	delay_us(2);
	IIC_SCL(0);
}

static inline void IIC_NAck_General(GPIO_TypeDef* scl_port, GPIO_TypeDef* sda_port,\
                        uint16_t scl_pin,uint16_t sda_pin)
{
	IIC_SCL(0);
	SDA_OUT();
	IIC_SDA(1);
	delay_us(2);
	IIC_SCL(1);
	delay_us(2);
	IIC_SCL(0);
}

bool IIC_Send_Byte_General(GPIO_TypeDef* scl_port, GPIO_TypeDef* sda_port,\
                        uint16_t scl_pin,uint16_t sda_pin,uint8_t txd)
{
	uint8_t t;
	SDA_OUT();
	IIC_SCL(0);
	for(t=0;t<8;t++)
	{
		IIC_SDA((txd&0x80)>>7);
		txd<<=1;
		delay_us(2);
		IIC_SCL(1);
		delay_us(2);
		IIC_SCL(0);
		delay_us(2);
	}
	return IIC_Wait_Ack_General(scl_port,sda_port,scl_pin,sda_pin);
}

uint8_t IIC_Read_Byte_General(GPIO_TypeDef* scl_port, GPIO_TypeDef* sda_port,\
                        uint16_t scl_pin,uint16_t sda_pin,uint8_t ack)
{
	unsigned char i,receive=0;
	SDA_IN();
	for(i=0;i<8;i++)
	{
		IIC_SCL(0);
		delay_us(2);
		IIC_SCL(1);
		receive<<=1;
		if(READ_SDA())
			receive++;
		delay_us(1);
	}
	if(!ack)
		IIC_NAck_General(scl_port,sda_port,scl_pin,sda_pin);
	else
		IIC_Ack_General(scl_port,sda_port,scl_pin,sda_pin);
	return receive;
}
//通用IIC写
bool IIC_Write_General(GPIO_TypeDef* scl_port, GPIO_TypeDef* sda_port,\
                        uint16_t scl_pin,uint16_t sda_pin,uint8_t dev_addr,uint8_t addr,const uint8_t* buf,uint16_t len)
{
	bool res=true;
	IIC_Start_General(scl_port,sda_port,scl_pin,sda_pin);
	res&=IIC_Send_Byte_General(scl_port,sda_port,scl_pin,sda_pin,dev_addr<<1);
	res&=IIC_Send_Byte_General(scl_port,sda_port,scl_pin,sda_pin,addr);
	for(;len>0;len--){
		res&=IIC_Send_Byte_General(scl_port,sda_port,scl_pin,sda_pin,*buf);
		buf++;
	}
	IIC_Stop_General(scl_port,sda_port,scl_pin,sda_pin);
	return res;
}
//通用IIC读
bool IIC_Read_General(GPIO_TypeDef* scl_port, GPIO_TypeDef* sda_port,\
                        uint16_t scl_pin,uint16_t sda_pin,uint8_t dev_addr,uint8_t addr,uint8_t* buf,uint16_t len)
{
	bool res=true;
	IIC_Start_General(scl_port,sda_port,scl_pin,sda_pin);
	res&=IIC_Send_Byte_General(scl_port,sda_port,scl_pin,sda_pin,dev_addr<<1);
	res&=IIC_Send_Byte_General(scl_port,sda_port,scl_pin,sda_pin,addr)==SUCCESS;
	IIC_Start_General(scl_port,sda_port,scl_pin,sda_pin);
	res&=IIC_Send_Byte_General(scl_port,sda_port,scl_pin,sda_pin,(dev_addr<<1)|1);
	for(;len>0;len--){
		*buf=IIC_Read_Byte_General(scl_port,sda_port,scl_pin,sda_pin,len>1);
		buf++;
	}
	IIC_Stop_General(scl_port,sda_port,scl_pin,sda_pin);
	return res;
}
