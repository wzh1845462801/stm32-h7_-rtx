#include "sdcard_rtos.h"


#define SD_TIMEOUT 				5 * 1000	
#define BUF_BLOCK_CNT			64			//此处可以根据自己的内存进行修改
#define USE_DMA_TRANSE			1

#define USE_DEBUG				0

#if USE_DMA_TRANSE==1		
#define USE_DMA_COPY			1
#if USE_DMA_COPY==1
#include "hw_mdma.h"
#define USE_DMA_COPY_CHANAL		0
#endif
#endif

static __ALIGNED(32) uint8_t sdcard_buffer[BLOCKSIZE*BUF_BLOCK_CNT]__IN_AXI_RAM;//做32字节对齐，满足cache line的大小要求


#if SUPPORT_OS==FREE_RTOS
#if USE_DMA_TRANSE
static SemaphoreHandle_t SDCard_Wait_Handle=NULL;
#endif
static SemaphoreHandle_t SDCard_Lock_Handle=NULL;
#elif SUPPORT_OS==RTX_RTOS
#if USE_DMA_TRANSE
static osRtxSemaphore_t SDCard_Wait_Work_Buf __SEMPHORE;
static const osSemaphoreAttr_t SDCard_Wait_Attr={
	.cb_mem=&SDCard_Wait_Work_Buf,
	.cb_size=sizeof(SDCard_Wait_Work_Buf),
	.attr_bits=0,
	.name="SDCard_Wait"
};

static osSemaphoreId_t SDCard_Wait_Handle=NULL;
#endif
static osRtxMutex_t SDCard_Lock_Work_Buf __MUTEX;
static const osMutexAttr_t SDCard_Lock_Attr={
	.cb_mem=&SDCard_Lock_Work_Buf,
	.cb_size=sizeof(SDCard_Lock_Work_Buf),
	.attr_bits=osMutexPrioInherit|osMutexRobust,
	.name="SDCard_Lock"
};
static osMutexId_t SDCard_Lock_Handle=NULL;

static osRtxMutex_t SDCard_Use_Right_Lock_Work_Buf __MUTEX;
static const osMutexAttr_t SDCard_Use_Right_Lock_Attr={
	.cb_mem=&SDCard_Use_Right_Lock_Work_Buf,
	.cb_size=sizeof(SDCard_Use_Right_Lock_Work_Buf),
	.attr_bits=osMutexPrioInherit|osMutexRobust|osMutexRecursive,
	.name="SDCard_UseRighr_Lock"
};
static osMutexId_t SDCard_Use_Right_Lock_Handle=NULL;
#endif

#if USE_DMA_COPY==1
static void SDCard_DMA_MemCpy_CallBack(MDMA_HandleTypeDef* hdma);
//要求地址4字节对齐，数据传输为4*16的整数倍
__STATIC_FORCEINLINE bool SDCard_DMA_MemCpy_Init(void){
	MDMA_InitTypeDef mdma_init;
	
	mdma_init.BufferTransferLength=128;
	mdma_init.DataAlignment=MDMA_DATAALIGN_PACKENABLE;
	mdma_init.DestBlockAddressOffset=0;
	mdma_init.DestBurst=MDMA_DEST_BURST_16BEATS;
	mdma_init.DestDataSize=MDMA_DEST_DATASIZE_WORD;
	mdma_init.DestinationInc=MDMA_DEST_INC_WORD;
	mdma_init.Endianness=MDMA_LITTLE_ENDIANNESS_PRESERVE;
	mdma_init.Priority=MDMA_PRIORITY_HIGH;
	mdma_init.Request=MDMA_REQUEST_SW;
	mdma_init.SourceBlockAddressOffset=0;
	mdma_init.SourceBurst=MDMA_SOURCE_BURST_16BEATS;
	mdma_init.SourceDataSize=MDMA_SRC_DATASIZE_WORD;
	mdma_init.SourceInc=MDMA_SRC_INC_WORD;
	mdma_init.TransferTriggerMode=MDMA_BLOCK_TRANSFER;
	
	if(HW_MDMA_Init(USE_DMA_COPY_CHANAL,&mdma_init)==false)
		return false;
	if(HW_MDMA_Set_CallBack(USE_DMA_COPY_CHANAL,SDCard_DMA_MemCpy_CallBack)==false)
		return false;
	return true;
}
#endif

__STATIC_FORCEINLINE bool SDCard_Object_Init(void){
#if SUPPORT_OS==RTX_RTOS
#if USE_DMA_TRANSE
	if(SDCard_Wait_Handle==NULL)
		SDCard_Wait_Handle=osSemaphoreNew(1,0,&SDCard_Wait_Attr);
	if(SDCard_Wait_Handle==NULL)
		return false;
#endif	
	if(SDCard_Lock_Handle==NULL)
		SDCard_Lock_Handle=osMutexNew(&SDCard_Lock_Attr);
	if(SDCard_Lock_Handle==NULL)
		return false;
	if(SDCard_Use_Right_Lock_Handle==NULL)
		SDCard_Use_Right_Lock_Handle=osMutexNew(&SDCard_Use_Right_Lock_Attr);
	if(SDCard_Use_Right_Lock_Handle==NULL)
		return false;
#elif SUPPORT_OS==FREE_RTOS
#if USE_DMA_TRANSE	
	if(SDCard_Wait_Handle==NULL)
		SDCard_Wait_Handle=xSemaphoreCreateBinary();
	if(SDCard_Wait_Handle==NULL)
		return false;
#endif
	if(SDCard_Lock_Handle==NULL)
		SDCard_Lock_Handle=xSemaphoreCreateMutex();
	if(SDCard_Lock_Handle==NULL){
		return false;
	}
#endif
#if USE_DMA_COPY==1
	if(SDCard_DMA_MemCpy_Init()==false)
		return false;
#endif	
	return true;
}

__STATIC_FORCEINLINE void SDCard_Lock(void){
#if SUPPORT_OS==FREE_RTOS
	xSemaphoreTake(SDCard_Lock_Handle,portMAX_DELAY);
#elif SUPPORT_OS==RTX_RTOS
	osMutexAcquire(SDCard_Lock_Handle,osWaitForever);
#endif
}

__STATIC_FORCEINLINE void SDCard_UnLock(void){
#if SUPPORT_OS==FREE_RTOS
	xSemaphoreGive(SDCard_Lock_Handle);
#elif SUPPORT_OS==RTX_RTOS
	osMutexRelease(SDCard_Lock_Handle);
#endif
}

__STATIC_FORCEINLINE void SDCard_UseRight_Lock(void){
#if SUPPORT_OS==FREE_RTOS
	xSemaphoreTake(SDCard_Use_Right_Lock_Handle,portMAX_DELAY);
#elif SUPPORT_OS==RTX_RTOS
	osMutexAcquire(SDCard_Use_Right_Lock_Handle,osWaitForever);
#endif
}

__STATIC_FORCEINLINE void SDCard_UseRight_UnLock(void){
#if SUPPORT_OS==FREE_RTOS
	xSemaphoreGive(SDCard_Use_Right_Lock_Handle);
#elif SUPPORT_OS==RTX_RTOS
	osMutexRelease(SDCard_Use_Right_Lock_Handle);
#endif
}

#if USE_DMA_TRANSE
__STATIC_FORCEINLINE bool SDCard_Wait(uint32_t time){
#if SUPPORT_OS==FREE_RTOS
	return xSemaphoreTake(SDCard_Wait_Handle,time)==pdTRUE;
#elif SUPPORT_OS==RTX_RTOS
	return osSemaphoreAcquire(SDCard_Wait_Handle,time)==osOK;
#endif
}

__STATIC_FORCEINLINE void SDCard_Give(void){
#if SUPPORT_OS==FREE_RTOS
	BaseType_t pxHigherPriorityTaskWoken;
	xSemaphoreGiveFromISR(SDCard_Wait_Handle,&pxHigherPriorityTaskWoken);
	portYIELD_FROM_ISR(pxHigherPriorityTaskWoken);
#elif SUPPORT_OS==RTX_RTOS
	osSemaphoreRelease(SDCard_Wait_Handle);
#endif
}


__STATIC_FORCEINLINE bool SDCard_MemCpy_Buf_To_Real(void* pdest,void*  psrc,uint32_t len){
#if USE_DMA_COPY==1
	if(!((uint32_t)pdest&0x03)&&!((uint32_t)psrc&0x03)&&!(len&0x3f)){
		SCB_CleanInvalidateDCache();
		HW_MDMA_Start_Transe(USE_DMA_COPY_CHANAL,psrc,pdest,len);
		if(SDCard_Wait(SD_TIMEOUT)==false){
			printf("SD_MDMA读超时\r\n");
			return false;
		}
		SCB_CleanInvalidateDCache();
	}
	else
#endif
	{
		SCB_CleanInvalidateDCache();
		memcpy(pdest,psrc,len);
	}
	return true;
}

__STATIC_FORCEINLINE bool SDCard_MemCpy_Real_To_Buf(void* pdest,const void*  psrc,uint32_t len){
#if USE_DMA_COPY==1
	if(!((uint32_t)pdest&0x03)&&!((uint32_t)psrc&0x03)&&!(len&0x3f)){
		SCB_CleanInvalidateDCache();
		HW_MDMA_Start_Transe(USE_DMA_COPY_CHANAL,psrc,pdest,len);
		if(SDCard_Wait(SD_TIMEOUT)==false){
			printf("SD_MDMA写超时\r\n");
			return false;
		}
		SCB_CleanInvalidateDCache();
	}
	else
#endif
	{
		memcpy(pdest,psrc,len);
		SCB_CleanInvalidateDCache();
	}
	return true;
}
#if USE_DMA_COPY==1
static void SDCard_DMA_MemCpy_CallBack(MDMA_HandleTypeDef* hdma){
	SDCard_Give();
}
#endif

extern void HAL_SD_TxCpltCallback(SD_HandleTypeDef *hsd){
	SDCard_Give();
}

extern void HAL_SD_RxCpltCallback(SD_HandleTypeDef *hsd){
	SDCard_Give();
}

static bool SD_Write_Disk_DMA_Align(uint8_t* buf,uint32_t sector,uint32_t cnt){
	if(HW_SDCard_WriteDisk_DMA(buf,sector,cnt)){
		uint32_t time_cnt=0;
		if(SDCard_Wait(SD_TIMEOUT)==false){
			goto fail;
		}
		while(HW_SDCard_Transe_IsOver()==false){
			delay_ms(1);
			time_cnt++;
			if(time_cnt>SD_TIMEOUT)
				goto fail;
		}
		
	}
	else
		goto fail;
	return true;
fail:
	printf("sd dma write error,buf:0x%08x,sector:%d,cnt:%d\r\n",(uint32_t)buf,sector,cnt);
	return false;
}

static bool SD_Read_Disk_DMA_Align(uint8_t* buf,uint32_t sector,uint32_t cnt){
	if(HW_SDCard_ReadDisk_DMA(buf,sector,cnt)){
		uint32_t time_cnt=0;
		if(SDCard_Wait(SD_TIMEOUT)==false){
			goto fail;
		}
		while(HW_SDCard_Transe_IsOver()==false){
			delay_ms(1);
			time_cnt++;
			if(time_cnt>SD_TIMEOUT)
				goto fail;
		}
	}
	else
		goto fail;
	return true;
fail:	
	printf("sd dma read error,buf:0x%08x,sector:%d,cnt:%d\r\n",(uint32_t)buf,sector,cnt);
	return false;
}
#else
static bool SD_Write_Disk_Align(uint8_t* buf,uint32_t sector,uint32_t cnt){
	if(HW_SDCard_WriteDisk(buf,sector,cnt)){
		uint32_t time_cnt=0;
		while(HW_SDCard_Transe_IsOver()==false){
			delay_ms(1);
			time_cnt++;
			if(time_cnt>SD_TIMEOUT)
				goto fail;
		}
	}
	else
		goto fail;
	return true;
fail:	
	printf("sd write error,buf:0x%08x,sector:%d,cnt:%d\r\n",(uint32_t)buf,sector,cnt);
	return false;
}

static bool SD_Read_Disk_Align(uint8_t* buf,uint32_t sector,uint32_t cnt){
	if(HW_SDCard_ReadDisk(buf,sector,cnt)){
		uint32_t time_cnt=0;
		while(HW_SDCard_Transe_IsOver()==false){
			delay_ms(1);
			time_cnt++;
			if(time_cnt>SD_TIMEOUT)
				goto fail;
		}
	}
	else
		goto fail;
	return true;
fail:	
	printf("sd read error,buf:0x%08x,sector:%d,cnt:%d\r\n",(uint32_t)buf,sector,cnt);
	return false;
}
#endif
bool SDCard_Init(void){
	static bool is_init=false;
	if(is_init==false){
		is_init=SDCard_Object_Init()&&HW_SDCard_Init();
	}
	return is_init;
}

#if USE_DMA_TRANSE

bool SDCard_Read(uint8_t* buf,uint32_t sector,uint32_t cnt){
	uint32_t surplus_num=cnt%BUF_BLOCK_CNT;
	uint32_t total_num=cnt/BUF_BLOCK_CNT;
	SDCard_Lock();
	if(surplus_num!=0){
		if(SD_Read_Disk_DMA_Align(sdcard_buffer,sector,surplus_num)==false){
			SDCard_UnLock();
			return false;
		}	
		if(SDCard_MemCpy_Buf_To_Real(buf,sdcard_buffer,surplus_num*BLOCKSIZE)==false){
			SDCard_UnLock();
			return false;
		}
		sector+=surplus_num;
		buf+=surplus_num*BLOCKSIZE;
	}
	if(total_num!=0){
		for(uint32_t i=0;i<total_num;i++){
			if(SD_Read_Disk_DMA_Align(sdcard_buffer,sector,BUF_BLOCK_CNT)==false){
				SDCard_UnLock();
				return false;
			}
			if(SDCard_MemCpy_Buf_To_Real(buf,sdcard_buffer,BUF_BLOCK_CNT*BLOCKSIZE)==false){
				SDCard_UnLock();
				return false;
			}
			sector+=BUF_BLOCK_CNT;
			buf+=BUF_BLOCK_CNT*BLOCKSIZE;
		}
	}
	SDCard_UnLock();
	return true;
}

bool SDCard_Write(const uint8_t* buf,uint32_t sector,uint32_t cnt){
	uint32_t surplus_num=cnt%BUF_BLOCK_CNT;
	uint32_t total_num=cnt/BUF_BLOCK_CNT;
	SDCard_Lock();
	if(surplus_num!=0){
		if(SDCard_MemCpy_Real_To_Buf(sdcard_buffer,buf,surplus_num*BLOCKSIZE)==false){
			SDCard_UnLock();
			return false;
		}
		if(SD_Write_Disk_DMA_Align(sdcard_buffer,sector,surplus_num)==false){
			SDCard_UnLock();
			return false;
		}
		sector+=surplus_num;
		buf+=surplus_num*BLOCKSIZE;
	}
	
	if(total_num!=0){
		for(uint32_t i=0;i<total_num;i++){
			if(SDCard_MemCpy_Real_To_Buf(sdcard_buffer,buf,BUF_BLOCK_CNT*BLOCKSIZE)==false){
				SDCard_UnLock();
				return false;
			}
			if(SD_Write_Disk_DMA_Align(sdcard_buffer,sector,BUF_BLOCK_CNT)==false){
				SDCard_UnLock();
				return false;
			}
			sector+=BUF_BLOCK_CNT;
			buf+=BUF_BLOCK_CNT*BLOCKSIZE;
		}
	}
	SDCard_UnLock();
	return true;
}

#else
bool SDCard_Read(uint8_t* buf,uint32_t sector,uint32_t cnt){
	uint32_t surplus_num=cnt%BUF_BLOCK_CNT;
	uint32_t total_num=cnt/BUF_BLOCK_CNT;
	SDCard_Lock();
	if(!((uint32_t)buf&0x03)){
		if(SD_Read_Disk_Align(buf,sector,cnt)==false){
			SDCard_UnLock();
			return false;
		}
	}
	else{
		if(surplus_num!=0){
			if(SD_Read_Disk_Align(sdcard_buffer,sector,surplus_num)==false){
				SDCard_UnLock();
				return false;
			}	
			memcpy(buf,sdcard_buffer,surplus_num*BLOCKSIZE);
			sector+=surplus_num;
			buf+=surplus_num*BLOCKSIZE;
		}
		if(total_num!=0){
			for(uint32_t i=0;i<total_num;i++){
				if(SD_Read_Disk_Align(sdcard_buffer,sector,BUF_BLOCK_CNT)==false){
					SDCard_UnLock();
					return false;
				}
				memcpy(buf,sdcard_buffer,BUF_BLOCK_CNT*BLOCKSIZE);
				sector+=BUF_BLOCK_CNT;
				buf+=BUF_BLOCK_CNT*BLOCKSIZE;
			}
		}
	}
	SDCard_UnLock();
	return true;
}

bool SDCard_Write(uint8_t* buf,uint32_t sector,uint32_t cnt){
	uint32_t surplus_num=cnt%BUF_BLOCK_CNT;
	uint32_t total_num=cnt/BUF_BLOCK_CNT;
	SDCard_Lock();
	if(!((uint32_t)buf&0x03)){
		if(SD_Write_Disk_Align(buf,sector,cnt)==false){
			SDCard_UnLock();
			return false;
		}
	}
	else{
		if(surplus_num!=0){
			memcpy(sdcard_buffer,buf,surplus_num*BLOCKSIZE);
			if(SD_Write_Disk_Align(sdcard_buffer,sector,surplus_num)==false){
				SDCard_UnLock();
				return false;
			}
			sector+=surplus_num;
			buf+=surplus_num*BLOCKSIZE;
		}
		
		if(total_num!=0){
			for(uint32_t i=0;i<total_num;i++){
				memcpy(sdcard_buffer,buf,BUF_BLOCK_CNT*BLOCKSIZE);
				if(SD_Write_Disk_Align(sdcard_buffer,sector,BUF_BLOCK_CNT)==false){
					SDCard_UnLock();
					return false;
				}
				sector+=BUF_BLOCK_CNT;
				buf+=BUF_BLOCK_CNT*BLOCKSIZE;
			}
		}
	}
	SDCard_UnLock();
	return true;
}

#endif
//0:FATFS读写，USB关闭
//1:FATFS读，USB读
//2:FATFS卸载，USB读写
static volatile uint8_t sdcard_use_right=0;

uint8_t SDCard_Get_State(void){
	return sdcard_use_right;
}

bool portFATFS_SDCard_Read(uint8_t* buf,uint32_t sector,uint32_t cnt){
	uint8_t state=SDCard_Get_State();
	if(state==FATFS_WR_NO_USB||state==FATFS_R_USB_R)
		return SDCard_Read(buf,sector,cnt);
	else
		return false;
}

bool portFATFS_SDCard_Write(const uint8_t* buf,uint32_t sector,uint32_t cnt){
	uint8_t state=SDCard_Get_State();
	if(state==FATFS_WR_NO_USB)
		return SDCard_Write(buf,sector,cnt);
	else
		return false;
}

bool portUSB_SDCard_Read(uint8_t* buf,uint32_t sector,uint32_t cnt){
	uint8_t state=SDCard_Get_State();
	if(state==FATFS_R_USB_R||state==NO_FATFS_USB_WR)
		return SDCard_Read(buf,sector,cnt);
	else
		return false;
}

bool portUSB_SDCard_Write(const uint8_t* buf,uint32_t sector,uint32_t cnt){
	uint8_t state=SDCard_Get_State();
	if(state==NO_FATFS_USB_WR)
		return SDCard_Write(buf,sector,cnt);
	else
		return false;
}

#include "rl_usb.h"
static bool is_usb_init=false;
static void usb_init(void){
	if(is_usb_init==false){
		USBD_Initialize(0);
		USBD_Connect(0);
		is_usb_init=true;
	}
}
static void usb_uninit(void){
	if(is_usb_init){
		USBD_Disconnect(0);
		USBD_Uninitialize(0);
		is_usb_init=false;
	}
}


#include "fatfs.h"

void SDCard_Set_State(uint8_t state){
	SDCard_UseRight_Lock();
	sdcard_use_right=state;
	switch(sdcard_use_right){
		case FATFS_WR_NO_USB:
			usb_uninit();
			FATFS_Init();
			break;
		case FATFS_R_USB_R:
			usb_init();
			FATFS_Init();
			break;
		case NO_FATFS_USB_WR:
			FATFS_UnInit();
			usb_init();
			break;
		default:
			sdcard_use_right=FATFS_WR_NO_USB;
			break;
	}
	SDCard_UseRight_UnLock();
}
/*
此函数对文件读写性能进行了测试，测试结果如下
-----------------------------------------------------------------------------------------------------			
|文件大小	|是否加入EXRAM	|缓冲区大小	|读速度		|写速度		|是否出现错误					 	|
-----------------------------------------------------------------------------------------------------			
|8MB		|否				|16*512B	|16.16MB/S	|4.19MB/S	|否									|
|8MB		|是				|16*512B	|36.04MB/S	|3.70MB/S	|写过程中出错，读取数据有部分错误	|
|64KB		|是				|16*512B	|15.62MB/S	|4.81MB/S	|否									|
|64KB		|否				|16*512B	|15.62MB/S	|4.81MB/S	|否									|
|64KB		|否				|64*512B	|10.42MB/S	|5.68MB/S	|否									|
|8MB		|否				|64*512B	|21.98MB/S	|8.24MB/S	|否									|
-----------------------------------------------------------------------------------------------------
*/
#include "ff.h"
#define TEST_FILE_SIZE	8*1024*1024

void test_sdcrad_rtos_speed(void){
	FIL* pfile=malloc(sizeof(FIL));
	UINT bw;
	FRESULT res;
	uint8_t* buf=malloc(TEST_FILE_SIZE);
	volatile uint32_t tick_start,tick_end;
	uint32_t i;
	uint32_t time;
	uint8_t test_char;
	double use_time,speed;
	printf("<<<SDIO性能测试>>>\r\n");
	f_unlink("0:/test.txt");
	res=f_open(pfile,"0:/test.txt",FA_OPEN_ALWAYS|FA_CREATE_ALWAYS|FA_WRITE);
	if(res!=FR_OK){
		printf("文件打开失败！\r\n");
	}
	test_char='a';
	for(i=0;i<TEST_FILE_SIZE;i++){
		buf[i]=test_char;
		test_char++;
		if(test_char>'z')
			test_char='a';
	}
	tick_start=DWT->CYCCNT;
	res=f_write(pfile,buf,TEST_FILE_SIZE,&bw);
	if(res!=FR_OK){
		printf("写文件出错！\r\n");
	}
//	res=f_sync(pfile);
//	if(res!=FR_OK){
//		printf("刷新文件出错！\r\n");
//	}
	tick_end=DWT->CYCCNT;
	
	
	time=tick_end-tick_start;
	use_time=time;
	speed=(TEST_FILE_SIZE/(1024*1024*1.0))/use_time*SystemCoreClock;
	printf("写速度：%.2fMB/S\r\n",speed);
	f_close(pfile);
	
	res=f_open(pfile,"0:/test.txt",FA_READ);
	if(res!=FR_OK){
		printf("文件打开失败！\r\n");
	}
	
	tick_start=DWT->CYCCNT;
	res=f_read(pfile,buf,TEST_FILE_SIZE,&bw);
	if(res!=FR_OK){
		printf("读文件出错！\r\n");
	}
	tick_end=DWT->CYCCNT;
	
	time=tick_end-tick_start;
	use_time=time;
	speed=(TEST_FILE_SIZE/(1024*1024*1.0))/use_time*SystemCoreClock;
	printf("读速度：%.2fMB/S\r\n",speed);
	
	test_char='a';
	for(i=0;i<TEST_FILE_SIZE;i++){
		if(buf[i]!=test_char){
			printf("读校验出错！位置%d\r\n",i);
		}
		test_char++;
		if(test_char>'z')
			test_char='a';
	}
	f_close(pfile);
	free(pfile);
	free(buf);
}

#if USE_DEBUG
static uint8_t test_buf[BLOCKSIZE];
static uint8_t test_AXI_RAM_ALIGN_4[BLOCKSIZE]__attribute__((section(".ARM.__at_0X2407FDFC")));
static uint8_t test_AXI_RAM_ALIGN_8[BLOCKSIZE]__attribute__((section(".ARM.__at_0X2407FBF8")));
static uint8_t test_EXRAM_ALIGN_4[BLOCKSIZE]__attribute__((section(".ARM.__at_0XC1FFFDFC")));
static uint8_t test_EXRAM_ALIGN_8[BLOCKSIZE]__attribute__((section(".ARM.__at_0XC1FFFBF8")));

static void HEX_Print(uint8_t* buf,uint32_t cnt){
	for(uint16_t i=0;i<cnt;i++){
		if((buf[i]&0x0f)>9){
			putchar((buf[i]&0x0f)-10+'A');
		}
		else
			putchar((buf[i]&0x0f)+'0');
		if(((buf[i]>>4)&0x0f)>9){
			putchar(((buf[i]>>4)&0x0f)-10+'A');
		}
		else
			putchar(((buf[i]>>4)&0x0f)+'0');
		putchar(' ');
	}
}

static void test_read_align(void){
	printf("<<<对齐读写测试>>>\r\n");
	printf("AXI_RAM 4字节对齐地址0x%8x\r\n",(uint32_t)test_AXI_RAM_ALIGN_4);
	printf("AXI_RAM 8字节对齐地址0x%8x\r\n",(uint32_t)test_AXI_RAM_ALIGN_8);
	printf("EXRAM 4字节对齐地址0x%8x\r\n",(uint32_t)test_EXRAM_ALIGN_4);
	printf("EXRAM 8字节对齐地址0x%8x\r\n",(uint32_t)test_EXRAM_ALIGN_8);
	printf("4字节对齐AXI_RAM测试\r\n");
	if(SD_Read_Disk_DMA_Align(test_AXI_RAM_ALIGN_4,37568,1)==false){
		printf("4字节对齐AXI_RAM测试失败！DMA读取错误\r\n");
	}
	SCB_CleanInvalidateDCache();
	if(SD_Read_Disk_Align(test_buf,37568,1)==false){
		printf("4字节对齐AXI_RAM测试失败！轮询读取错误\r\n");
	}
	printf("读取内容对比\r\n");
	printf("DMA\r\n");
	HEX_Print(test_AXI_RAM_ALIGN_4,BLOCKSIZE);
	printf("\r\n");
	printf("轮询\r\n");
	HEX_Print(test_buf,BLOCKSIZE);
	printf("\r\n");
	if(memcmp(test_AXI_RAM_ALIGN_4,test_buf,BLOCKSIZE)==0)
		printf("读取内容相同\r\n");
	else
		printf("读取内容不同\r\n");
	
	
	printf("8字节对齐AXI_RAM测试\r\n");
	if(SD_Read_Disk_DMA_Align(test_AXI_RAM_ALIGN_8,37568,1)==false){
		printf("8字节对齐AXI_RAM测试失败！DMA读取错误\r\n");
	}
	SCB_CleanInvalidateDCache();
	if(SD_Read_Disk_Align(test_buf,37568,1)==false){
		printf("8字节对齐AXI_RAM测试失败！轮询读取错误\r\n");
	}
	printf("读取内容对比\r\n");
	printf("DMA\r\n");
	HEX_Print(test_AXI_RAM_ALIGN_8,BLOCKSIZE);
	printf("\r\n");
	printf("轮询\r\n");
	HEX_Print(test_buf,BLOCKSIZE);
	printf("\r\n");
	if(memcmp(test_AXI_RAM_ALIGN_8,test_buf,BLOCKSIZE)==0)
		printf("读取内容相同\r\n");
	else
		printf("读取内容不同\r\n");
	
	printf("4字节对齐EXRAM测试\r\n");
	if(SD_Read_Disk_DMA_Align(test_EXRAM_ALIGN_4,37568,1)==false){
		printf("4字节对齐EXRAM测试失败！DMA读取错误\r\n");
	}
	SCB_CleanInvalidateDCache();
	if(SD_Read_Disk_Align(test_buf,37568,1)==false){
		printf("4字节对齐EXRAM测试失败！轮询读取错误\r\n");
	}
	printf("读取内容对比\r\n");
	printf("DMA\r\n");
	HEX_Print(test_EXRAM_ALIGN_4,BLOCKSIZE);
	printf("\r\n");
	printf("轮询\r\n");
	HEX_Print(test_buf,BLOCKSIZE);
	printf("\r\n");
	if(memcmp(test_EXRAM_ALIGN_4,test_buf,BLOCKSIZE)==0)
		printf("读取内容相同\r\n");
	else
		printf("读取内容不同\r\n");
	
	printf("8字节对齐EXRAM测试\r\n");
	if(SD_Read_Disk_DMA_Align(test_EXRAM_ALIGN_8,37568,1)==false){
		printf("8字节对齐EXRAM测试失败！DMA读取错误\r\n");
	}
	SCB_CleanInvalidateDCache();
	if(SD_Read_Disk_Align(test_buf,37568,1)==false){
		printf("8字节对齐EXRAM测试失败！轮询读取错误\r\n");
	}
	printf("读取内容对比\r\n");
	printf("DMA\r\n");
	HEX_Print(test_EXRAM_ALIGN_8,BLOCKSIZE);
	printf("\r\n");
	printf("轮询\r\n");
	HEX_Print(test_buf,BLOCKSIZE);
	printf("\r\n");
	if(memcmp(test_EXRAM_ALIGN_8,test_buf,BLOCKSIZE)==0)
		printf("读取内容相同\r\n");
	else
		printf("读取内容不同\r\n");
}

#endif
