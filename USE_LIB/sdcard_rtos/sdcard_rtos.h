#ifndef SDCARD_RTOS_H__
#define SDCARD_RTOS_H__

#include "stm32h7xx_sys.h"
#include "hw_sdcard.h"

#ifdef __cplusplus
extern "C" {
#endif
#define FATFS_WR_NO_USB		0
#define FATFS_R_USB_R		1
#define NO_FATFS_USB_WR		2

bool SDCard_Init(void);
bool SDCard_Read(uint8_t* buffer,uint32_t sector,uint32_t cnt);
bool SDCard_Write(const uint8_t* buf,uint32_t sector,uint32_t cnt);
uint8_t SDCard_Get_State(void);
void SDCard_Set_State(uint8_t state);
bool portFATFS_SDCard_Read(uint8_t* buf,uint32_t sector,uint32_t cnt);
bool portFATFS_SDCard_Write(const uint8_t* buf,uint32_t sector,uint32_t cnt);
bool portUSB_SDCard_Read(uint8_t* buf,uint32_t sector,uint32_t cnt);
bool portUSB_SDCard_Write(const uint8_t* buf,uint32_t sector,uint32_t cnt);

#ifdef __cplusplus
}
#endif

#endif