#include "lvgl_task.h"
#include "hw_lcd.h"
#include "lvgl/demos/widgets/lv_demo_widgets.h"
#include "lvgl/demos/benchmark/lv_demo_benchmark.h"
#include "lvgl/demos/music/lv_demo_music.h"
#include "fatfs.h"
#include "hw_rand.h"
#include "setting_save.h"

#define MAX_PROTECT_MESS_NUM	128

typedef struct{
	void(*call)(void*);
	void* data;
	osSemaphoreId_t ret_sem;
}LVGL_Task_Mess;

static osRtxThread_t LVGLWorkBuf __THREAD;
static __ALIGNED(8) uint8_t LVGLStack[1024*8];
static const osThreadAttr_t ThreadLVGL_Attr={
	.cb_mem=&LVGLWorkBuf,
	.cb_size=sizeof(LVGLWorkBuf),
	.stack_mem=LVGLStack,
	.stack_size=sizeof(LVGLStack),
	.priority=LVGL_TASK_RPIO,
	.name = "lvgl task",
	.attr_bits=osThreadDetached
};
static osThreadId_t ThreadLVGL	  = NULL;
static void APPTaskLVGL (void*p);

static __ALIGNED(4) uint8_t Message_Buf[osRtxMessageQueueMemSize(MAX_PROTECT_MESS_NUM,sizeof(LVGL_Task_Mess))];
static osRtxMessageQueue_t message_queue_work_buf __QUEUE;
static const osMessageQueueAttr_t	message_queue_attr={
	.cb_mem=&message_queue_work_buf,
	.cb_size=sizeof(message_queue_work_buf),
	.mq_mem=Message_Buf,
	.mq_size=sizeof(Message_Buf),
	.attr_bits=0,
	.name="LVGL_Queue"
};
static osMessageQueueId_t mess_queue=NULL;

bool LVGL_Task_Create(void){
	ThreadLVGL   =osThreadNew(APPTaskLVGL ,NULL,&ThreadLVGL_Attr);
	mess_queue   =osMessageQueueNew(MAX_PROTECT_MESS_NUM,sizeof(LVGL_Task_Mess),&message_queue_attr);
	return ThreadLVGL!=NULL&&mess_queue!=NULL;
}

bool LVGL_Protect_Run(void(*func)(void*),void* use_data,bool wait_response){
	LVGL_Task_Mess buf_mess;
	if(osThreadGetId()==ThreadLVGL){
		func(use_data);
		return true;
	}
	buf_mess.call=func;
	buf_mess.data=use_data;
	if(wait_response){
		buf_mess.ret_sem=osSemaphoreNew(1,0,NULL);
		if(buf_mess.ret_sem==NULL)
			return false;
	}
	else{
		buf_mess.ret_sem=NULL;
	}
	if(osMessageQueuePut(mess_queue,&buf_mess,0,osWaitForever)!=osOK){
		if(wait_response)
			osSemaphoreDelete(buf_mess.ret_sem);
		return false;
	}
	if(wait_response){
		osSemaphoreAcquire(buf_mess.ret_sem,osWaitForever);
		osSemaphoreDelete(buf_mess.ret_sem);
	}
	return true;
}

static void desk_top_init(void){
	lv_demo_widgets();
}

static void APPTaskLVGL (void*p){
	extern void lv_port_disp_init(void);
	extern void lv_port_indev_init(void);
	static LVGL_Task_Mess rec_mess;
	lv_init();					//	LVGL初始化
	lv_port_disp_init();		//	LVGL显示接口初始化
	lv_port_indev_init();	// LVGL触摸接口初始化	
	desk_top_init();
	LCD_BK_Ctrl(true);
	while(true){
		uint32_t next_time=lv_task_handler();
		uint32_t tick_start,tick_use;
		tick_start=osKernelGetTickCount();
		while(osMessageQueueGet(mess_queue,&rec_mess,NULL,next_time)==osOK){
			rec_mess.call(rec_mess.data);
			if(rec_mess.ret_sem!=NULL)
				osSemaphoreRelease(rec_mess.ret_sem);
			tick_use=osKernelGetTickCount()-tick_start;
			if(next_time>tick_use){
				next_time-=tick_use;
			}
			else
				next_time=0;
		}
	}
}




