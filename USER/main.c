#include "main.h"
#include "hw_led.h"
#include "hw_qspi_flash.h"
#include "flash_burn.h"
#include "hw_touch.h"
#include "hw_time.h"
#include "hw_cpu_temp.h"
#include "hw_rand.h"
#include "fatfs.h"
#include "setting_save.h"
#include "lualib.h"
#include "lauxlib.h"
#include "hw_temp.h"
#include "decoder_inc/wav_decoder.h"
#include "decoder_inc/mp3_decoder.h"
#include <time.h>
#include "decoder_inc/flac_decoder.h"
#include "decoder_inc/ape_decoder.h"
#include "decoder_inc/aac_decoder.h"
#include "visual_debug.h"
#include "lvgl_task.h"

static osRtxThread_t StartWorkerBuf __THREAD;
static __ALIGNED(8) uint8_t StartStack[1024*2];
/* 任务的属性设置 */
static const osThreadAttr_t ThreadStart_Attr = {
	.cb_mem = &StartWorkerBuf,
	.cb_size = sizeof(StartWorkerBuf),
	.stack_mem = StartStack,
	.stack_size = sizeof(StartStack),
	.priority = START_TASK_PRIO,
	.name = "osRtxStartThread",
	.attr_bits = osThreadDetached, 
};

static osRtxThread_t FreshWorkBuf __THREAD;
static __ALIGNED(8) uint8_t FreshStack[512];
/* 任务的属性设置 */
static const osThreadAttr_t ThreadFresh_Attr = {
	.cb_mem = &FreshWorkBuf,
	.cb_size = sizeof(FreshWorkBuf),
	.stack_mem = FreshStack,
	.stack_size = sizeof(FreshStack),
	.priority = FRESH_TASK_PRIO,
	.name = "FreshThread",
	.attr_bits = osThreadDetached, 
};

static osRtxThread_t LUAWorkBuf __THREAD;
static __ALIGNED(8) uint8_t LUAStack[1024*8];
/* 任务的属性设置 */
static const osThreadAttr_t ThreadLUA_Attr={
	.cb_mem=&LUAWorkBuf,
	.cb_size=sizeof(LUAWorkBuf),
	.stack_mem=LUAStack,
	.stack_size=sizeof(LUAStack),
	.priority=LUA___TASK_PRIO,
	.name = "LUAThread",
	.attr_bits=osThreadDetached
};


/* 任务句柄 */
static osThreadId_t ThreadIdStart = NULL;
static osThreadId_t ThreadIdFresh = NULL;
static osThreadId_t ThreadIdLUA	  = NULL;


static void AppTaskStart(void *argument);
static void AppTaskFresh(void* p);
static void AppTaskLUA  (void* p);
static void APPTaskLVGL (void*p);


int main(void){ 
	//单片机主频、HAL库等初始化在stm32h7_sys.c中定义
	//在进入main函数前已完成初始化
	//内核的初始化同样在main函数前完成
	//malloc等内存函数已初始化完成，适配C++全局构造函数中出现的new关键词
	//文件IO未初始化
	/* 创建启动任务 */
	ThreadIdStart = osThreadNew(AppTaskStart, NULL, &ThreadStart_Attr);  
	/* 开启多任务 */
	osKernelStart();
	while(true);
}


static void My_Music_Regiseter(const Music_Decoder* pDecoder){
	for(const char*const* surport_file=pDecoder->surport_file;*surport_file!=NULL;surport_file++){
		printf("%s,",*surport_file);
	}
	if(Music_Decoder_Register(pDecoder)){
		printf("音频解码器挂载成功\r\n");
	}
	else{
		printf("音频解码器挂载失败\r\n");
	}
}

static void My_Music_Init(void){
	if(Music_Device_Init()){
		printf("音乐播放器初始化成功\r\n");
		My_Music_Regiseter(&Wav_Decoder_Device);
		My_Music_Regiseter(&Mp3_Decoder_Device);
		My_Music_Regiseter(&Flac_Decoder_Device);
		My_Music_Regiseter(&Ape_Decoder_Device);
		My_Music_Regiseter(&AAC_Decoder_Device);
	}
	else
		printf("音乐播放器初始化失败\r\n");
}

static void Object_Init(void){
	FATFS_Init();				//初始化文件系统
	Load_Setting();				//加载初始化配置文件
	flash_data_init();			//QSPI_Flash存储数据预读取，配置QSPI_Flash为地址映射模式，加速读取
	My_Music_Init();
	Visual_Debug_Init();
}

static void Task_Create(void){
	ThreadIdFresh=osThreadNew(AppTaskFresh,NULL,&ThreadFresh_Attr);
	ThreadIdLUA  =osThreadNew(AppTaskLUA  ,NULL,&ThreadLUA_Attr  );
	LVGL_Task_Create();
}

static __NO_RETURN void AppTaskStart(void *argument){
	(void)argument;
	HW_LED_Init();
	HW_QSPI_Flash_Init();
	HW_RTC_Init();
	HW_CPU_Temp_Init();
	HW_Rand_Init();
	HW_Temp_Init();
	Object_Init();
	Task_Create();
	while(true){
		LED0_Toggle();
		delay_ms(500);
	}
}

static __NO_RETURN void AppTaskFresh(void* p){
	(void)p;
	while(true){
		RTC_Time_Fresh();
		delay_ms(500);
	}
}


static void lua_Err_Handle(lua_State* L, int lua_ErrCode) {
	
    const char*     ErrBuff;
    const char*     error;
    if(lua_ErrCode == 0)
        return;

    switch(lua_ErrCode) {
	case LUA_ERRSYNTAX:
		ErrBuff="syntax error during precompilation.";
		break;
	case LUA_ERRMEM:
		ErrBuff="memory allocation error.";
		break;
	case LUA_ERRRUN:
		ErrBuff="a runtime error.";
		break;
	case LUA_YIELD:
		ErrBuff="Thread has supended.";
		break;
	case LUA_ERRERR:
		ErrBuff="error while running the error handler function.";
		break;
	default:
		ErrBuff="unknow err.";
		break;
    }
    error = lua_tostring(L, -1);                    // print error results
    printf("LUA Error type:%s \r\n%s\r\n", ErrBuff, error);  // 
    lua_pop(L, 1);
}

static int do_file_script(void) {
    int         lua_ret;
    lua_State   *L;
    L = luaL_newstate();                    // 建立Lua运行环境
    luaL_openlibs(L);
    luaopen_base(L);
    lua_ret = luaL_dofile(L, "0:/LUA/LOAD/main.lua");   // load file.
    lua_Err_Handle(L, lua_ret);             // print lua run error code.
    lua_close(L);                           // close lua code.
    return 0;
}
static __NO_RETURN void AppTaskLUA  (void* p){
	(void)p;
	while(true){
		printf("Lua脚本正在运行。。。\r\n");
		do_file_script();
		printf("Lua脚本运行结束！\r\n");
		osThreadSuspend(osThreadGetId());
	}
}




